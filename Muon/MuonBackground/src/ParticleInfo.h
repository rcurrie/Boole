/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PARTICLEINFO_H
#define PARTICLEINFO_H 1

// Forward declarations
#include "Event/MCParticle.h"

/** @class ParticleInfo ParticleInfo.h
 *
 *
 *  @author Alessia Satta
 *  @date   2003-02-19
 */
class ParticleInfo {
public:
  /// Standard constructor
  ParticleInfo( const LHCb::MCParticle* particle, int station, int gaps, int collision );
  ParticleInfo( const LHCb::MCParticle* particle, int collision );

  virtual ~ParticleInfo(); ///< Destructor
  void             setHitIn( int station, int gap, int chamber );
  std::vector<int> multiplicityResults();
  int              getCollision();

protected:
private:
  int        m_pileupEventNumber;
  static int m_stationNumber;
  static int m_gapsNumber;
  static int maxDimension;

  std::vector<std::vector<int>>* m_storagePointer;
  std::vector<int>*              m_resultPointer;
};
#endif // PARTICLEINFO_H
