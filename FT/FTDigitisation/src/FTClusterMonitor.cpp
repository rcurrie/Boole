/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// local
#include "FTClusterMonitor.h"

// from boost
#include "AIDA/IHistogram1D.h"

//-----------------------------------------------------------------------------
// Implementation file for class : FTClusterMonitor
//
// 2012-07-05 : Eric Cogneras
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( FTClusterMonitor )

FTClusterMonitor::FTClusterMonitor( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer( name, pSvcLocator,
                {KeyValue{"ClusterLocation", LHCb::FTClusterLocation::Default},
                 // KeyValue{"HitsLocation", LHCb::MCHitLocation::FT},
                 KeyValue{"LinkerLocation", Links::location( std::string( LHCb::FTLiteClusterLocation::Default ) +
                                                             "2MCHitsWithSpillover" )}} ) {}

//=============================================================================
// Initialization
//=============================================================================
StatusCode FTClusterMonitor::initialize() {

  StatusCode sc = Consumer::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;

  /// Retrieve and initialize DeFT (no test: exception in case of failure)
  m_deFT = getDet<DeFTDetector>( DeFTDetectorLocation::Default );

  if ( m_deFT == nullptr ) return Error( "Could not initialize DeFTDetector", StatusCode::FAILURE );
  if ( m_deFT->version() < 61 ) return Error( "This version requires FTDet v6.1 or higher", StatusCode::FAILURE );

  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Initialize" << endmsg;

  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
void FTClusterMonitor::operator()( const LHCb::FTClusters& clusters,
                                   // const LHCb::MCHits& mcHits,
                                   const LHCb::LinksByKey& links ) const {

  // retrieve FTLiteClustertoMCHitLink
  auto myClusterToHitLink = InputLinks<ContainedObject, LHCb::MCHit>( links );

  plot( clusters.size(), "nClusters", "Number of clusters; Clusters/event; Events", 0., 15.e3, 100 );

  uint prevSiPM = 0u, prevModuleID = 0u;
  int  clustersInSiPM = 0;

  // Loop over FTCluster
  for ( const auto cluster : clusters ) {

    if ( cluster->isLarge() == 2 ) continue; // intermediate fragments of large clusters

    // Get the FTChannelID
    LHCb::FTChannelID chanID = cluster->channelID();
    // Get the correct module
    const DeFTModule* module = m_deFT->findModule( chanID );

    // draw cluster channel properties
    plot( (float)chanID.module(), "ClustersPerModule", "Clusters per module; Module; Clusters", -0.5, 5.5, 6 );
    plot( (float)chanID.sipmInModule(), "ClustersPerSiPM", "Clusters per SiPM; SiPMID; Clusters", 0., 16., 16 );
    plot( (float)chanID.channel(), "ClustersPerChannel", "Clusters per channel; Channel; Clusters", -0.5, 127.5, 128 );

    if ( module != nullptr ) {
      int pseudoChannel = module->pseudoChannel( chanID );
      plot( pseudoChannel, "ClustersPerPseudoChannel",
            "Clusters per pseudo channel;Pseudo channel;Clusters/(64 channels)", 0., 12288., 192 );
    }

    if ( cluster->isLarge() == 0 ) {

      plot( cluster->fraction(), "ClusterFraction", "Cluster fraction; Fraction", -0.25, 0.75, 10 );
      plot( cluster->size(), "ClusterSize", "Cluster size; Size", -0.5, 4.5, 5 );
      plot( cluster->charge(), "ClusterCharge", "Cluster charge; Charge", 10.5, 100.5, 90 );
      plot2D( cluster->size(), cluster->charge(), "ClusterSizeVsFraction", "Cluster size vs Charge; Size; Charge", -0.5,
              4.5, 0., 100., 5, 5 );
      plot2D( cluster->size(), cluster->fraction(), "ClusterSizeVsFraction",
              "Cluster size vs fraction ; Size; Fraction", -0.5, 4.5, -0.25, 0.75, 5, 100 );
    }

    // Plots for cluster position resolution
    // Get the correct mat
    const DeFTMat* mat        = module ? module->findMat( chanID ) : nullptr;
    const auto     mcHitLinks = myClusterToHitLink.from( chanID );

    if ( mat != nullptr ) {

      // Loop over all links to MCHits
      for ( const auto& imcHit : mcHitLinks ) {
        const auto mcHit = imcHit.to();

        // Plot the resolution only for p > pMin
        if ( mcHit->p() < m_minPforResolution ) continue;

        // Plot the resolution only for the signal spill
        if ( mcHit->parent()->registry()->identifier() != "/Event/" + LHCb::MCHitLocation::FT ) continue;

        double dXCluster = mat->distancePointToChannel( mcHit->midPoint(), chanID, cluster->fraction() );

        plot( dXCluster, "Resolution/ClusterResolution",
              "Cluster resolution; Cluster - MCHit x position [mm]; Number of clusters", -1, 1, 100 );

        plot( dXCluster,
              "Resolution/ClusterResolution" + std::string( cluster->isLarge() == 0 ? "Small" : "Fragmented" ),
              "Cluster resolution " + std::string( cluster->isLarge() == 0 ? "small" : "fragmented" ) + " clusters" +
                  "; Cluster - MCHit x position [mm]; Number of clusters",
              -1, 1, 100 );

        if ( cluster->isLarge() == 0 ) {

          plot( dXCluster, "Resolution/ClusterResolutionSize" + std::to_string( cluster->size() ),
                "Cluster resolution size" + std::to_string( cluster->size() ) +
                    "; Cluster - MCHit x position [mm]; Number of clusters",
                -1, 1, 100 );
        }
      }
    }

    // Count the number of clusters per SiPM
    uint thisSiPM = chanID.uniqueSiPM();
    if ( thisSiPM != prevSiPM ) {
      if ( clustersInSiPM != 0 ) {
        plot( clustersInSiPM, "ClustersInSiPM", "Clusters per SiPM; Number of clusters; Number of SiPMs", -0.5, 20.5,
              21 );
        plot( clustersInSiPM, "ClustersInSiPM_Module" + std::to_string( prevModuleID ),
              "Clusters per SiPM; Number of clusters; Number of SiPMs", -0.5, 20.5, 21 );
        clustersInSiPM = 0;
      }
      prevSiPM     = thisSiPM;
      prevModuleID = chanID.module();
    }
    ++clustersInSiPM;
  }

  // Fill this for the last time
  plot( clustersInSiPM, "ClustersInSiPM", "Clusters per SiPM; Number of clusters; Number of SiPMs", -0.5, 20.5, 21 );
  plot( clustersInSiPM, "ClustersInSiPM_Module" + std::to_string( prevModuleID ),
        "Clusters per SiPM; Number of clusters; Number of SiPMs", -0.5, 20.5, 21 );

  return;
}

//=============================================================================
// Finalization
//=============================================================================
StatusCode FTClusterMonitor::finalize() {

  AIDA::IHistogram1D* hNClusters = histo1D( HistoID( "nClusters" ) );

  info() << " --------FT clusters------------" << endmsg;
  if ( hNClusters != nullptr ) {
    info() << "Number of clusters per event  = " << format( "%4.1f", hNClusters->mean() ) + " +/- "
           << format( "%4.1f", hNClusters->rms() ) << endmsg;
  } else
    info() << "No clusters were found!" << endmsg;

  info() << " -------------------------------" << endmsg;

  return Consumer::finalize(); // must be executed first
}
