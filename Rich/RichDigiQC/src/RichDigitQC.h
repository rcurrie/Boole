/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//------------------------------------------------------------------------------------
/** @file RichDigitQC.h
 *
 *  Header file for RICH Digitisation Quality Control algorithm : Rich::MC::Digi::DigitQC
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date   2003-09-08
 */
//------------------------------------------------------------------------------------

#pragma once

// STD
#include <sstream>

// Boost
#include "boost/lexical_cast.hpp"

// base class
#include "RichKernel/RichHistoAlgBase.h"

// Event model
#include "Event/MCRichDigit.h"

// Kernel
#include "Kernel/RichDetectorType.h"
#include "Kernel/RichSmartID.h"

// Rich Utils
#include "RichUtils/RichHashMap.h"
#include "RichUtils/RichMap.h"
#include "RichUtils/RichPoissonEffFunctor.h"
#include "RichUtils/RichStatDivFunctor.h"

// RichDet
#include "RichDet/DeRichSystem.h"

// RICH Interfaces
#include "MCInterfaces/IRichMCTruthTool.h"
#include "RichInterfaces/IRichSmartIDTool.h"

// Boost
#include "boost/format.hpp"

namespace Rich::MC::Digi {

  /** @class DigitQC RichDigitQC.h
   *
   *  Monitor for Rich digitisation and DAQ simulation
   *
   *  @author Chris Jones   (Christopher.Rob.Jones@cern.ch)
   *  @date   2003-09-08
   */

  class DigitQC final : public Rich::HistoAlgBase {

  public:
    /// Standard constructor
    DigitQC( const std::string& name, ISvcLocator* pSvcLocator );

    StatusCode initialize() override final; // Algorithm initialization
    StatusCode execute() override final;    // Algorithm execution
    StatusCode finalize() override final;   // Algorithm finalization

  private: // methods
    /// Returns the location of container for the MCRichHit associated to the given digit
    std::string mchitLocation( const LHCb::MCRichDigit* digit ) const;

  private: // data
    /// Pointer to RICH system detector element
    const DeRichSystem* m_richSys;

    /// Pointer to RichSmartID tool
    const Rich::ISmartIDTool* m_smartIDs;

    /// Pointer to MC truth tool
    const Rich::MC::IMCTruthTool* m_mcTool = nullptr;

    // job options
    std::string m_digitTDS;   ///< Location of MCRichDigits in TES
    bool        m_extraHists; ///< Flag to turn on the production of additional histograms

    /// Number of events processed
    unsigned long long int m_evtC{0};

    /// Counter for hits in each HPD
    typedef Rich::HashMap<const LHCb::RichSmartID, unsigned int> HPDCounter;
    Rich::Map<Rich::DetectorType, HPDCounter> m_nHPDSignal; ///< Tally for HPD occupancy, in each RICH

    typedef Rich::HashMap<std::string, unsigned int> SpillCount;

    typedef Rich::Map<Rich::DetectorType, SpillCount> SpillDetCount;

    /// Number of digitised hits per RICH detector and event location
    SpillDetCount m_spillDigits;

    /// Number of signal digitised hits per RICH detector and event location
    SpillDetCount m_spillDigitsSignal;

    /// Number of raw MC hits hits per RICH detector and event location
    SpillDetCount m_totalSpills;

    /// Counter for hit tallies
    typedef std::map<Rich::DetectorType, unsigned int> HitTally;

    /// Number of digitised hits in each RICH
    HitTally m_allDigits;

    /// Number of rayleigh scattered hits in each RICH
    HitTally m_scattHits;

    /// Number of charged track hits in each RICH
    HitTally m_chrgTkHits;

    /// Number of gas quartz window CK hits in each RICH
    HitTally m_gasQCK;

    /// Number of HPD quartz window CK hits in each RICH
    HitTally m_hpdQCK;

    /// Number of nitrogen CK hits in each RICH
    HitTally m_nitroQCK;

    /// Number of nitrogen CK hits in each RICH
    HitTally m_aeroFiltQCK;

    /// Number of background hits in each RICH
    HitTally m_bkgHits;

    /// Number of charge shared hits in each RICH
    HitTally m_chrgShrHits;

    /// Number of HPD Reflection hits in each RICH
    HitTally m_hpdReflHits;

    /// Number of silicon backscatter hits in each RICH
    HitTally m_siBackScatt;

    /// Number of signal-induced noise hits in each RICH
    HitTally m_signalInducedNoiseHits;

    /// List of event locations to look for MCRichHits in
    typedef Rich::HashMap<std::string, bool> EventLocations;
    EventLocations                           m_evtLocs;
  };

  inline std::string DigitQC::mchitLocation( const LHCb::MCRichDigit* digit ) const {
    // Always just use the first hit, since this will always be signal if the digit has a signal contribution
    // In case of signal-induced noise digit, return proper information ( no associated hits )
    return ( digit->hits().empty() ? ( digit->history().signalInducedNoise() ? "Digit due to SIN" : "UNKNOWN" )
                                   : objectLocation( digit->hits().front().mcRichHit()->parent() ) );
  }

} // namespace Rich::MC::Digi