/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef FTFPGACLUSTERCREATOR_H
#define FTFPGACLUSTERCREATOR_H 1

// from Gaudi
#include "GaudiAlg/Transformer.h"

// LHCbKernel
#include "Kernel/FTChannelID.h"
// from Linker
#include "Associators/Associators.h"

// from FTEvent
#include "Event/FTCluster.h"
#include "Event/FTLiteCluster.h"
#include "Event/MCFTDigit.h"

/** @class FTFPGAClusterCreator FTFPGAClusterCreator.h
 *
 *
 *  @author Eric Cogneras
 *  @date   2012-04-06
 */

class FTFPGAClusterCreator
    : public Gaudi::Functional::MultiTransformer<
          std::tuple<LHCb::FTClusters, LHCb::LinksByKey, LHCb::LinksByKey, LHCb::LinksByKey, LHCb::LinksByKey>(
              const LHCb::MCFTDigits& )> {

public:
  FTFPGAClusterCreator( const std::string& name, ISvcLocator* pSvcLocator );

  std::tuple<LHCb::FTClusters, LHCb::LinksByKey, LHCb::LinksByKey, LHCb::LinksByKey, LHCb::LinksByKey>
  operator()( const LHCb::MCFTDigits& digits ) const override;

private:
  // Job options
  Gaudi::Property<bool> m_storePECharge{this, "StorePECharge", true, "Flag to store PE instead of ADC in FTCluster"};

  Gaudi::Property<unsigned int> m_clusterMinWidth{this, "ClusterMinWidth", 1, "Minimal cluster width"};
  Gaudi::Property<unsigned int> m_clusterMaxWidth{this, "ClusterMaxWidth", 4, "Maximal cluster width"};
  Gaudi::Property<float>        m_lowestFraction{this, "LowestFraction", -0.250,
                                          "The fraction is defined in the range (-0.250,0.750)"};
  Gaudi::Property<unsigned int> m_largeClusterSize{this, "LargeClusterSize", 4,
                                                   "Define when to flag unfragmented cluster as large"};

  Gaudi::Property<bool> m_usePEnotADC{this, "UsePENotADC", false, "Flag to use (float)PE instead of (int)ADC"};

  Gaudi::Property<bool> m_fragBigCluster{this, "FragBigCluster", true,
                                         "Flag to fragment clusters in ClusterMaxWidth chunks"};
  Gaudi::Property<bool> m_keepBigCluster{this, "KeepBigCluster", false,
                                         "Flag to keep (fragmented or not) big clusters"};
  Gaudi::Property<bool> m_keepIsolatedPeaks{this, "KeepIsolatedPeaks", true, "Flag to keep isolated clusters"};
  Gaudi::Property<bool> m_keepEdgeCluster{this, "KeepEdgeCluster", false,
                                          "Flag to keep sipm edge and central dead area clusters"};

  Gaudi::Property<float> m_adcThreshold1{this, "ADCThreshold1", 1, "add-to-cluster threshold"};
  Gaudi::Property<float> m_adcThreshold2{this, "ADCThreshold2", 2, "seed threshold"};
  Gaudi::Property<float> m_adcThreshold3{this, "ADCThreshold3", 3, "single-channel threshold"};
  Gaudi::Property<float> m_adcThreshold1Weight{this, "ADCThreshold1Weight", 1., "add-to-cluster weight"};
  Gaudi::Property<float> m_adcThreshold2Weight{this, "ADCThreshold2Weight", 2., "seed weight"};
  Gaudi::Property<float> m_adcThreshold3Weight{this, "ADCThreshold3Weight", 6., "single-channel weight"};
};
#endif // FTCLUSTERCREATOR_H
