/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef IMCFTDISTRIBUTIONCHANNELTOOL_H
#define IMCFTDISTRIBUTIONCHANNELTOOL_H 1

// from Gaudi
#include "GaudiKernel/IAlgTool.h"

// from LHCb
#include "Kernel/FTChannelID.h"

// FTDet
#include "FTDet/DeFTMat.h"

/** @class IMCFTDistributionChannelTool IMCFTDistributionChannelTool.h
 *
 *  Interface for the tool that transforms deposited energies to photons
 *  exiting the fibre ends
 *
 *  @author Violaine Bellee, Julian Wishahi
 *  @date   2017-02-14
 */

typedef std::vector<LHCb::FTChannelID>                    FTChannelIDs;
typedef std::vector<std::pair<LHCb::FTChannelID, double>> FTChannelIDsAndFracs;

struct IMCFTDistributionChannelTool : extend_interfaces<IAlgTool> {

  // Return the interface ID
  DeclareInterfaceID( IMCFTDistributionChannelTool, 1, 0 );

  virtual LHCb::FTChannelID targetChannel( double posX, const DeFTMat& mat )                                        = 0;
  virtual LHCb::FTChannelID targetChannel( double posX, double posZ, double dXdY, double dZdY, const DeFTMat& mat ) = 0;

  virtual FTChannelIDsAndFracs targetChannelsFractions( double posXentry, double posXexit, const DeFTMat& mat ) = 0;
};

#endif // IMCFTDISTRIBUTIONCHANNELTOOL_H
