/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//===============================================================================
/** @file RichSimpleFrontEndResponse.cpp
 *
 *  Implementation file for RICH digitisation algorithm : RichSimpleFrontEndResponse
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @author Alex Howard   a.s.howard@ic.ac.uk
 *  @date   2003-11-06
 */
//===============================================================================

#include "RichSimpleFrontEndResponse.h"

using namespace Rich::MC::Digi;

DECLARE_COMPONENT( SimpleFrontEndResponse )

// Standard constructor, initializes variables
SimpleFrontEndResponse::SimpleFrontEndResponse( const std::string& name, ISvcLocator* pSvcLocator )
    : Rich::AlgBase( name, pSvcLocator ), m_AdcCut( 85 ) {

  // job opts
  declareProperty( "MCRichSummedDepositsLocation",
                   m_mcRichSummedDepositsLocation = LHCb::MCRichSummedDepositLocation::Default );
  declareProperty( "MCRichDigitsLocation", m_mcRichDigitsLocation = LHCb::MCRichDigitLocation::Default );
  declareProperty( "SimpleCalibration", m_Calibration = 4420 );
  declareProperty( "SimpleBaseline", m_Baseline = 50 );
  declareProperty( "SimpleSigma", m_Sigma = 1.0 );

  Rndm::Numbers m_gaussRndm;
}

StatusCode SimpleFrontEndResponse::initialize() {
  // Initialize base class
  const StatusCode sc = Rich::AlgBase::initialize();
  if ( sc.isFailure() ) { return sc; }

  // create a collection of all pixels
  const Rich::ISmartIDTool* smartIDs = nullptr;
  acquireTool( "RichSmartIDTool", smartIDs, nullptr, true );
  const auto& pixels = smartIDs->readoutChannelList();
  actual_base        = theRegistry.GetNewBase( pixels );
  releaseTool( smartIDs );

  // Gauss randomn dist
  return m_gaussRndm.initialize( randSvc(), Rndm::Gauss( 0.0, 0.9 ) );
}

StatusCode SimpleFrontEndResponse::finalize() {
  // finalize randomn number generator
  m_gaussRndm.finalize().ignore();

  // finalize base class
  return Rich::AlgBase::finalize();
}

StatusCode SimpleFrontEndResponse::execute() {
  SummedDeposits = get<LHCb::MCRichSummedDeposits>( m_mcRichSummedDepositsLocation );

  _ri_debug << "Successfully located " << SummedDeposits->size() << " MCRichSummedDeposits at "
            << m_mcRichSummedDepositsLocation << endmsg;

  // Run the Simple treatment
  return Simple();
}

StatusCode SimpleFrontEndResponse::Simple() {

  // make new mcrichdigits
  auto* mcRichDigits = new LHCb::MCRichDigits();
  put( mcRichDigits, m_mcRichDigitsLocation );

  for ( auto* SumDep : *SummedDeposits ) {

    auto props = actual_base->DecodeUniqueID( SumDep->key() );
    if ( props ) {

      // Make new MCRichDigit
      auto* newDigit = new LHCb::MCRichDigit();

      double                       summedEnergy = 0.0;
      LHCb::MCRichDigitHit::Vector hitVect;
      for ( const auto& deposit : SumDep->deposits() ) {
        if ( deposit->time() > 0.0 && deposit->time() < 25.0 ) {
          summedEnergy += deposit->energy();
          hitVect.emplace_back( LHCb::MCRichDigitHit( *( deposit->parentHit() ), deposit->history() ) );
        }
      }
      newDigit->setHits( hitVect );

      SumDep->setSummedEnergy( summedEnergy );

      // Store history info
      newDigit->setHistory( SumDep->history() );

      const int value = int( ( summedEnergy + m_Sigma * m_gaussRndm() / 1000 ) * m_Calibration ) + m_Baseline;
      if ( !newDigit->hits().empty() && value >= m_AdcCut ) {
        mcRichDigits->insert( newDigit, SumDep->key() );
      } else {
        delete newDigit;
      }
    }
  }

  _ri_debug << "Created " << mcRichDigits->size() << " MCRichDigits at " << m_mcRichDigitsLocation << endmsg;

  return StatusCode::SUCCESS;
}
