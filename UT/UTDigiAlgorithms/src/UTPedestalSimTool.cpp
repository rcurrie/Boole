/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "GaudiAlg/GaudiTool.h"
#include "IUTPedestalSimTool.h"
#include "Kernel/LHCbConstants.h"
#include "Kernel/UTChannelID.h"

/** @class UTPedestalSimTool UTPedestalSimTool.h
 *
 *  Tool for for simulating pedestal
 *
 *  @author M.Needham
 *  @date   14/3/2010
 */

class UTPedestalSimTool : public extends<GaudiTool, IUTPedestalSimTool> {

public:
  using extends::extends;

  double pedestal( const LHCb::UTChannelID& chan ) const override {
    const unsigned int beetleStrip = ( chan.strip() - 1u ) % LHCbConstants::nStripsInBeetle;
    // cubic param of the baseline
    return ( m_param[0] + beetleStrip * ( m_param[1] + beetleStrip * ( m_param[2] + beetleStrip * m_param[3] ) ) );
  }

private:
  // Job option: param of pedestals
  Gaudi::Property<std::vector<double>> m_param{this, "ParamValues", {127.8, 2.45e-3, 9.83e-4, -3.25e-6}};
};

DECLARE_COMPONENT( UTPedestalSimTool )
