/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef BUILDMCTRACKINFO_H
#define BUILDMCTRACKINFO_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"

#include "Event/MCTrackInfo.h"

class DeVP;
class DeUTDetector;
class DeFTDetector;

/** @class BuildMCTrackInfo BuildMCTrackInfo.h
 *  Build the Reconstructable MCProperty table.
 *
 *  @author Olivier Callot
 *  @date   2012-04-02 : Updated version for upgrade: IT, OT and FT optional, Pixel/normal Velo
 */
class BuildMCTrackInfo final : public GaudiAlgorithm {
  using GaudiAlgorithm::GaudiAlgorithm;

public:
  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution

protected:
  void updateBit( int& result, int sta, bool isX ) {
    if ( 0 == sta ) {
      result |= MCTrackInfo::maskTT1;
    } else if ( 1 == sta ) {
      result |= MCTrackInfo::maskTT2;
    } else if ( 2 == sta ) {
      if ( isX )
        result |= MCTrackInfo::maskT1X;
      else
        result |= MCTrackInfo::maskT1S;
    } else if ( 3 == sta ) {
      if ( isX )
        result |= MCTrackInfo::maskT2X;
      else
        result |= MCTrackInfo::maskT2S;
    } else if ( 4 == sta ) {
      if ( isX )
        result |= MCTrackInfo::maskT3X;
      else
        result |= MCTrackInfo::maskT3S;
    }
  }

  void updateAccBit( int& result, int sta, bool isX ) {
    if ( 0 == sta ) {
      result |= MCTrackInfo::maskAccTT1;
    } else if ( 1 == sta ) {
      result |= MCTrackInfo::maskAccTT2;
    } else if ( 2 == sta ) {
      if ( isX )
        result |= MCTrackInfo::maskAccT1X;
      else
        result |= MCTrackInfo::maskAccT1S;
    } else if ( 3 == sta ) {
      if ( isX )
        result |= MCTrackInfo::maskAccT2X;
      else
        result |= MCTrackInfo::maskAccT2S;
    } else if ( 4 == sta ) {
      if ( isX )
        result |= MCTrackInfo::maskAccT3X;
      else
        result |= MCTrackInfo::maskAccT3S;
    }
  }

  void computeAcceptance( std::vector<int>& station );

private:
  Gaudi::Property<bool> m_withVP{this, "WithVP", true};
  Gaudi::Property<bool> m_withUT{this, "WithUT", true};
  Gaudi::Property<bool> m_withFT{this, "WithFT", true};

  DeVP*         m_vpDet = nullptr;
  DeUTDetector* m_utDet = nullptr;
  DeFTDetector* m_ftDet = nullptr;
  std::string   m_utClustersName;
  std::string   m_utHitsName;
};
#endif // BUILDMCTRACKINFO_H
