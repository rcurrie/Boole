/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef FTCLUSTERMONITOR_H
#define FTCLUSTERMONITOR_H 1

// Include files
// from Gaudi
#include "GaudiAlg/Consumer.h"
#include "GaudiAlg/GaudiHistoAlg.h"
#include "GaudiKernel/SystemOfUnits.h"

// from Linker
#include "Associators/Associators.h"

// FTDet
#include "FTDet/DeFTDetector.h"

// LHCbKernel
#include "Kernel/FTChannelID.h"

// from FTEvent
#include "Event/FTCluster.h"
#include "Event/FTLiteCluster.h"

// from MCEvent
#include "Event/MCHit.h"

/** @class FTClusterMonitor FTClusterMonitor.h
 *
 *
 *  @author Eric Cogneras
 *  @date   2012-07-05
 */

class FTClusterMonitor : public Gaudi::Functional::Consumer<void( const LHCb::FTClusters&,
                                                                  // const LHCb::MCHits&,
                                                                  const LHCb::LinksByKey& ),
                                                            Gaudi::Functional::Traits::BaseClass_t<GaudiHistoAlg>> {
public:
  FTClusterMonitor( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override;
  StatusCode finalize() override;
  void       operator()( const LHCb::FTClusters&,
                   // const LHCb::MCHits& mcHits,
                   const LHCb::LinksByKey& links ) const override;

private:
  Gaudi::Property<float> m_minPforResolution{this, "MinPforResolution", 0.0 * Gaudi::Units::GeV,
                                             "Minimum momentum for resolution plots"};

  DeFTDetector* m_deFT = nullptr; ///< pointer to FT detector description
};
#endif // FTCLUSTERMONITOR_H
