/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Units
#include "GaudiKernel/SystemOfUnits.h"

// From GaudiKernel
#include "GaudiKernel/SmartDataPtr.h"

// From PartProp
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"

// local
#include "RichDigiAlgMoni.h"

//-----------------------------------------------------------------------------
// Implementation file for class : RichDigiAlgMoni
//
// 2003-09-08 : Chris Jones   (Christopher.Rob.Jones@cern.ch)
//-----------------------------------------------------------------------------

using namespace Rich::MC::Digi;

DECLARE_COMPONENT( AlgMoni )

// Standard constructor, initializes variables
AlgMoni::AlgMoni( const std::string& name, ISvcLocator* pSvcLocator )
    : HistoAlgBase( name, pSvcLocator ), m_ID( 0 ), m_maxID( 500 ) {
  // Declare job options
  declareProperty( "InputMCRichDigits", m_mcdigitTES = LHCb::MCRichDigitLocation::Default );
  declareProperty( "InputMCRichDeposits", m_mcdepTES = LHCb::MCRichDepositLocation::Default );
  declareProperty( "InputMCRichHits", m_mchitTES = LHCb::MCRichHitLocation::Default );
  declareProperty( "MaxHPDHistos", m_maxID );
}

// Initialisation
StatusCode AlgMoni::initialize() {
  // Initialize base class
  const StatusCode sc = HistoAlgBase::initialize();
  if ( sc.isFailure() ) { return sc; }

  // get tools
  acquireTool( "RichSmartIDTool", m_smartIDTool, nullptr, true );
  acquireTool( "RichMCTruthTool", m_mcTool, nullptr, true );

  // RichDet
  m_richSys = getDet<DeRichSystem>( DeRichLocations::RichSystem );

  // Retrieve particle property service
  LHCb::IParticlePropertySvc* ppSvc = svc<LHCb::IParticlePropertySvc>( "LHCb::ParticlePropertySvc", true );

  // Retrieve particle masses
  m_particleMass[Rich::Unknown]  = 0;
  m_particleMass[Rich::Electron] = ppSvc->find( "e+" )->mass() / Gaudi::Units::MeV;
  m_particleMass[Rich::Muon]     = ppSvc->find( "mu+" )->mass() / Gaudi::Units::MeV;
  m_particleMass[Rich::Pion]     = ppSvc->find( "pi+" )->mass() / Gaudi::Units::MeV;
  m_particleMass[Rich::Kaon]     = ppSvc->find( "K+" )->mass() / Gaudi::Units::MeV;
  m_particleMass[Rich::Proton]   = ppSvc->find( "p+" )->mass() / Gaudi::Units::MeV;

  // release particle property service
  release( ppSvc ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );

  return sc;
}

// Main execution
StatusCode AlgMoni::execute() {
  _ri_debug << "Execute" << endmsg;

  // Maps for number of pe's
  PhotMap ckPhotMapHit;
  PhotMap ckPhotMapDig;
  PhotMap ckPhotMapDep;

  // Locate MCRichDigits
  // ================================================================================
  auto* mcRichDigits = get<LHCb::MCRichDigits>( m_mcdigitTES );
  _ri_debug << "Successfully located " << mcRichDigits->size() << " MCRichDigits at " << m_mcdigitTES << endmsg;

  // PD occupancy
  PDMulti pdMult;

  // Initialise mult counts
  std::vector<unsigned int> digMult( Rich::NRiches, 0 );
  std::vector<unsigned int> hpdCKMult( Rich::NRiches, 0 );

  Rich::Map<const LHCb::RichSmartID, bool> hasHPDQuartzCKBkg;

  // Histogramming
  PD_GLOBAL_POSITIONS;
  const double        maxMult[] = {5000, 2000};
  const Rich::HistoID hid;

  // Loop over all MCRichDigits
  for ( const auto* McDig : *mcRichDigits ) {

    // Smart ID
    const auto id = McDig->key();

    // position for this ID
    Gaudi::XYZPoint point;
    const auto      sc = m_smartIDTool->globalPosition( id, point );
    if ( !sc ) { continue; }

    // RICH
    const auto rich = id.rich();

    // increment digit count
    ++digMult[rich];

    // increment PD multiplicity count
    ++pdMult[id.pdID()];
    if ( McDig->history().hpdQuartzCK() ) {
      ++hpdCKMult[rich];
      hasHPDQuartzCKBkg[id.pdID()] = true;
    }

    // Position plots
    plot1D( point.x(), hid( rich, "digitXglo" ), "digits x global", xMinPDGlo[rich], xMaxPDGlo[rich] );
    plot1D( point.x(), hid( rich, "digitYglo" ), "digits y global", yMinPDGlo[rich], yMaxPDGlo[rich] );
    plot1D( point.x(), hid( rich, "digitZglo" ), "digits z global", zMinPDGlo[rich], zMaxPDGlo[rich] );
    plot2D( point.x(), point.y(), hid( rich, "digitXYglo" ), "digits yVx global", xMinPDGlo[rich], xMaxPDGlo[rich],
            yMinPDGlo[rich], yMaxPDGlo[rich] );
    plot2D( point.z(), point.x(), hid( rich, "digitZXglo" ), "digits xVz global", zMinPDGlo[rich], zMaxPDGlo[rich],
            xMinPDGlo[rich], xMaxPDGlo[rich] );
    plot2D( point.z(), point.y(), hid( rich, "digitZYglo" ), "digits yVz global", zMinPDGlo[rich], zMaxPDGlo[rich],
            yMinPDGlo[rich], yMaxPDGlo[rich] );

    // loop over all hits associated to the digit
    const auto& mcHits = McDig->hits();
    if ( mcHits.empty() ) { warning() << "MCRichDigit " << id << " has no MCRichHits..." << endmsg; }

    // hit mult per digit
    plot1D( mcHits.size(), hid( rich, "hitsPerDig" ), "Hit mult. per Digit", -0.5, 10.5, 11 );

    bool thisDigCounted = false;
    for ( const auto& mchit : mcHits ) {
      const auto* hit = mchit.mcRichHit();

      // Compare digit/hit err for signal hits only
      if ( !hit->isBackground() ) {

        plot1D( point.x() - hit->entry().x(), hid( rich, "digErrX" ), "digit-hit diff X", -10, 10 );
        plot1D( point.y() - hit->entry().y(), hid( rich, "digErrY" ), "digit-hit diff Y", -10, 10 );
        plot1D( point.z() - hit->entry().z(), hid( rich, "digErrZ" ), "digit-hit diff Z", -10, 10 );
        plot1D( std::sqrt( ( point - hit->entry() ).Mag2() ), hid( rich, "digErrR" ), "digit-hit diff R", -10, 10 );

        // Get MCRichOptical Photon
        const auto* mcPhot = m_mcTool->mcOpticalPhoton( hit );
        if ( mcPhot ) {
          // Compare position on HPD entrance window
          const Gaudi::XYZPoint& mcPoint = mcPhot->pdIncidencePoint();
          plot1D( point.X() - mcPoint.X(), hid( rich, "pdImpX" ), "dX on HPD entry window : CK signal only", -30, 30 );
          plot1D( point.Y() - mcPoint.Y(), hid( rich, "pdImpY" ), "dY on HPD entry window : CK signal only", -30, 30 );
          plot1D( point.Z() - mcPoint.Z(), hid( rich, "pdImpZ" ), "dZ on HPD entry window : CK signal only", -30, 30 );
          plot1D( std::sqrt( ( point - mcPoint ).Mag2() ), hid( rich, "pdImpR" ),
                  "dR on HPD entry window : CK signal only", 0, 10 );
        }
      }

      // Count beta=1 PEs
      countNPE( ckPhotMapDig, hit );

      // count digits from charged tracks
      if ( !thisDigCounted && hit->chargedTrack() ) { thisDigCounted = true; }

    } // end hits loop

  } // MCRichDigits Loop

  // Fill multiplicity plots

  plot1D( digMult[Rich::Rich1], hid( Rich::Rich1, "digitMult" ), "Overall Digit RICH1 Occupancy", 0,
          maxMult[Rich::Rich1] );
  plot1D( digMult[Rich::Rich2], hid( Rich::Rich2, "digitMult" ), "Overall Digit RICH2 Occupancy", 0,
          maxMult[Rich::Rich2] );

  plot1D( hpdCKMult[Rich::Rich1], hid( Rich::Rich1, "hpdQCKDigMult" ), "HPD Quartz CK Digit RICH1 Occupancy", 0,
          maxMult[Rich::Rich1] );
  plot1D( hpdCKMult[Rich::Rich2], hid( Rich::Rich2, "hpdQCKDigMult" ), "HPD Quartz CK Digit RICH2 Occupancy", 0,
          maxMult[Rich::Rich2] );

  // Fill HPD occupancy plots
  for ( const auto& occ : pdMult ) {
    plot1D( occ.second, hid( ( occ.first ).rich(), "digPerPD" ), "Digits per HPD (nDigits>0)", -0.5, 200.5, 201 );
    std::ostringstream HPD;
    HPD << occ.first;
    plot1D( occ.second, hid( occ.first.rich(), "hpdOcc/" + (std::string)m_richSys->hardwareID( occ.first ) ),
            HPD.str() + " Digits (nDigits>0)", -0.5, 200.5, 201 );
    if ( hasHPDQuartzCKBkg[occ.first] ) {
      plot1D( occ.second,
              hid( occ.first.rich(), "hpdOccWithHPDQCK/" + (std::string)m_richSys->hardwareID( occ.first ) ),
              HPD.str() + " Digits (nDigits>0 and nHPDQCK>0)", -0.5, 200.5, 201 );
    }
  }

  // Locate MCRichDeposits
  // ================================================================================
  SmartDataPtr<LHCb::MCRichDeposits> deps( eventSvc(), m_mcdepTES );
  if ( !deps ) {
    _ri_debug << "Cannot locate MCRichDeposits at " << m_mcdepTES << endmsg;
  } else {
    _ri_debug << "Successfully located " << deps->size() << " MCRichDeposits at " << m_mcdepTES << endmsg;

    std::vector<unsigned int> nChargedTracks( Rich::NRiches, 0 );

    // map of backgrounds in each HPD
    PartMap pmap1, pmap2, pmap3;

    // Loop over deposits
    for ( const auto* dep : *deps ) {

      // Which RICH ?
      const auto rich = dep->parentHit()->rich();

      // Count beta=1 PEs
      countNPE( ckPhotMapDep, dep->parentHit() );

      // count hits from charged tracks
      if ( dep->parentHit()->chargedTrack() ) ++nChargedTracks[rich];

      // study hpd quartz window backgrounds
      if ( dep->history().hpdQuartzCK() ) {
        // key for this hit HPD and MCParticle pair
        const PartKey key( dep->smartID().pdID(), dep->parentHit()->mcParticle() );
        // add pointer to deposit to vector for this key
        pmap1[key].push_back( dep );
        if ( msgLevel( MSG::VERBOSE ) ) {
          const auto locpoint = m_smartIDTool->globalToPDPanel( dep->parentHit()->entry() );
          verbose() << dep->smartID() << " glo=" << dep->parentHit()->entry() << " loc=" << locpoint
                    << " mcp=" << dep->parentHit()->mcParticle() << endmsg;
        }
      }

      // study gas quartz window backgrounds
      if ( dep->history().gasQuartzCK() ) {
        // key for this hit HPD and MCParticle pair
        const PartKey key( dep->smartID().pdID(), dep->parentHit()->mcParticle() );
        pmap2[key].push_back( dep );
      }

      // HPD Reflections
      if ( dep->history().hpdReflection() ) {
        // key for this hit HPD
        const PartKey key( dep->smartID().pdID(), nullptr );
        pmap3[key].push_back( dep );
      }

    } // MCRichDeposits loop

    // fill background plots
    fillHPDPlots( pmap1, "HPDQuartzBkg", "HPD Quartz Window CK" );
    fillHPDPlots( pmap2, "GasQuartzBkg", "Gas Quartz Window CK" );
    fillHPDPlots( pmap3, "HPDReflectionBkg", "HPD Internal reflections" );

  } // MCRichDeposits exist

  // Locate MCRichHits
  // ================================================================================
  SmartDataPtr<LHCb::MCRichHits> hits( eventSvc(), m_mchitTES );
  if ( !hits ) {
    _ri_debug << "Cannot locate MCRichHits at " << m_mchitTES << endmsg;
  } else {

    // Hit mult counters
    std::vector<unsigned int> hitMult( Rich::NRiches, 0 );
    std::vector<unsigned int> hpdQCKMult( Rich::NRiches, 0 );

    // Loop over hits
    for ( const auto* hit : *hits ) {

      // Which RICH ?
      const auto rich = hit->rich();

      if ( trueCKHit( hit ) ) {
        plot1D( hit->timeOfFlight(), hid( rich, "tofHitSignal" ), "Signal MCRichHit TOF", -100, 100 );
        plot1D( hit->energy(), hid( rich, "enHitSignal" ), "Signal MCRichHit Energy", 0, 100 );
      } else {
        plot1D( hit->timeOfFlight(), hid( rich, "tofHitBkgrd" ), "Background MCRichHit TOF", -100, 100 );
        plot1D( hit->energy(), hid( rich, "enHitBkgrd" ), "Background MCRichHit Energy", 0, 100 );
      }

      // increment hit count
      ++hitMult[rich];
      if ( hit->hpdQuartzCK() ) { ++hpdQCKMult[rich]; }

      // Increment PES count for high beta tracks
      countNPE( ckPhotMapHit, hit );

      // Plot hit positions
      const auto& point = hit->entry();
      plot1D( point.x(), hid( rich, "hitXGlo" ), "mchits x global", xMinPDGlo[rich], xMaxPDGlo[rich] );
      plot1D( point.y(), hid( rich, "hitYGlo" ), "mchits y global", yMinPDGlo[rich], yMaxPDGlo[rich] );
      plot1D( point.z(), hid( rich, "hitZGlo" ), "mchits z global", zMinPDGlo[rich], zMaxPDGlo[rich] );
      plot2D( point.x(), point.y(), hid( rich, "hitXYglo" ), "mchits yVx global", xMinPDGlo[rich], xMaxPDGlo[rich],
              yMinPDGlo[rich], yMaxPDGlo[rich] );
      plot2D( point.z(), point.x(), hid( rich, "hitZXglo" ), "mchits xVz global", zMinPDGlo[rich], zMaxPDGlo[rich],
              xMinPDGlo[rich], xMaxPDGlo[rich] );
      plot2D( point.z(), point.y(), hid( rich, "hitZYglo" ), "mchits yVz global", zMinPDGlo[rich], zMaxPDGlo[rich],
              yMinPDGlo[rich], yMaxPDGlo[rich] );
    }

    // Fill multiplicity plots

    plot1D( hitMult[Rich::Rich1], hid( Rich::Rich1, "hitMult" ), "Overall MCRichHit RICH1 Occupancy", 0,
            maxMult[Rich::Rich1] );
    plot1D( hitMult[Rich::Rich2], hid( Rich::Rich2, "hitMult" ), "Overall MCRichHit RICH2 Occupancy", 0,
            maxMult[Rich::Rich2] );

    plot1D( hpdQCKMult[Rich::Rich1], hid( Rich::Rich1, "hpdQCKHitMult" ), "HPD Quartz CK MCRichHit RICH1 Occupancy", 0,
            maxMult[Rich::Rich1] );
    plot1D( hpdQCKMult[Rich::Rich2], hid( Rich::Rich2, "hpdQCKHitMult" ), "HPD Quartz CK MCRichHit RICH2 Occupancy", 0,
            maxMult[Rich::Rich2] );

  } // MCRichHits exist

  // Loop over counted PES for MCRichHits
  for ( const auto& phot : ckPhotMapHit ) {
    plot1D( phot.second, hid( phot.first.second, "npesHit" ), "# MCRichHit p.e.s : beta=1", -0.5, 100.5, 101 );
  }

  // Loop over counted PES for MCRichDeposits
  for ( const auto& phot : ckPhotMapDep ) {
    plot1D( phot.second, hid( phot.first.second, "npesDep" ), "# MCRichDeposit p.e.s : beta=1", -0.5, 100.5, 101 );
  }

  // Loop over counted PES for MCRichDigits
  for ( const auto& phot : ckPhotMapDig ) {
    const auto digCount = phot.second;
    plot1D( digCount, hid( phot.first.second, "npesDig" ), "# MCRichDigit p.e.s : beta=1", -0.5, 100.5, 101 );

    // locate the entry for the MCRichDeps
    const auto depCount = ckPhotMapDep[phot.first];

    const auto frac = ( depCount != 0 ? digCount / depCount : 0 );
    plot1D( frac, hid( phot.first.second, "npesRetained" ), "Retained p.e.s : beta=1", 0, 1 );
  }

  return StatusCode::SUCCESS;
}

void AlgMoni::fillHPDPlots( const PartMap& pmap, const std::string& plotsDir, const std::string& plotsName ) {
  // histogramming
  PD_LOCAL_POSITIONS_X;
  PD_LOCAL_POSITIONS_Y;
  const Rich::HistoID hid;

  // count for each RICH
  std::vector<unsigned int> nHPDs( Rich::NRiches, 0 );

  _ri_debug << "Filling plots for " << plotsName << endmsg;

  // plots for each HPD
  for ( const auto& P : pmap ) {
    // SmartID for this HPD
    const auto hpdID = P.first.first;
    // increment count for RICH
    ++nHPDs[hpdID.rich()];
    // Centre point of HPD in local coords
    Gaudi::XYZPoint hpdGlo;
    const auto      sc = m_smartIDTool->pdPosition( hpdID, hpdGlo );
    if ( !sc ) continue;
    const auto hpdLoc = m_smartIDTool->globalToPDPanel( hpdGlo );
    // create histo title
    std::ostringstream HPD;
    HPD << plotsName << " " << hpdID << " " << hpdGlo;
    // histo ID
    std::string Hid = plotsDir + "/" + boost::lexical_cast<std::string>( m_ID++ );
    // loop over deposits
    Rich::Map<LHCb::RichSmartID, bool> uniqueIDs;
    _ri_debug << "Found " << P.second.size() << " Deposits for " << Hid << " " << HPD.str() << endmsg;
    for ( const auto* D : P.second ) {
      uniqueIDs[D->smartID()] = true;
      // hit point on silicon in local coords
      const auto hitP = m_smartIDTool->globalToPDPanel( D->parentHit()->entry() ) - hpdLoc;
      // fill single event HPD plot
      if ( m_ID < m_maxID ) {
        plot2D( hitP.X(), hitP.Y(), Hid, HPD.str(), -8 * Gaudi::Units::mm, 8 * Gaudi::Units::mm, -8 * Gaudi::Units::mm,
                8 * Gaudi::Units::mm, 32, 32 );
      }
      // Intergrated plot
      plot2D( hitP.X(), hitP.Y(), plotsDir + "/" + "intBkg", "Integrated " + plotsName, -8 * Gaudi::Units::mm,
              8 * Gaudi::Units::mm, -8 * Gaudi::Units::mm, 8 * Gaudi::Units::mm, 32, 32 );
    }
    // plot number of hits
    plot1D( P.second.size(), plotsDir + "/" + hid( hpdID.rich(), "/nHitsPerHPD" ).id(),
            "# hits per HPD from " + plotsName, 0.5, 200.5, 100 );
    // plot number of digits
    plot1D( uniqueIDs.size(), plotsDir + "/" + hid( hpdID.rich(), "/nDigsPerHPD" ).id(),
            "# digits per HPD from " + plotsName, 0.5, 200.5, 100 );
    // position of HPD
    plot2D( hpdLoc.X(), hpdLoc.Y(), plotsDir + "/" + hid( hpdID.rich(), "hitHPDs" ).id(), "HPD Hit Map",
            xMinPDLoc[hpdID.rich()], xMaxPDLoc[hpdID.rich()], yMinPDLoc[hpdID.rich()], yMaxPDLoc[hpdID.rich()] );
  }

  // number of affected HPD plots
  plot1D( nHPDs[Rich::Rich1], plotsDir + "/" + hid( Rich::Rich1, "/nHPDsPerEv" ).id(),
          "# RICH1 HPDs per event with " + plotsName, -0.5, 20.5, 21 );
  plot1D( nHPDs[Rich::Rich2], plotsDir + "/" + hid( Rich::Rich2, "/nHPDsPerEv" ).id(),
          "# RICH2 HPDs per event with " + plotsName, -0.5, 20.5, 21 );
}

void AlgMoni::countNPE( PhotMap& photMap, const LHCb::MCRichHit* hit ) {

  // MCParticle momentum
  const auto tkPtot = momentum( hit->mcParticle() );

  // Parent particle hypothesis
  const auto mcid = m_mcTool->mcParticleType( hit->mcParticle() );

  // Which radiator
  const auto rad = ( Rich::RadiatorType )( hit->radiator() );

  // Increment PES count for high beta tracks
  if ( tkPtot > 1 * Gaudi::Units::GeV && mcid != Rich::Unknown && mcid != Rich::Electron && hit->isSignal() &&
       mcBeta( hit->mcParticle() ) > 0.99 ) {
    PhotPair pairC( hit->mcParticle(), rad );
    ++photMap[pairC];
  }
}
