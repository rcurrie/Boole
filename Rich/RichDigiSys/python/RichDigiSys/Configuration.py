###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

## @package RichDigiSys
#  High level Configuration tools for RICH Digitisation
#  @author Chris Jones  (Christopher.Rob.Jones@cern.ch)
#  @date   24/10/2008

__author__ = "Chris Jones <Christopher.Rob.Jones@cern.ch>"

from LHCbKernel.Configuration import *
from Configurables import (
    Rich__MC__Digi__Signal, Rich__MC__Digi__CopySummedDepositsToDigits,
    Rich__MC__Digi__SimpleChargeSharing,
    Rich__MC__Digi__DetailedFrontEndResponse,
    Rich__MC__Digi__DetailedFrontEndResponsePMT,
    Rich__MC__Digi__SimpleFrontEndResponse,
    Rich__MC__Digi__MCRichDigitsToRawBufferAlg, Rich__MC__Digi__SummedDeposits)

# ----------------------------------------------------------------------------------


## @class RichDigiSysConf
#  Configurable for RICH digitisation
#  @author Chris Jones  (Christopher.Rob.Jones@cern.ch)
#  @date   24/10/2008
class RichDigiSysConf(LHCbConfigurableUser):

    ## Steering options
    __slots__ = {
        "UseSpillover": True,
        "UseLHCBackground": False,
        "ChargeShareFraction": 0.025,
        "ResponseModel": "Detailed",
        "ChargeShareModel": "Simple",
        "RawDataFormatVersion": 3,
        "Sequencer": None,
        "OutputLevel": INFO,
        "Threshold": 0.125,
        "SpilloverModel": "None",
        "DataType": "",
        "GainMean": 0.9,
        "GainRms": 0.2,
        "ReadParametersFromDB": True,
        "Sin": True,
        "SinProbTimeRange": 25.0,
        "SinTimeWindowRange": 100.0,
        "SinTimeWindowBegin": [-35.00, -5.00],
        "TimeGate": True,
        "TimeGateWindowBegin": [18.00, 48.00],
        "TimeGateLookupTableR1": [],
        "TimeGateLookupTableR2": [],
        "TestRawFormatDecoding": False
    }

    def makeComponent(self, type, name):
        c = type(name)
        if self.isPropertySet("OutputLevel"):
            c.OutputLevel = self.getProp("OutputLevel")
        return c

    ## @brief Apply the configuration to the given GaudiSequencer
    #  @param sequence The GaudiSequencer to add the RICH digitisation to
    def applyConf(self):

        if self.getProp("DataType") == "Upgrade":
            #default behaviour for RICH Upgrade
            if not self.isPropertySet("ChargeShareModel"):
                self.ChargeShareModel = "None"
            if not self.isPropertySet("ResponseModel"):
                self.ResponseModel = "DetailedPMT"
            if not self.isPropertySet("OutputLevel"):
                self.OutputLevel = INFO
            if not self.isPropertySet("RawDataFormatVersion"):
                self.RawDataFormatVersion = 3
            #if not self.isPropertySet("UseSpillover"):
            #    self.UseSpillover = False
            if self.getProp("UseSpillover") == True:
                if not self.isPropertySet("SpilloverModel"):
                    self.SpilloverModel = "PrevNext"

        sequence = self.getProp("Sequencer")

        # Process the raw hit signals.
        pdSignals = self.makeComponent(Rich__MC__Digi__Signal, "RichPDSignal")
        pdSignals.UseSpillover = self.getProp("UseSpillover")
        pdSignals.UseLHCBackground = self.getProp("UseLHCBackground")
        if self.getProp("ResponseModel") == "DetailedPMT":
            pdSignals.TimeCalib = [0, 0]
        sequence.Members += [pdSignals]

        # Charge Sharing
        chargeShareModel = self.getProp("ChargeShareModel")
        if chargeShareModel == "Simple":
            chargeShare = self.makeComponent(
                Rich__MC__Digi__SimpleChargeSharing, "RichChargeShare")
            chargeShare.ChargeShareFraction = self.getProp(
                "ChargeShareFraction")
            sequence.Members += [chargeShare]
        elif chargeShareModel == "None":
            pass
        else:
            raise RuntimeError("ERROR : Unknown PD charge sharing model '%s'" %
                               chargeShareModel)

        # deposit summation
        summedDeposits = self.makeComponent(Rich__MC__Digi__SummedDeposits,
                                            "RichSummedDeposits")
        if self.getProp("ResponseModel") == "DetailedPMT":
            summedDeposits.SimpleSpilloverForHPD = False
        sequence.Members += [summedDeposits]

        # PD response
        pdModel = self.getProp("ResponseModel")
        if pdModel == "Detailed":
            response = self.makeComponent(
                Rich__MC__Digi__DetailedFrontEndResponse, "RichPDResponse")
            response.TimeCalib = [10, 6]
        elif pdModel == "Simple":
            response = self.makeComponent(
                Rich__MC__Digi__SimpleFrontEndResponse, "RichPDResponse")
        elif pdModel == "Copy":
            response = self.makeComponent(
                Rich__MC__Digi__CopySummedDepositsToDigits, "RichPDResponse")
        elif pdModel == "DetailedPMT":
            response = self.makeComponent(
                Rich__MC__Digi__DetailedFrontEndResponsePMT, "RichPDResponse")
            response.Threshold = self.getProp("Threshold")
            response.SpilloverModel = self.getProp("SpilloverModel")
            response.GainMean = self.getProp("GainMean")
            response.GainRms = self.getProp("GainRms")
            response.ReadParametersFromDB = self.getProp(
                "ReadParametersFromDB")
            response.Sin = self.getProp("Sin")
            response.SinProbTimeRange = self.getProp("SinProbTimeRange")
            response.SinTimeWindowRange = self.getProp("SinTimeWindowRange")
            response.SinTimeWindowBegin = self.getProp("SinTimeWindowBegin")
            response.TimeGate = self.getProp("TimeGate")
            response.TimeGateWindowBegin = self.getProp("TimeGateWindowBegin")
            response.TimeGateLookupTableR1 = self.getProp(
                "TimeGateLookupTableR1")
            response.TimeGateLookupTableR2 = self.getProp(
                "TimeGateLookupTableR2")

        else:
            raise RuntimeError(
                "ERROR : Unknown PD Response model '%s'" % pdModel)
        sequence.Members += [response]

        # Fill the Raw Event
        rawEvtFill = self.makeComponent(
            Rich__MC__Digi__MCRichDigitsToRawBufferAlg, "RichFillRawBuffer")
        rawEvtFill.DataVersion = self.getProp("RawDataFormatVersion")
        sequence.Members += [rawEvtFill]

        if self.getProp("TestRawFormatDecoding"):
            from Configurables import Rich__Future__RawBankDecoder as RichDecoder
            testDecode = self.makeComponent(RichDecoder, "RichDecodeTest")
            sequence.Members += [testDecode]
