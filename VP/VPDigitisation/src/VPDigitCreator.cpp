/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/MCVPDigit.h"
#include "Event/VPDigit.h"
#include "GaudiAlg/GaudiTupleAlg.h"
#include "GaudiAlg/Transformer.h"
#include "GaudiKernel/RndmGenerators.h"
#include "Kernel/VPConstants.h"
#include "Kernel/VPDataFunctor.h"
#include <map>
#include <numeric>

using namespace LHCb;

/** @class VPDigitCreator VPDigitCreator.h
 *  Using MCVPDigits as input, this algorithm simulates the response of the
 *  VeloPix ASIC and produces a VPDigit for each MCVPDigit above threshold.
 *
 *  @author Thomas Britton
 *  @date   2010-07-07
 */

class VPDigitCreator : public Gaudi::Functional::Transformer<LHCb::VPDigits( const LHCb::MCVPDigits& ),
                                                             Gaudi::Functional::Traits::BaseClass_t<GaudiTupleAlg>> {

public:
  /// Standard constructor
  VPDigitCreator( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode     initialize() override; ///< Algorithm initialization
  LHCb::VPDigits operator()( const LHCb::MCVPDigits& mcdigits ) const override;

private:
  /// Discrimination threshold in number of electrons
  Gaudi::Property<double> m_threshold{this, "ChargeThreshold", 1000.0};

  /// Noise in number of electrons
  Gaudi::Property<double> m_electronicNoise{this, "ElectronicNoise", 130.0};

  /// Window in which pixel rise time is registered
  Gaudi::Property<double> m_timingWindow{this, "TimingWindow", 25.};

  /// Offset from z/c arrival that timing window starts
  Gaudi::Property<double> m_timingOffset{this, "TimingOffset", 0.};

  /// Option to simulate masked pixels // Simulate masked pixels or not
  Gaudi::Property<bool> m_maskedPixels{this, "MaskedPixels", false};

  /// Option to simulate noisy pixels // Simulate noisy pixels or not
  Gaudi::Property<bool> m_noisyPixels{this, "NoisyPixels", false};

  /// Fraction of masked pixels // Fraction of masked pixels
  Gaudi::Property<double> m_fractionMasked{this, "FractionMasked", 0.0};

  /// Fraction of noisy pixels // Fraction of noisy pixels
  Gaudi::Property<double> m_fractionNoisy{this, "FractionNoisy", 0.0};

  /// Number of signal VPDigits created
  mutable Gaudi::Accumulators::StatCounter<> m_numSignalDigitsCreated = {this, "1 Signal VPDigits"};

  /// Number of noise VPDigits created
  mutable Gaudi::Accumulators::StatCounter<> m_numNoiseDigitsCreated = {this, "2 Noise VPDigits"};

  /// Number of VPDigits missed due to masked pixels
  mutable Gaudi::Accumulators::StatCounter<> m_numDigitsKilledMasked = {this, "3 Lost Random bad pixels"};

  /// Number of VPDigits created due to spillover
  mutable Gaudi::Accumulators::StatCounter<> m_numSpillover = {this, "4 Spillover VPDigits"};

  /// Number of VPDigits lost due to timing window
  mutable Gaudi::Accumulators::StatCounter<> m_numLostTiming = {this, "5 Out of time VPDigits"};

  /// Random number generators
  mutable Rndm::Numbers m_gauss;
  mutable Rndm::Numbers m_uniform;
  mutable Rndm::Numbers m_poisson;
};

DECLARE_COMPONENT( VPDigitCreator )

//=============================================================================
// Standard constructor
//=============================================================================
VPDigitCreator::VPDigitCreator( const std::string& name, ISvcLocator* pSvcLocator )
    : Transformer{name,
                  pSvcLocator,
                  {"InputLocation", LHCb::MCVPDigitLocation::Default},
                  {"OutputLocation", LHCb::VPDigitLocation::Default}} {}

//=============================================================================
// Initialization
//=============================================================================
StatusCode VPDigitCreator::initialize() {
  return GaudiAlgorithm::initialize()
      .andThen( [&] { return m_gauss.initialize( randSvc(), Rndm::Gauss( 0., 1. ) ); } )
      .andThen( [&] { return m_uniform.initialize( randSvc(), Rndm::Flat( 0., 1. ) ); } )
      .andThen( [&] {
        StatusCode sc( StatusCode::SUCCESS );
        if ( !m_noisyPixels ) return sc; // nothing else to do
        double averageNoisy = m_fractionNoisy * VP::NSensors * VP::NPixelsPerSensor;
        if ( averageNoisy < 1 ) {
          m_noisyPixels = false;
          return sc;
        }
        return m_poisson.initialize( randSvc(), Rndm::Poisson( averageNoisy ) );
      } )
      .andThen( [&] { setHistoTopDir( "VP/" ); } );
}

//=============================================================================
// Main execution
//=============================================================================
LHCb::VPDigits VPDigitCreator::operator()( const LHCb::MCVPDigits& mcdigits ) const {

  // Create a container for the digits and transfer ownership to the TES.
  LHCb::VPDigits digits;
  unsigned int   numSpillover           = 0;
  unsigned int   numLostTiming          = 0;
  unsigned int   numDigitsKilledMasked  = 0;
  unsigned int   numSignalDigitsCreated = 0;
  unsigned int   numNoiseDigitsCreated  = 0;

  // Loop over the MC digits.
  for ( const LHCb::MCVPDigit* mcdigit : mcdigits ) {
    // Sum up the collected charge from all deposits, in the time windows
    double     chargeMain     = 0.;
    const auto chargeAndTimes = mcdigit->depositAndTimes();
    for ( unsigned int iDep = 0; iDep < chargeAndTimes.size(); ++iDep ) {
      auto depTime = chargeAndTimes[iDep].second + m_timingOffset;
      if ( depTime > 0. && depTime < m_timingWindow ) {
        // is this in the main bunch
        chargeMain += chargeAndTimes[iDep].first;
        // if no MCHit then assume spillover
        if ( !mcdigit->mcHits()[iDep].data() ) ++numSpillover;
      } else {
        // if has MCHit but out of time window
        if ( mcdigit->mcHits()[iDep].data() ) ++numLostTiming;
      }
    }
    // Check if the collected charge exceeds the threshold.
    if ( chargeMain < m_threshold + m_electronicNoise * m_gauss() ) continue;
    const LHCb::VPChannelID channel = mcdigit->channelID();

    if ( m_maskedPixels ) {
      // Draw a random number to choose if this pixel is masked.
      if ( m_uniform() < m_fractionMasked ) {
        ++numDigitsKilledMasked;
        continue;
      }
    }
    // Create a new digit.
    digits.insert( new LHCb::VPDigit(), channel );
    ++numSignalDigitsCreated; // all digits from tracks (in any bunch)
  }
  if ( m_noisyPixels ) {
    const auto numToAdd = m_poisson();
    // Add additional digits without a corresponding MC digit.
    for ( unsigned int i = 0; i < numToAdd; ++i ) {
      // Sample the sensor, chip, column and row of the noise pixel.
      const int         sensor = int( floor( m_uniform() * VP::NSensors ) );
      const int         chip   = int( floor( m_uniform() * VP::NChipsPerSensor ) );
      const int         col    = int( floor( m_uniform() * VP::NColumns ) );
      const int         row    = int( floor( m_uniform() * VP::NRows ) );
      LHCb::VPChannelID channel( sensor, chip, col, row );
      // Make sure the channel has not yet been added to the container.
      if ( digits.object( channel ) ) continue;
      digits.insert( new LHCb::VPDigit(), channel );
      ++numNoiseDigitsCreated;
    }
  }

  // add the values from this event to the running totals
  m_numSpillover += numSpillover;
  m_numLostTiming += numLostTiming;
  m_numDigitsKilledMasked += numDigitsKilledMasked;
  m_numSignalDigitsCreated += ( numSignalDigitsCreated - numSpillover );
  m_numNoiseDigitsCreated += numNoiseDigitsCreated;

  std::sort( digits.begin(), digits.end(), VPDataFunctor::Less_by_Channel<const LHCb::VPDigit*>() );
  return digits;
}
