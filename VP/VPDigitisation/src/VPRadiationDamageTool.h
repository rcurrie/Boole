/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/Point3DTypes.h"
// Det/VPDet
#include "VPDet/DeVP.h"

static const InterfaceID IID_VPRadiationDamageTool( "VPRadiationDamageTool", 1, 0 );

/** @class VPRadiationDamageTool VPRadiationDamageTool.h
 *
 */

class VPRadiationDamageTool : public GaudiTool {

public:
  /// Return the interface ID
  static const InterfaceID& interfaceID() { return IID_VPRadiationDamageTool; }

  /// Standard constructor
  VPRadiationDamageTool( const std::string& type, const std::string& name, const IInterface* parent );

  StatusCode initialize() override;

  /// Fluence as function of position and integrated luminosity (in fb-1).
  double fluence( const Gaudi::XYZPoint& point, const double lint ) const;
  /// Charge collection efficiency as function of 1 MeV n eq. fluence and bias.
  double chargeCollectionEfficiency( const double f, const double v ) const;

private:
  DeVP*               m_det;
  std::vector<double> m_a;
  std::vector<double> m_k;
};
