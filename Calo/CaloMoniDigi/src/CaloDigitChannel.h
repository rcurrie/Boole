/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CaloDigitChannel_H
#define CaloDigitChannel_H 1
//
// Include files

// from STL

#include <cmath>
#include <string>
#include <vector>
// From Gaudi

#include "GaudiAlg/GaudiTupleAlg.h"

#include "CaloDet/DeCalorimeter.h"

/** @class CaloDigitChannel CaloDigitChannel.h
 *
 *  Evaluate the Energy in the Calo Channel by Channel
 *
 *  @author Yasmine AMHIS
 *  @date   2007-03-28
 */

class CaloDigitChannel : public GaudiTupleAlg {
public:
  CaloDigitChannel( const std::string& name, ISvcLocator* pSvcLocator );
  virtual ~CaloDigitChannel();      ///< Destructor
  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution

private:
  DeCalorimeter* m_calo;

  std::string m_nameOfDetector; // Detector short name
  std::string m_inputData;      ///< Input container
};
#endif // CaloDigitChannel
