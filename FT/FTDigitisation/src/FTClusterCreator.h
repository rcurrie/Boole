/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef FTCLUSTERCREATOR_H
#define FTCLUSTERCREATOR_H 1

// from Gaudi
#include "GaudiAlg/Transformer.h"

// from Linker
#include "Associators/Associators.h"

// LHCbKernel
#include "Kernel/FTChannelID.h"

// from FTEvent
#include "Event/FTCluster.h"
#include "Event/FTLiteCluster.h"
#include "Event/MCFTDigit.h"

/** @class FTClusterCreator FTClusterCreator.h
 *
 *
 *  @author Eric Cogneras
 *  @date   2012-04-06
 */

class FTClusterCreator
    : public Gaudi::Functional::MultiTransformer<
          std::tuple<LHCb::FTClusters, LHCb::LinksByKey, LHCb::LinksByKey, LHCb::LinksByKey, LHCb::LinksByKey>(
              const LHCb::MCFTDigits& )> {
public:
  FTClusterCreator( const std::string& name, ISvcLocator* pSvcLocator );

  std::tuple<LHCb::FTClusters, LHCb::LinksByKey, LHCb::LinksByKey, LHCb::LinksByKey, LHCb::LinksByKey>
  operator()( const LHCb::MCFTDigits& digits ) const override;

private:
  // Job options
  // For monitoring
  Gaudi::Property<bool> m_storePECharge{this, "StorePECharge", true, "Flag to store PE instead of ADC in FTCluster"};

  // cluster size options
  Gaudi::Property<unsigned int> m_clusterMaxWidth{this, "ClusterMaxWidth", 4, "Maximal cluster width"};
  Gaudi::Property<float>        m_lowestFraction{this, "LowestFraction", -0.250,
                                          "The fraction is defined in the range (-0.250,0.750)"};

  // Threshold settings.
  Gaudi::Property<bool> m_usePEnotADC{
      this, "UsePENotADC", false, "Flag to use (float)PE instead of (int)ADC. Thresholds should be set accordingly"};

  Gaudi::Property<float> m_adcThreshold1{this, "ADCThreshold1", 1, "add-to-cluster threshold"};
  Gaudi::Property<float> m_adcThreshold2{this, "ADCThreshold2", 2, "seed threshold"};
  Gaudi::Property<float> m_adcThreshold3{this, "ADCThreshold3", 3, "single-channel threshold"};
  Gaudi::Property<float> m_adcThreshold1Weight{this, "ADCThreshold1Weight", 1., "add-to-cluster weight"};
  Gaudi::Property<float> m_adcThreshold2Weight{this, "ADCThreshold2Weight", 2., "seed weight"};
  Gaudi::Property<float> m_adcThreshold3Weight{this, "ADCThreshold3Weight", 6., "single-channel weight"};

  // type to have pair of digits and the flag for cluster candidates
  enum clusterFlag {
    small,          // 0
    largeEdgeFirst, // 1
    largeMiddle,    // 2
    largeEdgeSecond // 3
  };

  typedef std::pair<std::vector<LHCb::MCFTDigit*>, clusterFlag> clusterCandidate;
  typedef std::vector<clusterCandidate>                         clusterCandidates;

  // loop over digits to find cluster candidates, flagged them for the size
  // large clusters are cutted at max size but not fragmented
  void findClusterCandidates( const LHCb::MCFTDigits& digits, clusterCandidates& candidates ) const;

  // loop over cluster candidates, check cluster thresholds
  void makeClusters( clusterCandidates& candidates, LHCb::FTClusters& clusterCont ) const;

  // function to check either ADC or photoElectrons depending on m_usePEnotADC
  // This is only to reduce amount of code but it's same amount of work
  // usePEnotADC will only be used for monitoring purposes and test beam studies
  // There should be a way to avoid this check for every digit
  inline float charge( const LHCb::MCFTDigit* digit ) const {
    return m_usePEnotADC ? digit->photoElectrons() : digit->adcCount();
  };
};

#endif // FTCLUSTERCREATOR_H
