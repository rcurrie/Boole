/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "DetDesc/IGeometryInfo.h"
#include "Event/MCMuonDigit.h"
#include "Event/MuonDigit.h"
#include "GaudiAlg/GaudiAlgorithm.h"
#include "GaudiKernel/RndmGenerators.h"
#include "MuonCardiacChannelOutput.h"
#include "MuonChamberResponse.h"
#include "MuonDet/DeMuonDetector.h"
#include "MuonDet/MuonBasicGeometry.h"
#include "MuonDetectorResponse.h"
#include "MuonDigitizationData.h"
#include "MuonPhPreInput.h"
#include "MuonPhyChannelInput.h"
#include "MuonPhysicalChannel.h"
#include "MuonPhysicalChannelOutput.h"
#include <algorithm>
#include <boost/utility/string_ref.hpp>
#include <cmath>
#include <optional>
#include <string>
#include <vector>
using namespace std::string_literals;

class MuonDigitization : public GaudiAlgorithm {

public:
  MuonDigitization( const std::string& name, ISvcLocator* pSvcLocator );
  StatusCode initialize() override;
  StatusCode execute() override;
  StatusCode finalize() override;

private:
  void createInput( MuonDigitizationData<MuonPhyChannelInput>& PhyChaInput );
  void elaborateMuonPhyChannelInputs( MuonDigitizationData<MuonPhyChannelInput> const& PhyChaInput,
                                      MuonDigitizationData<MuonPhysicalChannel>&       PhysicalChannel );
  void applyPhysicalEffects( MuonDigitizationData<MuonPhysicalChannel>& PhysicalChannel );
  void fillPhysicalChannel( MuonDigitizationData<MuonPhysicalChannel> const& PhysicalChannel,
                            MuonDigitizationData<MuonPhysicalChannelOutput>& PhysicalChannelOutput );
  void fillCardiacChannel( MuonDigitizationData<MuonPhysicalChannelOutput> const& PhysicalChannel,
                           MuonDigitizationData<MuonCardiacChannelOutput>&        PhysicalChannelOutput );
  void createLogicalChannel( MuonDigitizationData<MuonPhysicalChannelOutput> const& PhyChaOutput,
                             LHCb::MCMuonDigits&                                    mcDigitContainer );
  void createLogicalChannel( MuonDigitizationData<MuonCardiacChannelOutput> const& PhyChaOutput,
                             LHCb::MCMuonDigits&                                   mcDigitContainer );

  void createRAWFormat( LHCb::MCMuonDigits const& mcDigitContainer, LHCb::MuonDigits& digitContainer );
  void addChamberNoise();
  void addElectronicNoise( MuonDigitizationData<MuonPhysicalChannel>& PhysicalChannel );

  static constexpr int    m_numberOfEvents{4 + 1};
  static constexpr int    m_numberOfEventsNeed{5};
  static constexpr int    m_container{4};
  double                  m_timeBin{};
  Gaudi::Property<double> m_BXTime{this, "BXTime", 25.0, [this]( auto& ) { m_timeBin = m_BXTime.value() / 0b10000; },
                                   Gaudi::Details::Property::ImmediatelyInvokeHandler{true}};
  Gaudi::Property<double> m_gate{this, "TimeGate", 25.0};
  Gaudi::Property<bool>   m_applyTimeJitter{this, "ApplyTimeJitter", true};
  Gaudi::Property<bool>   m_applyChamberNoise{this, "ApplyChamberNoise", false};
  Gaudi::Property<bool>   m_applyElectronicNoise{this, "ApplyElectronicNoise", true};
  Gaudi::Property<bool>   m_applyXTalk{this, "ApplyXTalk", true};
  Gaudi::Property<bool>   m_applyEfficiency{this, "ApplyEfficiency", true};
  Gaudi::Property<bool>   m_applyDeadtime{this, "ApplyDeadtime", true};
  Gaudi::Property<bool>   m_applyDialogDeadtime{this, "ApplyDialogDeadtime", true};
  Gaudi::Property<bool>   m_applyTimeAdjustment{this, "ApplyTimeAdjustment", true};
  Gaudi::Property<bool>   m_registerPhysicalChannelOutput{this, "RegisterPhysicalChannelOutput", false};
  Gaudi::Property<bool>   m_verboseDebug{this, "VerboseDebug", false};
  Gaudi::Property<unsigned int> m_TimeBits{this, "TimeBits", 4};
  Gaudi::Property<double>       m_dialogLength{this, "DialogLength", 28};

  int                                 m_stationNumber{};
  int                                 m_regionNumber{};
  int                                 m_partition{};
  std::optional<MuonDetectorResponse> m_detectorResponse{};
  DeMuonDetector*                     m_muonDetector{nullptr};
  Rndm::Numbers                       m_flatDist{};
};

DECLARE_COMPONENT( MuonDigitization )

namespace {

  struct SortPhChID {
    bool operator()( const MuonPhPreInput& first, const MuonPhPreInput& second ) const {
      return ( first.phChID().getID() ) >= ( second.phChID().getID() );
    }
  };

  auto comparePCWith( const MuonPhysicalChannel& pc ) {
    return [fe = pc.phChID().getFETile()]( const MuonPhysicalChannel* p ) { return p->phChID().getFETile() == fe; };
  }

  double getGlobalTimeOffset( boost::string_ref rootInTES ) {
    if ( rootInTES.starts_with( "Prev" ) && rootInTES.ends_with( "/" ) ) {
      rootInTES.remove_prefix( 4 );
      rootInTES.remove_suffix( 1 );
      return -std::stoi( rootInTES.data() ) * 25 * Gaudi::Units::ns;
    }
    if ( rootInTES.starts_with( "Next" ) && rootInTES.ends_with( "/" ) ) {
      rootInTES.remove_prefix( 4 );
      rootInTES.remove_suffix( 1 );
      return std::stoi( rootInTES.data() ) * 25 * Gaudi::Units::ns;
    }
    return 0.;
  }

  // reserve space for static variable
  constexpr auto OriginOfHitsContainer =
      std::array{LHCb::MuonOriginFlag::GEANT, LHCb::MuonOriginFlag::CHAMBERNOISE, LHCb::MuonOriginFlag::FLATSPILLOVER,
                 LHCb::MuonOriginFlag::BACKGROUND, LHCb::MuonOriginFlag::LHCBACKGROUND};
  constexpr auto spillTimeOffset = std::array{0, 0, -1, -2, 1, 2};
  const auto     spill           = std::array{"/LHCBackground"s, ""s, "/Prev"s, "/PrevPrev"s, "/Next"s, "/NextNext"s};
  const auto TESPathOfHitsContainer = std::array{"Hits"s, "ChamberNoiseHits"s, "FlatSpilloverHits"s, "BackgroundHits"s};
} // namespace

MuonDigitization::MuonDigitization( const std::string& name, ISvcLocator* pSvcLocator )
    : GaudiAlgorithm( name, pSvcLocator ) {}

StatusCode MuonDigitization::initialize() {
  return GaudiAlgorithm::initialize()
      .andThen( [&] { return m_flatDist.initialize( randSvc(), Rndm::Flat( 0.0, 1.0 ) ); } )
      .andThen( [&] {
        MuonBasicGeometry basegeometry( detSvc(), msgSvc() );
        m_stationNumber = basegeometry.getStations();
        m_regionNumber  = basegeometry.getRegions();
        if ( msgLevel( MSG::DEBUG ) ) {
          for ( int i = 0; i < m_stationNumber; ++i ) {
            debug() << " station " << i << " " << basegeometry.getStationName( i ) << endmsg;
          }
        }
        m_partition = basegeometry.getPartitions();
        m_detectorResponse.emplace( randSvc(), detSvc(), msgSvc() );
        if ( msgLevel( MSG::DEBUG ) ) debug() << " detectorResponse Initialized" << endmsg;
        m_muonDetector = getDet<DeMuonDetector>( DeMuonLocation::Default );
      } );
}

StatusCode MuonDigitization::execute() {

  if ( msgLevel( MSG::DEBUG ) ) debug() << "starting the Muon Digitization algorithm " << endmsg;

  SmartDataPtr<LHCb::MCHits> hitPointer( eventSvc(), LHCb::MCHitLocation::Muon );

  if ( hitPointer ) {
    if ( m_verboseDebug ) {
      for ( const auto* i : *hitPointer ) {
        info() << "muon x , y, z , exit  " << i->exit().x() << " " << i->exit().y() << "  " << i->exit().z() << endmsg;
        if ( msgLevel( MSG::DEBUG ) ) {
          debug() << "muon x , y, z entry ,  " << i->entry().x() << " " << i->entry().y() << "  " << i->entry().z()
                  << endmsg;
          debug() << "time of flight ,  " << i->time() << endmsg;
          int det = i->sensDetID();
          debug() << " chamber and gap ID	" << m_muonDetector->chamberID( det ) << " "
                  << m_muonDetector->gapID( det ) << endmsg;
        }
        const LHCb::MCParticle* origparticle = i->mcParticle();
        if ( origparticle ) {
          if ( msgLevel( MSG::DEBUG ) )
            debug() << "Particle from which it originates (PDG code)" << origparticle->particleID().abspid() << endmsg;
        } else {
          warning() << "Particle from which it originates is not defined " << endmsg;
        }
      }
    }
  } else {
    Error( "unable to retrieve the hit container" ).ignore();
  }

  if ( m_applyChamberNoise.value() ) addChamberNoise();

  MuonDigitizationData<MuonPhyChannelInput> PhyChaInput( "MuPI", &msgStream() );
  createInput( PhyChaInput );

  MuonDigitizationData<MuonPhysicalChannel> PhyChaOutput( "MuPO", &msgStream() );
  elaborateMuonPhyChannelInputs( PhyChaInput, PhyChaOutput );

  if ( m_applyElectronicNoise.value() ) addElectronicNoise( PhyChaOutput );

  applyPhysicalEffects( PhyChaOutput );

  if ( m_registerPhysicalChannelOutput.value() ) {
    MuonBasicGeometry basegeometry( detSvc(), msgSvc() );
    PhyChaOutput.registerPartitions( eventSvc(), &basegeometry )
        .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  }

  MuonDigitizationData<MuonPhysicalChannelOutput> PhysicalChannelOutput( "MULC", &msgStream() );
  fillPhysicalChannel( PhyChaOutput, PhysicalChannelOutput );

  if ( m_verboseDebug ) {
    for ( int i = 0; i < m_partition; ++i ) {
      for ( auto const* iter : PhysicalChannelOutput.getPartition( i ) ) {
        info() << "FE ID " << iter->phChID().getID() << endmsg;
        info() << "Station " << iter->phChID().getStation() << endmsg;
        info() << "Region " << iter->phChID().getRegion() << endmsg;
        info() << "Quadrant " << iter->phChID().getQuadrant() << endmsg;
        info() << "Chamber " << iter->phChID().getChamber() << endmsg;
        info() << " ch X " << iter->phChID().getPhChIDX() << endmsg;
        info() << " ch Y " << iter->phChID().getPhChIDY() << endmsg;
        info() << " frontend " << iter->phChID().getFrontEnd() << endmsg;
        info() << " fired " << iter->phChInfo().isAlive() << endmsg;
        info() << " nature " << iter->phChInfo().natureOfHit() << endmsg;
        for ( const auto& iterTraceBack : iter->hitsTraceBack() ) {
          info() << "hit time " << iterTraceBack.hitArrivalTime() << endmsg;
        }
      }
    }
  }

  MuonDigitizationData<MuonCardiacChannelOutput> CardiacChannelOutput( "MUCC", &msgStream() );
  fillCardiacChannel( PhysicalChannelOutput, CardiacChannelOutput );

  auto* mcDigitContainer = new LHCb::MCMuonDigits;
  put( mcDigitContainer, LHCb::MCMuonDigitLocation::MCMuonDigit );
  m_applyDialogDeadtime.value() ? createLogicalChannel( CardiacChannelOutput, *mcDigitContainer )
                                : createLogicalChannel( PhysicalChannelOutput, *mcDigitContainer );

  auto* digitContainer = new LHCb::MuonDigits;
  put( digitContainer, LHCb::MuonDigitLocation::MuonDigit );
  createRAWFormat( *mcDigitContainer, *digitContainer );

  if ( msgLevel( MSG::DEBUG ) ) debug() << "End of the Muon Digitization" << endmsg;
  return StatusCode::SUCCESS;
}

StatusCode MuonDigitization::finalize() {
  m_detectorResponse.reset();
  return GaudiAlgorithm::finalize();
}

void MuonDigitization::addChamberNoise() {
  int container = 1;
  for ( int ispill = 1; ispill <= m_numberOfEvents; ++ispill ) {
    auto*       hitsContainer = new ObjectVector<LHCb::MCHit>();
    std::string path          = "/Event" + spill[ispill] + "/MC/Muon/" + TESPathOfHitsContainer[container];
    eventSvc()->registerObject( path, hitsContainer ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    for ( int k = 0; k < m_stationNumber; ++k ) {
      for ( int s = 0; s < m_regionNumber; ++s ) {
        const int partitionNumber = k * m_regionNumber + s;
        const int chamberInRegion = m_muonDetector->chamberInRegion( k, s );
        const int gapPerChamber   = m_muonDetector->gapsInRegion( k, s );
        for ( int chamber = 0; chamber < chamberInRegion; ++chamber ) {
          const int numberOfNoiseHit = ( m_detectorResponse->getChamberResponse( partitionNumber ) )->extractNoise();
          // the readout number is essentially meaningless...
          // it is chamber noise.....
          if ( numberOfNoiseHit == 0 ) continue;
          DeMuonChamber* p_chamber = m_muonDetector->getChmbPtr( k, s, chamber );
          for ( int hit = 1; hit <= numberOfNoiseHit; ++hit ) {
            // define position of hit
            // first gap number
            const int gap = std::min( (int)( m_flatDist() * gapPerChamber ), m_muonDetector->gapsInRegion( k, s ) - 1 );
            IPVolume* gasGap      = p_chamber->getGasGap( gap );
            IPVolume* gasGapLayer = p_chamber->getGasGapLayer( gap );

            const auto*  gapBox   = dynamic_cast<const SolidBox*>( gasGap->lvolume()->solid() );
            const double zhalfgap = gapBox->zHalfLength();

            // then x&y
            const double x    = m_flatDist() * m_muonDetector->getSensAreaX( k, s );
            const double y    = m_flatDist() * m_muonDetector->getSensAreaY( k, s );
            const double time = m_flatDist() * m_BXTime;
            // traslate xyz e local to global..
            const Gaudi::XYZPoint pointInLayer1 = gasGap->toMother( Gaudi::XYZPoint( x, y, -1 * zhalfgap + 0.1 ) );
            const Gaudi::XYZPoint pointInLayer2 = gasGap->toMother( Gaudi::XYZPoint( x, y, zhalfgap - 0.1 ) );
            const Gaudi::XYZPoint pointInCh1    = gasGapLayer->toMother( pointInLayer1 );
            const Gaudi::XYZPoint pointInCh2    = gasGapLayer->toMother( pointInLayer2 );
            const Gaudi::XYZPoint point1        = p_chamber->geometry()->toGlobal( pointInCh1 );
            const Gaudi::XYZPoint point2        = p_chamber->geometry()->toGlobal( pointInCh2 );
            const double          z             = ( point1.z() + point2.z() ) / 2.0;
            const double          tofOfLight    = ( sqrt( x * x + y * y + z * z ) ) / 300.0;
            const auto hitMid = Gaudi::XYZPoint{( point1.x() + point2.x() ) / 2.0F, ( point1.y() + point2.y() ) / 2.0F,
                                                ( point1.z() + point2.z() ) / 2.0F};
            const int  sen    = m_muonDetector->sensitiveVolumeID( hitMid );

            auto pHit = std::make_unique<LHCb::MCHit>();
            pHit->setEntry( point1 );
            pHit->setDisplacement( point2 - point1 );
            pHit->setTime( time + tofOfLight );
            pHit->setSensDetID( sen );
            hitsContainer->push_back( pHit.release() );
            if ( m_verboseDebug ) {
              info() << "adding chamber noise hit " << ispill << " " << k << " " << s << numberOfNoiseHit << endmsg;
            }
          }
          if ( m_verboseDebug )
            info() << "adding chamber noise hit " << ispill << " " << k << " " << s << " chamber " << chamber << " "
                   << numberOfNoiseHit << endmsg;
        }
      }
    }
  }
}

void MuonDigitization::createInput( MuonDigitizationData<MuonPhyChannelInput>& PhyChaInput ) {
  // loop over the containers
  std::vector<MuonPhPreInput> keepTemporary[20];

  const double globalTimeOffset = getGlobalTimeOffset( rootInTES() );

  for ( int ispill = 0; ispill <= m_numberOfEvents; ++ispill ) { // loop over event types
    const long spillTime = spillTimeOffset[ispill] * m_BXTime;

    for ( int container = 0; container < m_container; ++container ) { // loop over containers
      const std::string path = "/Event" + spill[ispill] + "/MC/Muon/" + TESPathOfHitsContainer[container];
      if ( m_verboseDebug ) { info() << "createInput: hit container path " << container << " " << path << endmsg; }

      SmartDataPtr<LHCb::MCHits> hitPointer( eventSvc(), path );
      if ( !hitPointer ) continue;

      for ( auto& iter : *hitPointer ) { // loop over hits
        const int det = iter->sensDetID();
        if ( det < 0 ) continue;

        const unsigned int hitStation = m_muonDetector->stationID( det );
        const unsigned int hitRegion  = m_muonDetector->regionID( det );
        const unsigned int hitChamber = m_muonDetector->chamberID( det );
        const unsigned int hitGap     = m_muonDetector->gapID( det );
        const unsigned int hitQuarter = m_muonDetector->quadrantID( det );

        for ( const auto& [fe, distanceFromBoundary] :
              m_muonDetector->listOfPhysChannels( iter->entry(), iter->exit(), hitRegion, hitChamber ) ) {

          MuonPhPreInput inputPointer;
          inputPointer.setPhChID( MuonPhChID{}
                                      .setStation( hitStation )
                                      .setRegion( hitRegion )
                                      .setQuadrant( hitQuarter )
                                      .setChamber( hitChamber )
                                      .setPhChIDX( fe.getFEIDX() )
                                      .setPhChIDY( fe.getFEIDY() )
                                      .setFrontEnd( fe.getLayer() )
                                      .setReadout( fe.getReadout() )
                                      .setGap( hitGap ) );

          auto& tb = inputPointer.getHitTraceBack();
          tb.setMCHit( iter );

          // correct for the  tof .... i.e. subtract the tof that
          // a lightparticle impacting the center of the pc. has....

          double     xcenter, ycenter, zcenter;
          StatusCode sc =
              m_muonDetector->getPCCenter( fe, hitChamber, hitStation, hitRegion, xcenter, ycenter, zcenter );
          if ( sc.isFailure() ) warning() << " getpc ch error" << endmsg;
          const double tofOfLight = sqrt( xcenter * xcenter + ycenter * ycenter + zcenter * zcenter ) / 300.0;
          tb.setHitArrivalTime( iter->time() + globalTimeOffset + +spillTime - tofOfLight + 0.5 )
              .setCordinate( distanceFromBoundary );
          // patch for machine background
          auto [bx, icont] = ( ispill == 0 ? std::pair{ispill, 4} : std::pair{ispill - 1, container} );
          tb.getMCMuonHitOrigin().setBX( bx ).setHitNature( OriginOfHitsContainer[icont] );
          tb.getMCMuonHistory().setBXOfHit( bx ).setNatureOfHit( OriginOfHitsContainer[icont] );

          if ( m_verboseDebug ) {
            const int code = ( iter->mcParticle() ? iter->mcParticle()->particleID().abspid() : 0 );
            info() << "hit processing " << hitStation << " " << hitRegion << " " << hitQuarter << " " << hitChamber
                   << " " << hitGap << " " << fe.getLayer() << " " << fe.getReadout() << " " << tofOfLight << " "
                   << OriginOfHitsContainer[container] << " " << ispill << " " << code << endmsg;
            info() << " ph ch ID " << inputPointer.phChID() << " id " << inputPointer.phChID().getID() << endmsg;
          }

          keepTemporary[hitStation * 4 + hitRegion].push_back( std::move( inputPointer ) );

        } // end of loop over phisical channels
      }   // end of loop over MC hits
    }     // end of loop over containers
  }       // end of loop over event types

  for ( int iterRegion = 0; iterRegion < m_partition; ++iterRegion ) { // loop over partitions
    auto& kt = keepTemporary[iterRegion];
    std::stable_sort( kt.begin(), kt.end(), SortPhChID{} );
    while ( !kt.empty() ) {
      auto& back = kt.back();
      if ( m_verboseDebug ) { info() << "Sorted hits s/r/q/c/idx/idy/FE/RO/g " << back.phChID() << endmsg; }
      auto phChPointer = std::make_unique<MuonPhyChannelInput>( back.phChID(), back.getHitTraceBack() );
      PhyChaInput.addMuonObject( iterRegion, std::move( phChPointer ) );
      kt.pop_back();
    }
  }
}

void MuonDigitization::elaborateMuonPhyChannelInputs( MuonDigitizationData<MuonPhyChannelInput> const& PhyChaInput,
                                                      MuonDigitizationData<MuonPhysicalChannel>& PhysicalChannel ) {
  auto addChannel = [&]( int i, const MuonPhyChannelInput& ref, MuonPhChID FE ) {
    auto outputPointer = std::make_unique<MuonPhysicalChannel>( FE, m_gate, m_BXTime );
    outputPointer->setResponse( m_detectorResponse->getResponse( ref.phChID() ) );
    outputPointer->hitsTraceBack().push_back( ref.getHitTraceBack() );
    return PhysicalChannel.addMuonObject( i, std::move( outputPointer ) );
  };
  for ( int i = 0; i < m_partition; ++i ) {
    const auto& range = PhyChaInput.getPartition( i );
    if ( range.empty() ) continue;
    auto  iter   = range.begin();
    auto  prevFE = ( *iter )->phChID().getFETile();
    auto* prev   = addChannel( i, **iter, prevFE );
    for ( ++iter; iter != range.end(); ++iter ) {
      if ( const auto FE = ( *iter )->phChID().getFETile();
           FE == prevFE ) { // phys channels with same FE are not recreated
        prev->hitsTraceBack().push_back( ( *iter )->getHitTraceBack() );
      } else {
        prev   = addChannel( i, **iter, FE );
        prevFE = FE;
      }
    }
  }
}

void MuonDigitization::fillPhysicalChannel( MuonDigitizationData<MuonPhysicalChannel> const& PhysicalChannel,
                                            MuonDigitizationData<MuonPhysicalChannelOutput>& PhysicalChannelOutput ) {
  for ( int i = 0; i < m_partition; ++i ) {
    for ( auto* iterInput : PhysicalChannel.getPartition( i ) ) {
      auto objToAdd       = std::make_unique<MuonPhysicalChannelOutput>( *iterInput );
      bool fired          = false;
      bool interestingHit = false;
      if ( m_verboseDebug ) info() << objToAdd->phChID().getID() << " " << objToAdd->phChID() << endmsg;
      for ( auto& iterInHits : objToAdd->hitsTraceBack() ) {
        if ( m_verboseDebug )
          info() << " geo acce " << iterInHits.getMCMuonHistory().isHitOutGeoAccemtance() << " cha ineff "
                 << iterInHits.getMCMuonHistory().isKilledByChamberInefficiency() << " in deadtime "
                 << iterInHits.getMCMuonHistory().isHitInDeadtime() << " time of firing " << iterInHits.hitArrivalTime()
                 << endmsg;
        if ( !fired ) {
          if ( !iterInHits.getMCMuonHistory().isHitOutGeoAccemtance() &&
               !iterInHits.getMCMuonHistory().isKilledByChamberInefficiency() &&
               !iterInHits.getMCMuonHistory().isHitInDeadtime() ) {
            const double timeOfFiring = iterInHits.hitArrivalTime();
            if ( timeOfFiring > 0 && timeOfFiring < m_gate ) {
              fired = true;
              iterInHits.getMCMuonHistory().setFiredFlag( 1u );
              objToAdd->setFiringTime( timeOfFiring );
              objToAdd->phChInfo()
                  .setAliveDigit( 1 )
                  .setBXIDFlagHit( iterInHits.getMCMuonHistory().BX() )
                  .setNatureHit( iterInHits.getMCMuonHitOrigin().getNature() );
              if ( iterInHits.getMCMuonHistory().isHitOriginatedInCurrentEvent() ) {
                if ( iterInHits.getMCMuonHistory().isInForTimeAdjustment() ) {
                  objToAdd->phChInfo().setAliveTimeAdjDigit( 1 );
                  if ( iterInHits.getMCMuonHistory().hasTimeJittered() ) {
                    objToAdd->phChInfo().setTimeJitteredDigit( 1 );
                  }
                }
              }
            }
          }
        }
      }
      if ( m_verboseDebug ) info() << " fired " << fired << endmsg;
      if ( !fired ) {
        for ( auto& iterInHits : objToAdd->hitsTraceBack() ) {
          if ( iterInHits.getMCMuonHistory().isHitOriginatedInCurrentEvent() ) {
            interestingHit = true;

            if ( iterInHits.getMCMuonHistory().hasTimeJittered() ) {
              if ( !( iterInHits.getMCMuonHistory().isInForTimeAdjustment() ) ) {
                // first source of dead time jitter
                objToAdd->phChInfo().setTimeJitteredDigit( 1 );
              } else if ( iterInHits.getMCMuonHistory().isHitInDeadtime() ) {
                // remember to check that the time adjustment do not
                // put back the hit int he gate....=>
                // only deadtime can kill this digit
                objToAdd->phChInfo().setDeadtimeDigit( 1 );
              }
            }
            if ( iterInHits.getMCMuonHistory().isKilledByChamberInefficiency() ) {
              // hit is killed by chamber inefficiency
              objToAdd->phChInfo().setChamberInefficiencyDigit( 1 );
            } else if ( iterInHits.getMCMuonHistory().isHitOutGeoAccemtance() ) {
              // hit is killed by geomatrial acceptance
              objToAdd->phChInfo().setGeometryInefficiency( 1 );
            } else if ( iterInHits.getMCMuonHistory().isHitInDeadtime() ) {
              // hit in deadtime
              objToAdd->phChInfo().setDeadtimeDigit( 1 );
            } else if ( iterInHits.getMCMuonHistory().isOutForTimeAdjustment() ) {
              // hit is killed by time adjustment
              objToAdd->phChInfo().setTimeAdjDigit( 1 );
            }
          }
        }
      }
      if ( !fired && !interestingHit ) {
        auto iterInHits = objToAdd->hitsTraceBack().begin();
        objToAdd->phChInfo()
            .setAliveDigit( 0 )
            .setBXIDFlagHit( iterInHits->getMCMuonHistory().BX() )
            .setNatureHit( iterInHits->getMCMuonHitOrigin().getNature() );
        // objToAdd->phChInfo().setSecondPart((*iterInHits).secondPart());
        if ( iterInHits->getMCMuonHistory().hasTimeJittered() ) {
          if ( !( iterInHits->getMCMuonHistory().isInForTimeAdjustment() ) ) {
            // first source of dead time jitter
            objToAdd->phChInfo().setTimeJitteredDigit( 1 );
          } else if ( iterInHits->getMCMuonHistory().isHitInDeadtime() ) {
            // remember to check that the time adjustment do not
            // put back the hit int he gate....=>
            // only deadtime can kill this digit
            objToAdd->phChInfo().setDeadtimeDigit( 1 );
          }
        }
        if ( iterInHits->getMCMuonHistory().isKilledByChamberInefficiency() ) {
          // hit is killed by chamber inefficiency
          objToAdd->phChInfo().setChamberInefficiencyDigit( 1 );
        } else if ( iterInHits->getMCMuonHistory().isHitOutGeoAccemtance() ) {
          // hit is killed by geomatrial acceptance
          objToAdd->phChInfo().setGeometryInefficiency( 1 );
        } else if ( iterInHits->getMCMuonHistory().isHitInDeadtime() ) {
          // hit in deadtime
          objToAdd->phChInfo().setDeadtimeDigit( 1 );
        } else if ( iterInHits->getMCMuonHistory().isOutForTimeAdjustment() ) {
          // hit is killed by time adjustment
          objToAdd->phChInfo().setTimeAdjDigit( 1 );
        }
      }

      // debug printout
      if ( msgLevel( MSG::DEBUG ) || m_verboseDebug ) {
        bool muon = false;
        for ( auto& iterInHits : objToAdd->hitsTraceBack() ) {
          // search the muon first...
          if ( iterInHits.getMCHit() ) {
            const LHCb::MCParticle* particle = iterInHits.getMCHit()->mcParticle();
            if ( particle ) {
              const int pid = particle->particleID().abspid();
              if ( pid == 13 || pid == -13 ) {
                if ( msgLevel( MSG::DEBUG ) )
                  debug() << "moun hit   time   ??????" << iterInHits.hitArrivalTime() << endmsg;
                muon = true;
              }
            }
          }
        }
        if ( muon && m_verboseDebug ) {
          info() << "**** start new pc****   station  and region " << i << " fired " << fired << endmsg;
          for ( const auto& iterInHits : objToAdd->hitsTraceBack() ) {
            info() << "time" << iterInHits.hitArrivalTime() << " tile " << objToAdd->phChID().getFETile() << endmsg;
            info() << " deadtime " << iterInHits.getMCMuonHistory().isHitInDeadtime() << " time jitter "
                   << iterInHits.getMCMuonHistory().hasTimeJittered() << " efficiency  "
                   << iterInHits.getMCMuonHistory().isKilledByChamberInefficiency() << endmsg;
          }
        }
      }
      objToAdd->fillTimeList();
      PhysicalChannelOutput.addMuonObject( i, std::move( objToAdd ) );
    }
  }
}

void MuonDigitization::fillCardiacChannel( MuonDigitizationData<MuonPhysicalChannelOutput> const& PhysicalChannelOutput,
                                           MuonDigitizationData<MuonCardiacChannelOutput>& CardiacChannelOutput ) {
  for ( int i = 0; i < m_partition; ++i ) {
    const auto& partition = CardiacChannelOutput.getPartition( i );
    for ( auto* iterInput : PhysicalChannelOutput.getPartition( i ) ) {
      for ( const auto& tile : iterInput->calculateTileID( m_muonDetector ) ) {
        auto iterOutput =
            std::find_if( partition.begin(), partition.end(), [&]( const auto& i ) { return i->chID() == tile; } );
        if ( iterOutput != partition.end() ) {
          ( *iterOutput )->addPhyChannel( iterInput );
        } else {
          auto objToAdd = std::make_unique<MuonCardiacChannelOutput>( tile );
          objToAdd->addPhyChannel( iterInput );
          CardiacChannelOutput.addMuonObject( i, std::move( objToAdd ) );
        }
      }
    }
    for ( auto& iterOutput : partition ) { iterOutput->processForDeadTime( m_dialogLength, m_gate ); }
  }
}

void MuonDigitization::applyPhysicalEffects( MuonDigitizationData<MuonPhysicalChannel>& PhysicalChannel ) {
  // loop over the 20 containers

  for ( int i = 0; i < m_partition; ++i ) {
    const int station = i / 4;
    const int region  = i % 4;
    if ( !PhysicalChannel.getPartition( i ).empty() ) {

      auto phChInX = std::array{-1, -1};
      auto phChInY = std::array{-1, -1};
      for ( int iloop = 0; iloop < m_muonDetector->readoutInRegion( station, region ); ++iloop ) {
        phChInX[m_muonDetector->getReadoutType( iloop, station, region )] =
            m_muonDetector->getPhChannelNX( iloop, station, region );
        phChInY[m_muonDetector->getReadoutType( iloop, station, region )] =
            m_muonDetector->getPhChannelNY( iloop, station, region );
      }

      std::vector<std::unique_ptr<MuonPhysicalChannel>> XTalkPhysicalChannel;
      std::vector<std::unique_ptr<MuonPhysicalChannel>> channelsDueToXTalk;
      for ( auto* iter : PhysicalChannel.getPartition( i ) ) {
        // apply per pc the time jitter on each hit
        if ( m_applyTimeJitter.value() ) iter->applyTimeJitter();
        // apply per pc the geometry inefficiency 	on each hit
        if ( m_applyEfficiency.value() ) iter->applyGeoInefficiency();
        // apply per pc the chamber inefficiency 	on each hit
        if ( m_applyEfficiency.value() ) iter->applyChamberInefficiency();

        // apply per pc the X Talk on each hit
        // start Xtalk

        if ( m_applyXTalk.value() ) iter->applyXTalk( phChInX, phChInY, channelsDueToXTalk );
        for ( auto& iterXTalk : channelsDueToXTalk ) {
          MuonPhysicalChannel* pFound = PhysicalChannel.findObjectIn( i, comparePCWith( *iterXTalk ) );
          if ( !pFound ) {
            iterXTalk->setResponse( m_detectorResponse->getResponse( iterXTalk->phChID() ) );
            XTalkPhysicalChannel.push_back( std::move( iterXTalk ) );
          } else {
            if ( msgLevel( MSG::DEBUG ) )
              debug() << "xtalk hit test  molto dopo " << iter << " " << iterXTalk.get() << " " << pFound << endmsg;
            pFound->addToPC( *iterXTalk );
          }
        }
        // end Xtalk
        // empty the Xtalked PC container
        channelsDueToXTalk.clear();
      }
      // add the xtalk hit to the main container
      for ( auto& iter : XTalkPhysicalChannel ) {
        MuonPhysicalChannel* pFound = PhysicalChannel.findObjectIn( i, comparePCWith( *iter ) );
        if ( pFound ) {
          pFound->addToPC( *iter );
          iter.reset();
        } else {
          PhysicalChannel.addMuonObject( i, std::move( iter ) );
        }
      }
      // start deadtime
      for ( auto* iter : PhysicalChannel.getPartition( i ) ) {
        // sort in time the hit of each pc
        iter->sortInTimeHits();
        // apply time adjustment
        if ( m_applyTimeAdjustment.value() ) iter->applyTimeAdjustment();
        // apply deadtime
        if ( m_applyDeadtime.value() ) iter->applyDeadtime( m_numberOfEventsNeed );
      }
      // end deadtime
    }
  }
}

void MuonDigitization::createLogicalChannel( MuonDigitizationData<MuonPhysicalChannelOutput> const& PhyChaOutput,
                                             LHCb::MCMuonDigits&                                    mcDigitContainer ) {
  int countDigits = 0;
  for ( int i = 0; i < m_partition; ++i ) {

    for ( auto* iter : PhyChaOutput.getPartition( i ) ) {
      if ( m_verboseDebug ) {
        if ( msgLevel( MSG::DEBUG ) ) debug() << "Create logical channel from FE ID " << iter->phChID() << endmsg;
      }

      const auto phChTileID = iter->calculateTileID( m_muonDetector );

      if ( m_verboseDebug ) {
        info() << " after tile calculation " << phChTileID.size() << " " << endmsg;
        info() << " tile  " << phChTileID[0] << phChTileID[1] << " " << endmsg;
      }

      //
      // loop over possible phchtileID (1 or 2 if double mapping)
      //
      int iTile = 0;
      for ( LHCb::MuonTileID phChTile : phChTileID ) {
        //
        // loop over already created Digits
        //
        if ( m_verboseDebug ) {
          if ( msgLevel( MSG::DEBUG ) ) debug() << " Loop on mappings " << iTile << " " << phChTileID.size() << endmsg;
        }
        auto iterDigit = std::find_if( mcDigitContainer.begin(), mcDigitContainer.end(),
                                       [&]( const auto& d ) { return d->key() == phChTile; } );

        if ( ( iterDigit != mcDigitContainer.end() ) ) {
          //
          // tile is the key of the existing Digit, phChTileID[]
          // is the just created ID of the ph.ch.
          //
          if ( m_verboseDebug ) {
            info() << " Loop on mappings found already " << phChTile << " " << endmsg;
            info() << "  " << ( *iterDigit )->DigitInfo().isAlive() << " " << iter->phChInfo().isAlive() << endmsg;
          }
          // Digit already exists, update bits and links
          if ( ( *iterDigit )->DigitInfo().isAlive() && iter->phChInfo().isAlive() ) {
            // both fired
            if ( ( *iterDigit )->firingTime() < iter->firingTime() ) {
              for ( auto& hits : iter->hitsTraceBack() ) { hits.getMCMuonHistory().setFiredFlag( 0 ); }
            } else {
              ( *iterDigit )->setFiringTime( iter->firingTime() );
              ( *iterDigit )
                  ->DigitInfo()
                  .setNatureHit( iter->phChInfo().natureOfHit() )
                  .setBXIDFlagHit( iter->phChInfo().BX() );
              for ( auto& hits : ( *iterDigit )->HitsHistory() ) { hits.setFiredFlag( 0 ); }
            }
          }

          if ( ( *iterDigit )->DigitInfo().isAlive() && !( iter->phChInfo().isAlive() ) ) {
            // only one is fired
          }
          if ( !( ( *iterDigit )->DigitInfo().isAlive() ) && ( iter->phChInfo().isAlive() ) ) {
            // only one is fired
            ( *iterDigit )->setFiringTime( iter->firingTime() );
            ( *iterDigit )
                ->DigitInfo()
                .setNatureHit( iter->phChInfo().natureOfHit() )
                .setBXIDFlagHit( iter->phChInfo().BX() )
                .setSecondPart( 0 )
                .setAliveDigit( 1 );

            if ( m_verboseDebug ) info() << " importante " << ( *iterDigit )->DigitInfo().isAlive() << endmsg;
          }
          if ( !( ( *iterDigit )->DigitInfo().isAlive() ) && !( iter->phChInfo().isAlive() ) ) {
            if ( m_verboseDebug ) info() << " molto importante " << ( *iterDigit )->DigitInfo().isAlive() << endmsg;
            // both not fired
            if ( ( *iterDigit )->DigitInfo().isInDeadTime() || iter->phChInfo().isInDeadTime() ) {
              ( *iterDigit )->DigitInfo().setDeadtimeDigit( 1 );
            }
            if ( ( *iterDigit )->DigitInfo().isDeadForChamberInefficiency() ||
                 iter->phChInfo().isDeadForChamberInefficiency() ) {
              ( *iterDigit )->DigitInfo().setChamberInefficiencyDigit( 1 );
            }
            if ( ( *iterDigit )->DigitInfo().isDeadByTimeJitter() || iter->phChInfo().isDeadByTimeJitter() ) {
              ( *iterDigit )->DigitInfo().setTimeJitteredDigit( 1 );
            }
            if ( ( *iterDigit )->DigitInfo().isDeadByTimeAdjustment() || iter->phChInfo().isDeadByTimeAdjustment() ) {
              ( *iterDigit )->DigitInfo().setTimeAdjDigit( 1 );
            }
            if ( ( *iterDigit )->DigitInfo().isAliveByTimeAdjustment() || iter->phChInfo().isAliveByTimeAdjustment() ) {
              ( *iterDigit )->DigitInfo().setAliveTimeAdjDigit( 1 );
            }
            if ( ( *iterDigit )->DigitInfo().isDeadByGeometry() || iter->phChInfo().isDeadByGeometry() ) {
              ( *iterDigit )->DigitInfo().setGeometryInefficiency( 1 );
            }
          }
          // add links to the hits
          for ( auto& iterOnHits : iter->hitsTraceBack() ) {
            ( *iterDigit )->HitsHistory().push_back( iterOnHits.getMCMuonHistory() );
            ( *iterDigit )->addToMCHits( iterOnHits.getMCHit() );
          }
        } else {
          if ( m_verboseDebug ) info() << " create new Digit with tile " << phChTile << " " << iTile << endmsg;
          auto newMCDigit = std::make_unique<LHCb::MCMuonDigit>( phChTile );
          for ( auto& iterOnHits : iter->hitsTraceBack() ) {
            newMCDigit->HitsHistory().push_back( iterOnHits.getMCMuonHistory() );
            newMCDigit->addToMCHits( iterOnHits.getMCHit() );
          }
          newMCDigit->setDigitInfo( iter->phChInfo() );
          newMCDigit->setFiringTime( iter->firingTime() );

          mcDigitContainer.insert( newMCDigit.release() );
          ++countDigits;
        }
        ++iTile;
      }
    }
  }
  if ( msgLevel( MSG::DEBUG ) ) debug() << " MC Digits created " << countDigits << endmsg;
}

void MuonDigitization::createLogicalChannel( MuonDigitizationData<MuonCardiacChannelOutput> const& PhyChaOutput,
                                             LHCb::MCMuonDigits&                                   mcDigitContainer ) {
  int countDigits = 0;
  for ( int i = 0; i < m_partition; ++i ) {
    for ( const auto* iter : PhyChaOutput.getPartition( i ) ) {
      auto newMCDigit = std::make_unique<LHCb::MCMuonDigit>( iter->chID() );
      for ( auto& iterOnHitsCardiac : iter->hitsTraceBack() ) {
        newMCDigit->HitsHistory().push_back( iterOnHitsCardiac->getMCMuonHistory() );
        newMCDigit->addToMCHits( iterOnHitsCardiac->getMCHit() );
      }
      newMCDigit->setDigitInfo( iter->chInfo() );
      if ( iter->hasFired() ) { newMCDigit->setFiringTime( iter->firingTime() ); }
      mcDigitContainer.insert( newMCDigit.release() );
      ++countDigits;
    }
  }
  if ( msgLevel( MSG::DEBUG ) ) debug() << " MC Digits created " << countDigits << endmsg;
}

void MuonDigitization::createRAWFormat( LHCb::MCMuonDigits const& mcDigitContainer, LHCb::MuonDigits& digitContainer ) {
  for ( auto& iterMCDigit : mcDigitContainer ) {
    if ( !iterMCDigit->DigitInfo().isAlive() ) continue;
    auto               muonDigit = std::make_unique<LHCb::MuonDigit>( iterMCDigit->key() );
    const unsigned int time      = iterMCDigit->firingTime() / m_timeBin;
    muonDigit->setTimeStamp( time );
    digitContainer.insert( muonDigit.release() );
    if ( msgLevel( MSG::DEBUG ) ) {
      LHCb::MuonTileID gg = iterMCDigit->key();
      debug() << "new daq word LO/s/r/q/nx/ny/time " << gg.layout() << " " << gg.station() << " " << gg.region() << " "
              << gg.quarter() << " " << gg.nX() << " " << gg.nY() << " "
              << " " << time << endmsg;
      debug() << gg << endmsg;
    }
  }
}

void MuonDigitization::addElectronicNoise( MuonDigitizationData<MuonPhysicalChannel>& PhysicalChannel ) {
  for ( int ispill = 1; ispill <= m_numberOfEvents; ++ispill ) {
    const double shiftOfTOF = -m_BXTime * ( ispill - 1 );
    for ( int i = 0; i < m_stationNumber; ++i ) {
      for ( int k = 0; k < m_regionNumber; ++k ) {
        const int partitionNumber = i * m_regionNumber + k;
        const int chamberInRegion = m_muonDetector->chamberInRegion( i, k );
        for ( int chamber = 0; chamber != chamberInRegion; ++chamber ) {
          for ( int frontEnd = 0;
                frontEnd < (int)( m_muonDetector->gapsInRegion( i, k ) / m_muonDetector->gapsPerFE( i, k ) );
                ++frontEnd ) {
            for ( int readout = 0; readout < m_muonDetector->readoutInRegion( i, k ); ++readout ) {
              const int phChInX       = m_muonDetector->getPhChannelNX( readout, i, k );
              const int phChInY       = m_muonDetector->getPhChannelNY( readout, i, k );
              const int noiseChannels = m_detectorResponse->getResponse( partitionNumber, readout )->electronicNoise();
              const unsigned int readoutType = m_muonDetector->getReadoutType( readout, i, k );
              for ( int hitNoise = 0; hitNoise < noiseChannels; ++hitNoise ) {
                int chX = m_flatDist() * phChInX;
                int chY = m_flatDist() * phChInY;
                if ( chX == phChInX ) chX = phChInX - 1;
                if ( chY == phChInY ) chY = phChInY - 1;
                const double     time = m_flatDist() * m_BXTime + shiftOfTOF;
                LHCb::MuonTileID chTile;
                StatusCode       scchamber = m_muonDetector->Chamber2Tile( chamber, i, k, chTile );
                if ( scchamber.isFailure() ) warning() << " failure of chamber 2 tile conversion " << endmsg;
                MuonPhChID ID = MuonPhChID{}
                                    .setStation( i )
                                    .setRegion( k )
                                    .setQuadrant( chTile.quarter() )
                                    .setChamber( chamber )
                                    .setPhChIDX( chX )
                                    .setPhChIDY( chY )
                                    .setFrontEnd( frontEnd )
                                    .setReadout( readoutType );
                auto newPhysicalChannel = std::make_unique<MuonPhysicalChannel>( ID.getFETile(), m_gate, m_BXTime );
                MuonHitTraceBack pointerToHit;
                pointerToHit.setHitArrivalTime( time );
                pointerToHit.getMCMuonHitOrigin()
                    .setHitNature( LHCb::MuonOriginFlag::ELECTRONICNOISE )
                    .setBX( ispill - 1 );
                pointerToHit.getMCMuonHistory()
                    .setNatureOfHit( LHCb::MuonOriginFlag::ELECTRONICNOISE )
                    .setBXOfHit( ispill - 1 );
                newPhysicalChannel->hitsTraceBack().push_back( pointerToHit );
                MuonPhysicalChannel* pFound =
                    PhysicalChannel.findObjectIn( partitionNumber, comparePCWith( *newPhysicalChannel ) );
                if ( pFound ) {
                  pFound->addToPC( *newPhysicalChannel );
                } else {
                  newPhysicalChannel->setResponse( m_detectorResponse->getResponse( newPhysicalChannel->phChID() ) );
                  PhysicalChannel.addMuonObject( partitionNumber, std::move( newPhysicalChannel ) );
                }
              }
            }
          }
        }
      }
    }
  }
}
