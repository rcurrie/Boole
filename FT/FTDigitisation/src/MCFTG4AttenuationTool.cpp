/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "MCFTG4AttenuationTool.h"
#include <DetDesc/Condition.h>

//-----------------------------------------------------------------------------
// Implementation file for class : MCFTG4ttenuationTool
//
// 2017-07-11 : Martin Bieker
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_COMPONENT( MCFTG4AttenuationTool )

//=============================================================================
// Calculate the attenuation
//=============================================================================
IMCFTAttenuationTool::Attenuation MCFTG4AttenuationTool::attenuation( double hitXPosition, double hitYPosition ) const {
  hitXPosition = std::abs( hitXPosition );
  hitYPosition = std::abs( hitYPosition );

  int binX = findBin( m_xEdges, hitXPosition );
  int binY = findBin( m_yEdges, hitYPosition );

  // Hit outside quadrant (should never happen!)
  if ( binX == -1 || binY == -1 ) {
    return {0.0, 0.0};
  } else {
    return {m_effDir[binY * m_nBinsX + binX], m_effRef[binY * m_nBinsX + binX]};
  }
}

//=========================================================================
// Retrieve the Attenuation map from the CondDB
//=========================================================================
StatusCode MCFTG4AttenuationTool::initialize() {
  auto sc = base_class::initialize();
  if ( sc.isFailure() ) { return sc; }

  // Select the type of radiation map
  std::string conditionsLocation;
  conditionsLocation =
      "/dd/Conditions/Calibration/FT/AttenuationMap" + std::to_string( m_irradiation ) + "fb_z_920-940";
  if ( m_irradiationLinearModel ) conditionsLocation += "_linearmodel";
  if ( m_agingFibres ) conditionsLocation += "_aging";
  if ( m_replaceModules ) conditionsLocation += "_replacedmodule";

  info() << "Selecting " << std::to_string( m_irradiation ) << "/fb attenuation map using "
         << std::string( m_irradiationLinearModel ? "linear" : "power-law" ) << " radiation damage model "
         << std::string( m_agingFibres ? "with" : "without" ) << " aging fibres and "
         << std::string( m_replaceModules ? "with" : "without" ) << " replaced center module. " << endmsg;

  // When conditions are not found, return failure
  if ( existDet<Condition>( conditionsLocation ) == false ) {
    return Error( "No attenuation maps found in SIMCOND: update to a newer tag", StatusCode::FAILURE );
  }

  // Get the conditions
  Condition* cond = getDet<Condition>( conditionsLocation );

  m_nBinsX = cond->param<int>( "x_n_bins" );
  m_nBinsY = cond->param<int>( "y_n_bins" );

  m_xEdges = cond->param<std::vector<double>>( "x_edges" );
  m_yEdges = cond->param<std::vector<double>>( "y_edges" );
  std::sort( m_xEdges.begin(), m_xEdges.end() );
  std::sort( m_yEdges.begin(), m_yEdges.end() );

  m_effDir = cond->param<std::vector<double>>( "eff_dir" );
  m_effRef = cond->param<std::vector<double>>( "eff_refl" );

  // Validate Attenuatom Map
  if ( !validateMap() ) return Error( "Attenuation Map does not match bin counts" );

  std::transform( m_effRef.begin(), m_effRef.end(), m_effRef.begin(),
                  [this]( double val ) { return val * m_mirrorReflectivity; } );

  if ( msgLevel( MSG::DEBUG ) ) { debug() << "nBinsX :" << m_nBinsX << "	nBinsY: " << m_nBinsY << std::endl; }

  return sc;
}

bool MCFTG4AttenuationTool::validateMap() {
  if ( m_nBinsX != m_xEdges.size() - 1 || m_nBinsY != m_yEdges.size() - 1 ) { return false; }
  if ( m_nBinsX * m_nBinsY != m_effDir.size() || m_nBinsX * m_nBinsY != m_effRef.size() ) { return false; }
  return true;
}

int MCFTG4AttenuationTool::findBin( LHCb::span<const double> axis, double position ) const {
  auto iterator = std::upper_bound( axis.begin(), axis.end(), position );
  // If position is outside of map: use first or last bin.
  if ( iterator == axis.end() )
    --iterator;
  else if ( iterator == axis.begin() )
    ++iterator;
  return iterator - axis.begin() - 1;
}
