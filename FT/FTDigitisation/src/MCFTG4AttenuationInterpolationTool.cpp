/*****************************************************************************\
* (c) Copyright 2018 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "MCFTG4AttenuationInterpolationTool.h"
#include <DetDesc/Condition.h>
#include <math.h>

//-----------------------------------------------------------------------------
// Implementation file for class : MCFTG4AttenuationInterpolationTool
//
// 2018-28-08 : Martin Bieker
//-----------------------------------------------------------------------------

// names of the parameters describing the binning of the LYAMs
namespace {
  static const std::set<std::string> s_binParams{"x_n_bins", "y_n_bins", "x_edges", "y_edges"};
}

// Declaration of the Tool Factory
DECLARE_COMPONENT( MCFTG4AttenuationInterpolationTool )

//=========================================================================
// Retrieve the Attenuation map from the CondDB
//=========================================================================
StatusCode MCFTG4AttenuationInterpolationTool::initialize() {
  auto sc = base_class::initialize();
  if ( sc.isFailure() ) { return sc; }

  // When conditions are not found, return failure
  if ( existDet<Condition>( m_parameterLocation ) == false ) {
    return Error( "No parameter maps found in SIMCOND: update to a newer tag", StatusCode::FAILURE );
  }

  processParameterMap( m_parameterLocation );

  if ( !validateMap() ) {
    return Error( "Consitency check of parameter map failed: Please check SIMCOND tag", StatusCode::FAILURE );
  }
  fillMaps();

  if ( msgLevel( MSG::DEBUG ) ) { debug() << "nBinsX :" << m_nBinsX << "	nBinsY: " << m_nBinsY << std::endl; }

  return sc;
}

//=============================================================================
// Return calculated attenution to MCFTDepositCreator
//=============================================================================
IMCFTAttenuationTool::Attenuation MCFTG4AttenuationInterpolationTool::attenuation( double hitXPosition,
                                                                                   double hitYPosition ) const {
  hitXPosition = std::abs( hitXPosition );
  hitYPosition = std::abs( hitYPosition );

  int binX = findBin( m_xEdges, hitXPosition );
  int binY = findBin( m_yEdges, hitYPosition );

  // Hit outside quadrant (should never happen!)
  if ( binX == -1 || binY == -1 ) {
    return {0.0, 0.0};
  } else {
    return {m_effDir[binY * m_nBinsX + binX], m_effRef[binY * m_nBinsX + binX]};
  }
}

MCFTG4AttenuationInterpolationTool::ParameterName
MCFTG4AttenuationInterpolationTool::convert( std::string const& key ) const {
  if ( key == "mask" ) return ParameterName::mask;
  if ( key == "a_refl" ) return ParameterName::a_refl;
  if ( key == "a_dir" ) return ParameterName::a_dir;
  if ( key == "b_refl" ) return ParameterName::b_refl;
  if ( key == "b_dir" ) return ParameterName::b_dir;
  if ( key == "c_refl" ) return ParameterName::c_refl;
  if ( key == "c_dir" ) return ParameterName::c_dir;
  if ( key == "d_refl" ) return ParameterName::d_refl;
  if ( key == "d_dir" ) return ParameterName::d_dir;
  if ( key == "y_pos" )
    return ParameterName::y_pos;
  else
    throw std::runtime_error( "unknown ParameterName " + key );
}

//=============================================================================
// Read in and validate parameter map from CondDB
//=============================================================================
void MCFTG4AttenuationInterpolationTool::processParameterMap( const std::string& conditionsLocation ) {

  // Get the conditions
  Condition* cond = getDet<Condition>( conditionsLocation );

  // Process binning information
  m_nBinsX = cond->param<int>( "x_n_bins" );
  m_nBinsY = cond->param<int>( "y_n_bins" );
  m_xEdges = cond->param<std::vector<double>>( "x_edges" );
  m_yEdges = cond->param<std::vector<double>>( "y_edges" );
  std::sort( m_xEdges.begin(), m_xEdges.end() );
  std::sort( m_yEdges.begin(), m_yEdges.end() );

  // Loop over maps of parameters
  for ( auto parameterName : cond->paramNames() ) {
    if ( s_binParams.count( parameterName ) != 0 ) { continue; }
    m_parameters[convert( parameterName )] = cond->param<std::vector<double>>( parameterName );
  } // for
}

bool MCFTG4AttenuationInterpolationTool::validateMap() {
  if ( m_nBinsX != m_xEdges.size() - 1 || m_nBinsY != m_yEdges.size() - 1 ) {
    return false;
  } else {
    return std::none_of( m_parameters.begin(), m_parameters.end(),
                         [nb = m_nBinsX * m_nBinsY]( const auto& i ) { return nb != std::get<1>( i ).size(); } );
  }
}

int MCFTG4AttenuationInterpolationTool::findBin( LHCb::span<const double> axis, double position ) const {
  // axis container must not be empty to prevent undefined behaviour
  assert( !axis.empty() );

  auto iterator = std::upper_bound( axis.begin(), axis.end(), position );
  // If position is outside of map: use first or last bin.
  if ( iterator == axis.end() )
    --iterator;
  else if ( iterator == axis.begin() )
    ++iterator;
  return iterator - axis.begin() - 1;
}

void MCFTG4AttenuationInterpolationTool::fillMaps() {
  // Map can only be filled once
  assert( m_effDir.empty() );
  assert( m_effRef.empty() );

  m_effDir.reserve( m_nBinsX * m_nBinsY );
  m_effRef.reserve( m_nBinsX * m_nBinsY );

  for ( unsigned int bin = 0; bin < m_nBinsX * m_nBinsY; ++bin ) {
    m_effDir.push_back( calculateAttenuation( bin, ReflectedPhotons{false} ) );
    m_effRef.push_back( calculateAttenuation( bin, ReflectedPhotons{true} ) * m_mirrorReflectivity );
  }
}

double MCFTG4AttenuationInterpolationTool::calculateAttenuation(
    int binID, MCFTG4AttenuationInterpolationTool::ReflectedPhotons isRefl ) {
  if ( isRefl ) {
    return m_parameters[ParameterName::mask][binID] *
           ( ( m_parameters[ParameterName::a_refl][binID] * m_irrad +
               +m_parameters[ParameterName::b_refl][binID] *
                   exp( -m_parameters[ParameterName::c_refl][binID] * m_irrad ) +
               m_parameters[ParameterName::d_refl][binID] ) *
             exp( -4.2e-4 * ( m_parameters[ParameterName::y_pos][binID] + 2.5 ) * m_fibreAge ) );

  } else {
    return m_parameters[ParameterName::mask][binID] *
           ( ( m_parameters[ParameterName::a_dir][binID] * m_irrad +
               +m_parameters[ParameterName::b_dir][binID] *
                   exp( -m_parameters[ParameterName::c_dir][binID] * m_irrad ) +
               m_parameters[ParameterName::d_dir][binID] ) *
             exp( -4.2e-4 * ( 2.5 - m_parameters[ParameterName::y_pos][binID] ) * m_fibreAge ) );
  }
}
