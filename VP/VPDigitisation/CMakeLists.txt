###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# Package: VPDigitisation
###############################################################################
gaudi_subdir(VPDigitisation)

gaudi_depends_on_subdirs(Det/VPDet
                         Event/DigiEvent
                         Event/LinkerEvent
                         Event/MCEvent
                         Event/GenEvent
                         GaudiAlg
                         Kernel/LHCbKernel)

find_package(GSL)

find_package(Boost)
find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_module(VPDigitisation
                 src/*.cpp
                 INCLUDE_DIRS GSL AIDA HepMC Event/DigiEvent
                 LINK_LIBRARIES GSL VPDetLib LinkerEvent GenEvent MCEvent GaudiAlgLib LHCbKernel)
