/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MERGEEVENTALG_H
#define MERGEEVENTALG_H 1

// Include files
#include "GaudiAlg/GaudiAlgorithm.h"
#include "GaudiKernel/IEvtSelector.h"
#include "GaudiKernel/Service.h"

// Forward declarations
struct IDataManagerSvc;
class IDataProviderSvc;
class IConversionSvc;
class EventSelector;
class DataStoreItem;
class ILumiTool;

/** @class MergeEventAlg MergeEventAlg.h
 *
 *  Merge events from different OO files and put them in different
 *  TES locations. Initializes an EventSelector per additional file
 *
 *  Currently implements:
 *     merging of flat event from LHC background
 *     merging of luminosity weighted spillover
 *
 *  @author Gloria CORTI
 *  @date   2003-06-23
 */

class MergeEventAlg : public GaudiAlgorithm {

public:
  /// Typedefs
  typedef std::vector<std::unique_ptr<DataStoreItem>> Items;
  typedef std::vector<std::string>                    ItemNames;

  /// Standard constructor
  MergeEventAlg( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution
  StatusCode finalize() override;   ///< Algorithm finalization

private:
  /// Add item to list of data objects to load new path
  void addItem( Items& itms, const std::string& descriptor );

  /// Read LHC background event and load it into transient Event Store
  StatusCode readLHCBackground();

  /// Read spillover event and load it into transient Event Store
  StatusCode readSpillover();

  /// Read additional event and load it into transient Event Store
  StatusCode readAndLoadEvent( const std::string& subPath );

  /// Read leaves of unil specified depth
  StatusCode readLeaves( const std::string& subPath, const DataObject* pObj, long depth );

  /// Reset path of references attached to object
  StatusCode resetLinks( const std::string& subPath, const DataObject* pObj );

  /* Reset path. When "/Event" in in the original path if subPath is ""
   * "/Event/" will be removed from the path, otherwise subPath will be
   * introduced after "/Event/"
   * When "/Event" is not in the original path, subPath is prepended to
   * the original path
   */
  std::string resetPath( const std::string& oldPath, const std::string& subPath = "" );

  /// Type of events to merge
  std::string m_mergeType;
  /// Name of additional event selector
  std::string m_mergeSelectorName;
  /// Vector of subPaths to be populated
  ItemNames m_subPaths;

  /// Vector of item names
  ItemNames m_itemNames;
  /// Vector of items to be loaded from this event selector
  Items m_itemList;

  /// Service of additional event selector
  Service* m_mergeSelector = nullptr;
  /// IEvtSelector interface of additional event selector
  IEvtSelector* m_mergeISelector = nullptr;
  /// EventSelector iterator
  IEvtSelector::Context* m_mergeIt = nullptr;
  /// Luminosity tool
  ILumiTool*   m_lumiTool     = nullptr;
  unsigned int m_eventCounter = 0; ///< Number of events read
};
#endif // MERGEEVENTALG_H
