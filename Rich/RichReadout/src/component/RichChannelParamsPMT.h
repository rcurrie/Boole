/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef RichChannelParamsPMT_h
#define RichChannelParamsPMT_h 1

#include "RichPixelReadout.h"

namespace Rich {
  namespace MC {
    namespace Digi {

      class RichPixel;

      /// General pixel properties
      class RichChannelParamsPMT {

      public:
        RichChannelParamsPMT() {}

        ~RichChannelParamsPMT() {}

        // GET
        double pixelMeanGain() { return m_pixelMeanGain; }

        double pixelSigmaGain() { return m_pixelSigmaGain; }

        double pixelSigmaNoise() { return m_pixelSigmaNoise; }

        double channelThreshold() { return m_channelThreshold; }

        // SET
        void setPixelMeanGain( double val ) { m_pixelMeanGain = val; }

        void setPixelSigmaGain( double val ) { m_pixelSigmaGain = val; }
        void setPixelSigmaNoise( double val ) { m_pixelSigmaNoise = val; }

        void setChannelThreshold( double val ) { m_channelThreshold = val; }

      private:
        // Parameters
        double m_pixelMeanGain{0};
        double m_pixelSigmaGain{0};
        double m_pixelSigmaNoise{0};
        double m_channelThreshold{0};
      };

    } // namespace Digi
  }   // namespace MC
} // namespace Rich

#endif
