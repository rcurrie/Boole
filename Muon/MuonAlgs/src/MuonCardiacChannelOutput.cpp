/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "MuonCardiacChannelOutput.h"
#include "MuonCardiacTraceBack.h"
#include "MuonDet/DeMuonDetector.h"
#include "MuonHitTraceBack.h"
#include <iostream>

void MuonCardiacChannelOutput::addPhyChannel( MuonPhysicalChannelOutput* value ) {
  m_phChannel.push_back( value );
  for ( MuonHitTraceBack& i : value->hitsTraceBack() ) {
    if ( i.getMCMuonHistory().isHitFiring() ) {
      m_timeList.emplace_back( i.hitArrivalTime(), MuonCardiacTraceBack{&i} );
    }
  }
}

void MuonCardiacChannelOutput::processForDeadTime( float value, float timeGate ) {
  fillhitsTraceBack();
  std::stable_sort( m_timeList.begin(), m_timeList.end(),
                    []( const auto& p1, const auto& p2 ) { return p1.first < p2.first; } );
  double timeOfStartOfDeadtime = -5000;
  double lenghtOfDeadtime      = value;
  for ( auto& [timeOfHit, tb] : m_timeList ) {
    if ( timeOfHit < timeOfStartOfDeadtime + lenghtOfDeadtime ) {
      // hit in deadtime
      tb.setDeadTime();
    } else {
      // (re)start the deadtime
      timeOfStartOfDeadtime = timeOfHit;
    }
  }
  // now set the firing time in the current BX
  bool fired = false;
  for ( const auto& [timeOfHit, tb] : m_timeList ) {
    if ( !tb.getDeadTime() && timeOfHit > 0 && timeOfHit < timeGate ) {
      setFiringTime( timeOfHit );
      fired = true;
      setChInfoFiring( tb );
      m_fired = true;
    }
  }
  // if all not fired put the info of the one that would have
  // fired without deadtime
  if ( !fired ) UpdateMCInfo();
}

void MuonCardiacChannelOutput::fillhitsTraceBack() {
  for ( auto& it : m_phChannel ) {
    for ( auto& iterOrigin : it->hitsTraceBack() ) { m_Hits.push_back( &iterOrigin ); }
  }
}

void MuonCardiacChannelOutput::UpdateMCInfo() {

  // loop on all ph ch and create the MCMuonDigitInfo for the Cardiac ch.
  if ( !m_fired ) {
    assert( !m_phChannel.empty() );
    // set the MCMuonDigitInfo equal to the first
    m_phChInfo.setDigitInfo( m_phChannel.front()->phChInfo().DigitInfo() );

    for ( auto iterpc = std::next( m_phChannel.begin() ); iterpc != m_phChannel.end(); ++iterpc ) {
      LHCb::MCMuonDigitInfo other = ( *iterpc )->phChInfo();
      if ( other.isInDeadTime() ) m_phChInfo.setDeadtimeDigit( 1 );
      if ( other.isDeadForChamberInefficiency() ) m_phChInfo.setChamberInefficiencyDigit( 1 );
      if ( other.isDeadByTimeJitter() ) m_phChInfo.setTimeJitteredDigit( 1 );
      if ( other.isDeadByTimeAdjustment() ) m_phChInfo.setTimeAdjDigit( 1 );
      if ( other.isDeadByGeometry() ) m_phChInfo.setGeometryInefficiency( 1 );
      if ( other.isInDialogDeadTime() ) m_phChInfo.setDialogDeadtimeDigit( 1 );
    }
    m_phChInfo.setAliveDigit( 0 );
  }
  // now the retrieve of the firing info and setting of it
}

void MuonCardiacChannelOutput::setFiringTime() {
  for ( const auto& [timeOfHit, tb] : m_timeList ) {
    if ( !tb.getDeadTime() && timeOfHit > 0 && timeOfHit < 25 ) {
      setFiringTime( timeOfHit );
      //        return;
    }
  }
}

// void Muon
void MuonCardiacChannelOutput::setChInfoFiring( MuonCardiacTraceBack pointer ) {
  // this hit has fired the electronics ...
  // so get the PH to which belongs and copy the chinfo
  MuonPhysicalChannelOutput* phchannel = getPhCh( pointer );
  m_phChInfo                           = phchannel->phChInfo();
  m_phChInfo.setAliveDigit( 1 );
}

MuonPhysicalChannelOutput* MuonCardiacChannelOutput::getPhCh( MuonCardiacTraceBack pointer ) {
  for ( auto& itph : m_phChannel ) {
    for ( auto& iterOrigin : itph->hitsTraceBack() ) {
      MuonHitTraceBack* po = &iterOrigin;
      if ( po == pointer.getHitTrace() ) return itph;
    }
  }
  return nullptr;
}
