/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
//
// This File contains the monitor for muon Digi system
//
// C++ code for 'LHCb Muon package(s)'
//
//   Author: A. Sarti
//   Created: 20-04-2005

// from STL
#include <string>

// Local
#include "Event/MCHeader.h"
#include "Event/MCHit.h"
#include "Event/MCMuonDigit.h"
#include "Event/MuonDigit.h"
#include "MuonDet/DeMuonDetector.h"
#include "MuonDigitChecker.h"
#include <limits>
#include <sstream>

namespace {
  constexpr auto NaN = std::numeric_limits<double>::signaling_NaN();
}

DECLARE_COMPONENT( MuonDigitChecker )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
MuonDigitChecker::MuonDigitChecker( const std::string& name, ISvcLocator* pSvcLocator )
    : GaudiTupleAlg( name, pSvcLocator ), m_hitMonitor( false ) {
  declareProperty( "hitMonitor", m_hitMonitor );
  //  setProperty( "NTupleProduce", "false" );
  setProperty( "HistoTopDir", "MUON/" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  setProperty( "NTupleTopDir", "MUON/" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
}

//=============================================================================
// Initialisation. Check parameters
//=============================================================================
StatusCode MuonDigitChecker::initialize() {

  StatusCode sc = GaudiTupleAlg::initialize();
  if ( sc.isFailure() ) { return Error( "Failed to initialize", sc ); }

  for ( int ix = 0; ix < 6; ix++ ) {
    if ( ix < 4 && m_hitMonitor ) {
      for ( int i = 0; i < 5; i++ ) {
        for ( int j = 0; j < 4; j++ ) { m_nhit[i][j][ix] = m_cnt[i][j][ix] = 0; }
      }
    }
    for ( int i = 0; i < 5; i++ ) {
      for ( int j = 0; j < 4; j++ ) { m_nDhit[i][j][ix] = m_Dcnt[i][j][ix] = 0; }
    }
  }

  m_base = std::make_unique<MuonBasicGeometry>( this->detSvc(), this->msgSvc() );
  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode MuonDigitChecker::execute() {

  const LHCb::MCHeader* evt = get<LHCb::MCHeader>( LHCb::MCHeaderLocation::Default, IgnoreRootInTES );

  int tnhit[5][4][4];
  int tnDhit[5][4][6];
  for ( int ix = 0; ix < 6; ix++ ) {
    if ( ix < 4 && m_hitMonitor ) {
      for ( int i = 0; i < 5; i++ ) {
        for ( int j = 0; j < 4; j++ ) { tnhit[i][j][ix] = 0; }
      }
    }
    for ( int i = 0; i < 5; i++ ) {
      for ( int j = 0; j < 4; j++ ) { tnDhit[i][j][ix] = 0; }
    }
  }

  std::vector<float> sta, reg, cha, con, x, y, z, vtime, id;
  std::vector<float> px, py, pz, E, xv, yv, zv, tv, mom;
  std::vector<float> ple, hen, dix, dxz, dyz;

  std::vector<float> digit_s, digit_r, digit_x, digit_y;
  std::vector<float> digit_z, digit_dx, digit_dy, digit_dz, digit_time;
  std::vector<float> digit_origin, digit_bx, digit_firing, digit_multi;

  DeMuonDetector* muonD = getDet<DeMuonDetector>( DeMuonLocation::Default );

  std::string spill[5]        = {"", "/Prev", "/PrevPrev", "/Next", "/NextNext"};
  std::string TESContainer[4] = {"Hits", "ChamberNoiseHits", "FlatSpilloverHits", "BackgroundHits"};

  // loop over Muon Hits only if required
  if ( m_hitMonitor ) {

    // get the MCHits
    // container :  0 = Geant
    //              1 = Chamber Noise
    //              2 = Flat Spill Background
    //              3 = Low Energy Background

    for ( int container = 0; container < 4; container++ ) {
      // spill[0] = current event
      //       n  (n=1,4) = event preceeding the current for n*25 ns

      std::string path = "/Event" + spill[0] + LHCb::MCHitLocation::Muon + TESContainer[container];

      LHCb::MCHits* hits = get<LHCb::MCHits>( LHCb::MCHitLocation::Muon );

      int MyDetID;
      // Loop over Muon Hits of given type
      if ( hits ) {
        for ( const auto* hit : *hits ) {

          MyDetID = hit->sensDetID();
          if ( MyDetID < 0 ) continue;
          // Needs to extract info from sens ID
          int station = muonD->stationID( MyDetID );
          int region  = muonD->regionID( MyDetID );
          int chamber = muonD->chamberID( MyDetID );

          if ( msgLevel( MSG::DEBUG ) ) debug() << " region:: " << region << endmsg;

          float xpos = ( hit->entry().x() + hit->exit().x() ) / 2.0;
          float ypos = ( hit->entry().y() + hit->exit().y() ) / 2.0;
          float zpos = ( hit->entry().z() + hit->exit().z() ) / 2.0;
          float time = hit->time();

          double tof = time - sqrt( xpos * xpos + ypos * ypos + zpos * zpos ) / 300.0;
          if ( tof < 0.1 ) tof = 0.1;
          float r = sqrt( xpos * xpos + ypos * ypos );

          sta.push_back( station );
          reg.push_back( region );
          cha.push_back( chamber );
          con.push_back( container );

          // Temporary monitoring (need to check if already available)
          ple.push_back( hit->pathLength() );
          hen.push_back( hit->energy() );
          dix.push_back( hit->displacement().x() );
          dxz.push_back( hit->dxdz() );
          dyz.push_back( hit->dydz() );

          x.push_back( xpos );
          y.push_back( ypos );
          z.push_back( zpos );
          vtime.push_back( time );

          // Fill some histos
          int hh = station * 4 + region;

          plot( r, hh + 2000, "Radial Multiplicity", 0., 6000., 200 );
          plot( tof, hh + 1000, "Time multiplicity", 0., 100., 200 );

          // MC truth
          const LHCb::MCParticle* particle = hit->mcParticle();
          if ( particle ) {
            if ( abs( particle->particleID().pid() ) < 100000 ) { id.push_back( particle->particleID().pid() ); }

            px.push_back( particle->momentum().px() );
            py.push_back( particle->momentum().py() );
            // Pz sign tells you the particle direction
            pz.push_back( particle->momentum().pz() );
            E.push_back( particle->momentum().e() );

            // Particle Vertex studies
            xv.push_back( particle->originVertex()->position().x() );
            yv.push_back( particle->originVertex()->position().y() );
            zv.push_back( particle->originVertex()->position().z() );
            tv.push_back( particle->originVertex()->time() );

            const LHCb::MCParticle* moth = particle->mother();
            if ( moth && ( abs( moth->particleID().pid() ) < 100000 ) ) {
              mom.push_back( moth->particleID().pid() );
            } else {
              mom.push_back( 0 );
            }
          } else {
            id.push_back( 0 );
            px.push_back( 0 );
            py.push_back( 0 );
            pz.push_back( 0 );
            E.push_back( 0 );
            xv.push_back( 0 );
            yv.push_back( 0 );
            zv.push_back( 0 );
            tv.push_back( 0 );
            mom.push_back( 0 );
          }
          tnhit[station][region][container]++;
        }
      }
    }
    for ( int ic = 0; ic < 4; ic++ ) {
      for ( int r = 0; r < 4; r++ ) {
        for ( int s = 0; s < 5; s++ ) {
          // Looking at mean number of hits
          m_cnt[s][r][ic]++;
          m_nhit[s][r][ic] += tnhit[s][r][ic];
        }
      }
    }
  }

  // Loop on Digits (strips)
  // get the MCMuonDigits
  LHCb::MuonDigits*   digit   = get<LHCb::MuonDigits>( LHCb::MuonDigitLocation::MuonDigit );
  LHCb::MCMuonDigits* mcdigit = get<LHCb::MCMuonDigits>( LHCb::MCMuonDigitLocation::MCMuonDigit );

  int    Dsta, Dreg, Dcon;
  double Dfir;
  bool   Deve;

  for ( const auto* jdigit : *digit ) {
    Dsta = jdigit->key().station();
    Dreg = jdigit->key().region();

    digit_s.push_back( Dsta );
    digit_r.push_back( Dreg );
    digit_time.push_back( jdigit->TimeStamp() );

    // Timestamp is one of the 8 intervals (8 digits) in which
    // the 20ns acceptance window is subdivided after beam crossing
    double     x = NaN, y = NaN, z = NaN;
    double     dx = NaN, dy = NaN, dz = NaN;
    StatusCode sc = muonD->Tile2XYZ( jdigit->key(), x, dx, y, dy, z, dz );
    if ( sc.isFailure() && msgLevel( MSG::DEBUG ) ) debug() << "error in tile " << endmsg;
    digit_x.push_back( x );
    digit_y.push_back( y );
    digit_z.push_back( z );

    digit_dx.push_back( dx );
    digit_dy.push_back( dy );
    digit_dz.push_back( dz );

    // Match with "true" MC digit
    LHCb::MCMuonDigit* MCmd = mcdigit->object( jdigit->key() );
    if ( NULL == MCmd ) {
      error() << "Could not find the match for " << jdigit->key() << " in " << LHCb::MCMuonDigitLocation::MCMuonDigit
              << endmsg;
      return StatusCode::FAILURE;
    }

    LHCb::MCMuonDigitInfo digInfo = MCmd->DigitInfo();

    // Hit orginin (codes in MuonEvent/v2r1/Event/MuonOriginFlag.h)
    // Geant                 = 0
    // FlastSpillOver        = 1
    // Low Energy Background = 2
    // Chamber Noise         = 3
    // Crosstalk             = 4
    // Electronic Noise      = 5
    Dcon = digInfo.natureOfHit();
    Deve = digInfo.doesFiringHitBelongToCurrentEvent();
    // bool Dali = digInfo.isAlive(); // not used
    Dfir = MCmd->firingTime();
    digit_firing.push_back( Dfir );
    digit_origin.push_back( Dcon );
    digit_bx.push_back( Deve );

    // Hits mupltiplicity
    digit_multi.push_back( MCmd->mcHits().size() );
    // if(Deve)
    tnDhit[Dsta][Dreg][Dcon]++;
    // if(globalTimeOffset()>0)info()<<" qui "<<Dcon<<" "<<Dfir<<" "<<Deve<<" "<<Dsta<<" "<<Dreg<<endmsg;
  }

  // Looking at mean number of hits
  for ( int c = 0; c < 6; c++ ) {
    for ( int r = 0; r < 4; r++ ) {
      for ( int s = 0; s < 5; s++ ) {
        m_Dcnt[s][r][c]++;
        m_nDhit[s][r][c] += tnDhit[s][r][c];
      }
    }
  }
  if ( m_hitMonitor ) {
    Tuple nt1 = nTuple( 41, "MC HITS", CLID_ColumnWiseTuple );

    longlong   evtNr = evt->evtNumber();
    StatusCode sc    = nt1->column( "Event", evtNr, 0LL, 10000LL );
    if ( sc.isFailure() && msgLevel( MSG::DEBUG ) ) debug() << "nt error" << endmsg;

    nt1->farray( "is", sta, "Nhits", 1000 ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    nt1->farray( "ir", reg, "Nhits", 1000 ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    nt1->farray( "ch", cha, "Nhits", 1000 ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    nt1->farray( "ic", con, "Nhits", 1000 ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    nt1->farray( "x", x, "Nhits", 1000 ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    nt1->farray( "y", y, "Nhits", 1000 ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    nt1->farray( "z", z, "Nhits", 1000 ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    nt1->farray( "t", vtime, "Nhits", 1000 ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    nt1->farray( "id", id, "Nhits", 1000 ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    nt1->farray( "px", px, "Nhits", 1000 ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    nt1->farray( "py", py, "Nhits", 1000 ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    nt1->farray( "pz", pz, "Nhits", 1000 ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    nt1->farray( "E", E, "Nhits", 1000 ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    nt1->farray( "xv", xv, "Nhits", 1000 ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    nt1->farray( "yv", yv, "Nhits", 1000 ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    nt1->farray( "zv", zv, "Nhits", 1000 ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    nt1->farray( "tv", tv, "Nhits", 1000 ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    nt1->farray( "idm", mom, "Nhits", 1000 ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    nt1->farray( "pl", ple, "Nhits", 1000 ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    nt1->farray( "he", hen, "Nhits", 1000 ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    nt1->farray( "dx", dix, "Nhits", 1000 ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    nt1->farray( "xz", dxz, "Nhits", 1000 ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    nt1->farray( "yz", dyz, "Nhits", 1000 ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    sc = nt1->write();
    if ( sc.isFailure() && msgLevel( MSG::DEBUG ) ) debug() << "Failure writing nt1" << endmsg;

    Tuple nt2 = nTuple( 42, "DIGITS", CLID_ColumnWiseTuple );

    longlong digit_evt = evt->evtNumber();
    //    StatusCode sc=nt2->column("Event", digit_evt, 0LL,10000LL);
    sc = nt2->column( "Event", digit_evt, 0LL, 10000LL );
    if ( sc.isFailure() ) debug() << " nt2 error " << endmsg;

    sc = nt2->farray( "is", digit_s, "Ndigits", 1000 );
    sc = nt2->farray( "ir", digit_r, "Ndigits", 1000 );
    sc = nt2->farray( "x", digit_x, "Ndigits", 1000 );
    sc = nt2->farray( "y", digit_y, "Ndigits", 1000 );
    sc = nt2->farray( "z", digit_z, "Ndigits", 1000 );
    sc = nt2->farray( "dx", digit_dx, "Ndigits", 1000 );
    sc = nt2->farray( "dy", digit_dy, "Ndigits", 1000 );
    sc = nt2->farray( "dz", digit_dz, "Ndigits", 1000 );
    sc = nt2->farray( "t", digit_time, "Ndigits", 1000 );
    sc = nt2->farray( "origin", digit_origin, "Ndigits", 1000 );
    sc = nt2->farray( "bx", digit_bx, "Ndigits", 1000 );
    sc = nt2->farray( "firing", digit_firing, "Ndigits", 1000 );
    sc = nt2->farray( "multip", digit_multi, "Ndigits", 1000 );
    sc = nt2->write();
    if ( sc.isFailure() && msgLevel( MSG::DEBUG ) ) debug() << "Failure writing nt2" << endmsg;
  }
  return StatusCode::SUCCESS;
}

//=============================================================================
//  Finalize
//=============================================================================

StatusCode MuonDigitChecker::finalize() {

  int nStations = m_base->getStations();
  //  info()<<"Stazioni trovate: "<<nStations<<endmsg;;
  std::stringstream statStr;
  for ( int i = 0; i < nStations; i++ ) {
    std::string statName = m_base->getStationName( i );
    statStr << statName << "     ";
  }

  info() << "-----------------------------------------------------------------" << endmsg;
  info() << "       Muon Monitoring Table " << endmsg;
  info() << "-----------------------------------------------------------------" << endmsg;
  if ( fullDetail() == true && m_hitMonitor ) {
    std::string TESn[4] = {"Hits", "ChamberNoiseHits", "FlatSpilloverHits", "BackgroundHits"};

    info() << "       Hit Information     " << endmsg;

    for ( int c = 0; c < 4; c++ ) {
      std::string myTes = TESn[c];
      info() << "-----------------------------------------------------------------" << endmsg;
      info() << "----- TES Container: " << myTes << "------------------------" << endmsg;
      info() << "-----------------------------------------------------------------" << endmsg;
      //      info()<<" M1      M2      M3      M4      M5 "<<endmsg;
      info() << statStr.str() << endmsg;
      for ( int r = 0; r < 4; r++ ) {
        for ( int s = 0; s < nStations; s++ ) {
          if ( m_cnt[s][r][c] ) {
            info() << format( "%5.3lf  ", (double)m_nhit[s][r][c] / m_cnt[s][r][c] );
          } else {
            info() << "0.000  ";
          }
        }
        info() << " R" << r + 1 << endmsg;
      }
    }
  }

  info() << "-----------------------------------------------------------------" << endmsg;
  info() << "    Digi Information     " << endmsg;
  info() << "-----------------------------------------------------------------" << endmsg;

  std::string HitOr[6] = {"Geant",         "FlastSpillOver", "Low Energy Background",
                          "Chamber Noise", "Crosstalk",      "Electronic Noise"};

  for ( int c = 0; c < 6; c++ ) {
    std::string myCon = HitOr[c];
    info() << "-----------------------------------------------------------------" << endmsg;
    info() << "-----   Container: " << myCon << "------------------------" << endmsg;
    info() << "-----------------------------------------------------------------" << endmsg;
    //    info()<<"   M1     M2     M3     M4     M5 "<<endmsg;
    info() << statStr.str() << endmsg;
    for ( int r = 0; r < 4; r++ ) {
      for ( int s = 0; s < nStations; s++ ) {
        if ( m_Dcnt[s][r][c] ) {
          info() << format( "%5.3lf  ", (double)m_nDhit[s][r][c] / m_Dcnt[s][r][c] );
        } else {
          info() << "0.000  ";
        }
      }
      info() << " R" << r + 1 << endmsg;
    }
  }

  m_base.reset();

  // Execute the base class finalize
  return GaudiTupleAlg::finalize();
}
