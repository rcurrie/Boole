/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MCFTPHOTONMONITOR_H
#define MCFTPHOTONMONITOR_H 1

// Include files
// from Gaudi
#include "GaudiAlg/Consumer.h"
#include "GaudiAlg/GaudiHistoAlg.h"

// from Event
#include "Event/MCHit.h"

// from FTEvent
#include "Event/MCFTPhoton.h"

/** @class MCFTPhotonMonitor MCFTPhotonMonitor.h
 *
 *
 *  @author Violaine Bellee, Julian Wishahi, Luca Pescatore
 *  @date   2016-12-18
 */
class MCFTPhotonMonitor : public Gaudi::Functional::Consumer<void( const LHCb::MCFTPhotons& ),
                                                             Gaudi::Functional::Traits::BaseClass_t<GaudiHistoAlg>> {

public:
  // using GaudiHistoAlg::GaudiHistoAlg;
  MCFTPhotonMonitor( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override;
  void       operator()( const LHCb::MCFTPhotons& deposits ) const override;

private:
  // Gaudi properties

  // AttenuatioTool to plot the attenuation maps
  Gaudi::Property<std::string> m_attenuationToolName{this, "AttenuationToolName", "MCFTG4AttenuationTool",
                                                     "Name of the MCFTAttenuationTool"};
};
#endif // MCFTPHOTONMONITOR_H
