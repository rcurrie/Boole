/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef IMCFTDISTRIBUTIONFIBRETOOL_H
#define IMCFTDISTRIBUTIONFIBRETOOL_H 1

// from Gaudi
#include "GaudiKernel/IAlgTool.h"

/** @class IMCFTDistributionFibreTool IMCFTDistributionFibreTool.h
 *
 *  Interface for the tool that calculates the coordinates and fractional path
 *  length in the core of each fibre
 *
 *  @author Julian Wishahi, Tobias Tekampe
 *  @date   2015-10-27
 */

struct IMCFTDistributionFibreTool : extend_interfaces<IAlgTool> {

  // Return the interface ID
  DeclareInterfaceID( IMCFTDistributionFibreTool, 1, 0 );

  // Return a vector of fibre positions and for each the path length fraction in
  // the fibre core
  virtual std::vector<std::pair<Gaudi::XYZPoint, double>>
  effectivePathFracInCores( const Gaudi::XYZPoint& enPoint, const Gaudi::XYZPoint& exPoint ) const = 0;
};
#endif // IMCFTDISTRIBUTIONFIBRETOOL_H
