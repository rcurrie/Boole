/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// from FTEvent
#include "Event/MCFTDeposit.h"

// local
#include "FTClusterCreator.h"

//-----------------------------------------------------------------------------
// Implementation file for class : FTClusterCreator
//
// 2012-04-06 : Eric Cogneras
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( FTClusterCreator )

FTClusterCreator::FTClusterCreator( const std::string& name, ISvcLocator* pSvcLocator )
    : MultiTransformer(
          name, pSvcLocator, {KeyValue{"InputLocation", LHCb::MCFTDigitLocation::Default}},
          {KeyValue{"OutputLocation", LHCb::FTClusterLocation::Default},
           KeyValue{"MCToClusterLocation", Links::location( LHCb::FTLiteClusterLocation::Default )},
           KeyValue{"MCToClusterExtLocation",
                    Links::location( LHCb::FTLiteClusterLocation::Default + "WithSpillover" )},
           KeyValue{"HitToClusterLocation", Links::location( LHCb::FTLiteClusterLocation::Default + "2MCHits" )},
           KeyValue{"HitToClusterExtLocation",
                    Links::location( LHCb::FTLiteClusterLocation::Default + "2MCHitsWithSpillover" )}} ) {}

//=============================================================================
// Main execution
//=============================================================================
std::tuple<LHCb::FTClusters, LHCb::LinksByKey, LHCb::LinksByKey, LHCb::LinksByKey, LHCb::LinksByKey> FTClusterCreator::
                                                                                                     operator()( const LHCb::MCFTDigits& digits ) const {

  // Create the objects to be returned and reserve memory.
  OutputLinks<ContainedObject, LHCb::MCParticle> mcToClusterLink{};
  OutputLinks<ContainedObject, LHCb::MCParticle> mcToClusterLinkExtended{};
  OutputLinks<ContainedObject, LHCb::MCHit>      hitToClusterLink{};
  OutputLinks<ContainedObject, LHCb::MCHit>      hitToClusterLinkExtended{};

  LHCb::FTClusters clusterCont{};
  clusterCont.reserve( digits.size() );

  clusterCandidates candidates{};
  candidates.reserve( 15e3 );

  findClusterCandidates( digits, candidates );
  makeClusters( candidates, clusterCont );

  // Linkers
  for ( auto clusIter = clusterCont.begin(); clusIter < clusterCont.end(); ++clusIter ) {

    // check contributing from MCHits
    std::set<const LHCb::MCHit*> contributingMCHits;

    const auto&       clus   = *clusIter;
    LHCb::FTChannelID chanID = clus->channelID();

    if ( clus->isLarge() == 1 and clus->fraction() == 0 )
      chanID = floor( clus->channelID() - float( clus->size() - 1 ) / 2 );

    for ( auto clusDigit : clus->channelIDs() ) {
      for ( const auto& deposit : digits.object( clusDigit )->deposits() ) {
        const auto& mcHit = deposit->mcHit();
        if ( mcHit != nullptr ) { contributingMCHits.insert( mcHit ); }
      }
    }

    // make links
    for ( const auto& i : contributingMCHits ) {
      hitToClusterLinkExtended.link( chanID, i ); // Needed for xdigi / xdst
      if ( i->parent()->registry()->identifier() == "/Event/" + LHCb::MCHitLocation::FT )
        hitToClusterLink.link( chanID, i );

      const auto part = ( i->mcParticle() );
      if ( part != nullptr ) {
        mcToClusterLinkExtended.link( chanID, part ); // Needed for xdigi / xdst
        if ( part->parent()->registry()->identifier() == "/Event/" + LHCb::MCParticleLocation::Default )
          mcToClusterLink.link( chanID, part );
      }
    } // mchits in the cluster

    // now modified clusters
    if ( clus->isLarge() == 1 && clusIter < clusterCont.end() - 1 ) {
      const auto& nextclus = *( clusIter + 1 );

      if ( nextclus->isLarge() == 1 ) {

        auto startDigit = clus->channelIDs().front();
        int  width      = clus->size() + nextclus->size();
        int  newChan    = startDigit + float( width - 1 ) / 2;

        for ( auto clusDigit : nextclus->channelIDs() ) {
          for ( const auto& deposit : digits.object( clusDigit )->deposits() ) {
            const auto& mcHit = deposit->mcHit();
            if ( mcHit != nullptr ) { contributingMCHits.insert( mcHit ); }
          }
        }

        for ( const auto& i : contributingMCHits ) {
          hitToClusterLinkExtended.link( newChan, i ); // Needed for xdigi / xdst
          if ( i->parent()->registry()->identifier() == "/Event/" + LHCb::MCHitLocation::FT )
            hitToClusterLink.link( newChan, i );

          const auto part = ( i->mcParticle() );
          if ( part != nullptr ) {
            mcToClusterLinkExtended.link( newChan, part ); // Needed for xdigi / xdst
            if ( part->parent()->registry()->identifier() == "/Event/" + LHCb::MCParticleLocation::Default )
              mcToClusterLink.link( newChan, part );
          }
        } // mchits in the combined cluster
      }
    }
  }

  if ( msgLevel( MSG::VERBOSE ) ) {
    for ( const auto clus : clusterCont ) { verbose() << clus->channelID() << endmsg; }
  }
  // Extra checks
  static_assert( std::is_move_constructible<LHCb::FTClusters>::value,
                 "FTClusters must be move constructible for this to work." );

  return std::make_tuple( std::move( clusterCont ), std::move( mcToClusterLink.links() ),
                          std::move( mcToClusterLinkExtended.links() ), std::move( hitToClusterLink.links() ),
                          std::move( hitToClusterLinkExtended.links() ) );
}

//----------------------------------------------
// Main clustering, check for neighbouring digits over threshold1
// Sum of total change should be over either treshold 2 or 3 for single clusters
// No check for fragmented clusters as total charge doesn't represent real total charge
//----------------------------------------------
void FTClusterCreator::findClusterCandidates( const LHCb::MCFTDigits&              digits,
                                              FTClusterCreator::clusterCandidates& candidates ) const {

  // simple lambda: check if two digits are next to each other in same SiPM
  // n=1 next, n=-1 previous
  auto compare = []( auto a, auto b, int n ) {
    return ( a->channelID().uniqueSiPM() == b->channelID().uniqueSiPM() &&
             a->channelID().channel() == b->channelID().channel() + n );
  };

  // Digit Container is sorted wrt channelID
  auto digitIter = digits.begin();

  std::vector<LHCb::MCFTDigit*> digitVec;
  digitVec.reserve( m_clusterMaxWidth );

  // loop over digits
  while ( digitIter != digits.end() ) {
    digitVec.clear();

    // Check if digit is above threshold1
    if ( charge( ( *digitIter ) ) >= m_adcThreshold1 ) {

      // ADC above seed : start clustering
      if ( msgLevel( MSG::VERBOSE ) )
        verbose() << " ---> START NEW CLUSTER WITH SEED @ " << ( *digitIter )->channelID() << endmsg;

      digitVec.push_back( *digitIter );
      auto startClusIter = digitIter; // begin channel of cluster
      auto stopClusIter  = digitIter; // end channel of cluster

      // loop till below threshold1 or end, optionally exit when cluster bigger than a limit
      while ( ( ++digitIter != digits.end() ) && ( charge( *digitIter ) >= m_adcThreshold1 ) &&
              ( ( stopClusIter + 1 - startClusIter ) < m_clusterMaxWidth ) ) {
        // current digit in the same SiPM, and neighbour channel to last channel
        if ( compare( *digitIter, *stopClusIter, +1 ) ) {
          stopClusIter = digitIter;
          digitVec.push_back( *digitIter );
        } else
          break; // end
      }

      bool isInFragBefore( false ), isInFragAfter( false );

      // check if this is a fragment of an already divided cluster
      if ( startClusIter > digits.begin() ) {
        isInFragBefore = ( charge( *( startClusIter - 1 ) ) >= m_adcThreshold1 &&
                           compare( *( startClusIter - 1 ), *startClusIter, -1 ) );
      }
      // check if there are any further fragments
      if ( stopClusIter < digits.end() - 1 ) {
        isInFragAfter = ( charge( *( stopClusIter + 1 ) ) >= m_adcThreshold1 &&
                          compare( *( stopClusIter + 1 ), *stopClusIter, +1 ) );
      }

      if ( msgLevel( MSG::VERBOSE ) )
        verbose() << " ---> Done with cluster finding, now calculating charge / frac.pos" << endmsg;

      // manage the 3 categories of clusters according to the config flags
      // (single (0), edge(1), middle(2)
      bool isLarge = ( isInFragBefore || isInFragAfter );
      bool isEdge  = ( isInFragBefore != isInFragAfter );

      // unsigned int flag= (!isLarge          ? 0:
      // isLarge && isEdge ? 1: 2);

      clusterFlag flag = ( !isLarge ? clusterFlag::small
                                    : isLarge && isEdge && isInFragAfter
                                          ? clusterFlag::largeEdgeFirst
                                          : isLarge && isEdge && isInFragBefore ? clusterFlag::largeEdgeSecond
                                                                                : clusterFlag::largeMiddle );

      candidates.emplace_back( digitVec, flag );
    } // first digit over threshold1
    else
      digitIter++;
  } // end loop over digits
}

//----------------------------------------------
// Make raw clusters, this will be passed on to the encoder
//-----------------------------------------------
void FTClusterCreator::makeClusters( FTClusterCreator::clusterCandidates& candidates,
                                     LHCb::FTClusters&                    clusterCont ) const {

  if ( msgLevel( MSG::VERBOSE ) ) verbose() << " Start making clusters " << endmsg;

  std::vector<LHCb::FTChannelID> ids;
  ids.reserve( 4 );

  for ( const auto& cand : candidates ) {
    ids.clear();
    float totalCharge   = 0.0;
    float totalChargePE = 0.0;
    float wsumPosition  = 0.0;

    // Loop over digits in the cluster
    unsigned int widthClus  = cand.first.size();
    auto         startDigit = cand.first[0];

    unsigned int i = 0;
    for ( auto clusDigit : cand.first ) {

      ids.push_back( ( clusDigit )->channelID() );
      float channelWeight = charge( clusDigit );
      if ( !m_usePEnotADC )
        channelWeight = ( channelWeight >= m_adcThreshold3
                              ? m_adcThreshold3Weight.value()
                              : channelWeight >= m_adcThreshold2
                                    ? m_adcThreshold2Weight.value()
                                    : channelWeight >= m_adcThreshold1 ? m_adcThreshold1Weight.value() : 0. );

      totalCharge += channelWeight;
      totalChargePE += ( clusDigit )->photoElectrons();

      // mean position will be [ (rel. pos. from left) * charge ] / totalCharge
      //(see below for fragmented clusters)
      wsumPosition += i * channelWeight;
      i++;
    } // end of loop over digits in 'cluster'

    // single/small clusters should pass one of the two thresholds
    // no check is done for fragmented clusters or large clusters
    if ( ( cand.second > 0 ) || ( widthClus == 1 && totalCharge > m_adcThreshold3Weight.value() - 0.5 ) ||
         ( widthClus > 1 && totalCharge > m_adcThreshold1Weight.value() + m_adcThreshold2Weight.value() - 0.5 ) ) {
      // compute position: the middle of the cluster only (no weighting), otherwise use weights
      // also add channelID (uint) offset
      float clusPosition = ( cand.second == 0 ? ( startDigit )->channelID() + wsumPosition / totalCharge
                                              : ( startDigit )->channelID() + float( widthClus - 1 ) / 2 );

      // The fractional position is defined in (-0.250, 0.750)
      unsigned int clusChanPosition     = std::floor( clusPosition - m_lowestFraction );
      float        fractionChanPosition = ( clusPosition - clusChanPosition );
      int          frac                 = int( 2 * ( fractionChanPosition - m_lowestFraction ) );

      unsigned int flag = cand.second;
      // second Edge is treated differently
      // large flag is 1 and fraction is always 0 to distinguish it from first edge
      // instead of center channel, we send last channel number
      if ( cand.second == clusterFlag::largeEdgeSecond ) {
        flag                 = 1;
        frac                 = 0;
        clusChanPosition     = ( startDigit )->channelID() + widthClus - 1;
        fractionChanPosition = 0;
      }

      // save single clusters and edge clusters, or all if keepBigCluster is on
      LHCb::FTCluster* newCluster = new LHCb::FTCluster( clusChanPosition, fractionChanPosition, frac, ids, flag,
                                                         m_storePECharge ? totalChargePE : totalCharge );

      clusterCont.insert( newCluster );

    } // total charge check
  }   // end candidate loop
}
//-------------------------------------------------------------------
