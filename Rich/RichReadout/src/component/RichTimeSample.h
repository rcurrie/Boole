/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

#include <ostream>
#include <vector>

#include "RichPixel.h"

namespace Rich::MC::Digi {

  //===============================================================================
  /** @class RichTimeSample MCRichDigitsToRawBufferAlg.h
   *
   *
   *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
   *  @date   2003-11-06
   */
  //===============================================================================
  class RichTimeSample {

  public:
    RichTimeSample( const int isize = 25, const double value = 50 ) : TimeFrame( isize, value ) {}

    inline void insert( const unsigned int i, const double value ) { TimeFrame[i] = value; }

    inline unsigned int size() const { return TimeFrame.size(); }

    inline double& operator[]( const unsigned int i ) { return TimeFrame[i]; }

    inline double operator[]( const unsigned int i ) const { return TimeFrame[i]; }

    inline RichTimeSample& operator*=( const double c ) {
      for ( auto& i : TimeFrame ) { i *= c; }
      return *this;
    }

    /// Operator overloading for string output
    friend std::ostream& operator<<( std::ostream& os, const RichTimeSample& obj ) {
      for ( unsigned int i = 0; i < obj.size(); ++i ) { os << i << "=" << obj[i] << " "; }
      return os;
    }

  private:
    std::vector<double> TimeFrame;
  };

} // namespace Rich::MC::Digi
