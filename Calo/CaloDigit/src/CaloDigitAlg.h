/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CALODIGIT_CALODIGITALG_H
#define CALODIGIT_CALODIGITALG_H 1
// ============================================================================
// from Gaudi
#include "GaudiAlg/GaudiHistoAlg.h"
#include "GaudiKernel/IRndmGenSvc.h"
#include "GaudiKernel/RndmGenerators.h"

// CaloDet
#include "CaloDet/DeCalorimeter.h"

// FPE guard to protect Rndm::Poisson
#include "Kernel/FPEGuard.h"

/** @class CaloDigitAlg CaloDigitAlg.h
 *
 *  a (sub)Algorithm responsible
 *  for digitisation of MC-information
 *
 *  @author: Olivier Callot
 *   @date:   5 June 2000
 */

class CaloDigitAlg : public GaudiHistoAlg {

public:
  CaloDigitAlg( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~CaloDigitAlg(); ///< Destructor

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution
  StatusCode finalize() override;

protected:
  IRndmGenSvc* rndmSvc() const { return m_rndmSvc; }

private:
  std::string m_detectorName;  ///< Detector element location
  std::string m_inputData;     ///< Input container
  std::string m_outputData;    ///< Output container
  std::string m_inputPrevData; ///< Input data (prev.BX)

  DeCalorimeter*       m_calo    = nullptr; ///< Detector element pointer
  mutable IRndmGenSvc* m_rndmSvc = nullptr; ///< random number service

  double              m_coherentNoise{0};    ///< magnitude of coherent noise
  double              m_incoherentNoise{0};  ///< magnitude of incoherent noise
  double              m_gainError{0.01};     ///< error in PMT gain
  double              m_fracPrev{0};         ///< fraction of previous BX subtracted
  double              m_pedestalShift{0};    ///< pedestal shift due to noise
  double              m_deadCellFraction{0}; ///< Fraction of dead cells
  double              m_zSupThreshold{0};    ///< Zero suppression for data, PreShower only
  double              m_mip{0};              // MIP deposit (Prs/Spd)
  double              m_sat{0}; // fraction of full dynamics to consider (taking ped substraction into account)
  std::vector<double> m_phe;    // number of photoelectron per MIP (for each area)
  int                 m_numberOfCells{0}; ///< Number of cells of this detector.
  int                 m_saturatedAdc{0};  ///< Value to set in case ADC is saturated.
  int                 m_maxAdcValue{0};   ///< Maximum ADC content, after ped. substr
  double              m_activeToTotal{0}; ///< Ratio activeE to total energy in ADC.
  bool                m_zSup{true};

  enum { GAUS_INTEGRAL = 0, GAUS_MEAN = 1, GAUS_SIGMA = 2 };
  bool   m_monitorNoise{false};          ///< create noise monitoring histogram for each section
  bool   m_useAdvancedNoise{false};      ///< use advanced noise spectra generation
  double m_noiseIntegral[3] = {0, 0, 0}; ///< integrals of noise spectra
  std::map<int, std::vector<std::vector<double>>> m_advancedNoise;       /// advanced noise spectra for different areas
  AIDA::IHistogram1D*                             m_noiseHist[3] = {{}}; ///< monitoring histograms from advanced noise

private:
  /// protected poisson distribution against FPE
  inline double safe_poisson( double num ) {
    // turn off the inexact FPE
    FPE::Guard    underFPE( FPE::Guard::mask( "AllExcept" ), true );
    Rndm::Numbers nPe( this->rndmSvc(), Rndm::Poisson( num ) );
    return nPe();
  }
};

#endif //    CALODIGIT_CALODIGITALG_H
