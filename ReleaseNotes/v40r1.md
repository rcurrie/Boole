

2018-08-07 Boole v40r1
===

This version uses Lbcom v30r1, LHCb v50r1, Gaudi v30r3 and LCG_93 with ROOT 6.12.06.

This version is a production release for all simulations  

This version is released on `master` branch. The previous release on `master` branch  was Boole `v40r0`.  

### New features

- Updated VeloSim and VeloRadDamage to use new 2nd metal model (optionally) if requested by SIMCOND, lhcb/Lbcom!239, !152 (@hcroft)   
  New model makes the cluster size closer to the data in the MC. For details see the presentations in the T&A/Simulation meeting: https://indico.cern.ch/event/693499/


- MCFTG4AttenuationTool updated to allow different attenuation maps, !157, !158 (@sesen) [LHCBSCIFI-122]  
  We have attenuation maps at different irradiation levels: 0, 1, 10, 25, 50, 100/fb with power-law model and 50, 100/fb also with linear model. These can be selected with  
  `MCFTDepositCreator().addTool(MCFTG4AttenuationTool("MCFTG4AttenuationTool"))`  
  `MCFTDepositCreator().MCFTG4AttenuationTool.Irradiation = 10 (default:50)`  
  `MCFTDepositCreator().MCFTG4AttenuationTool.IrradiationLinearModel = true (default:false)`  

- Reproducible MDF content, lhcb/LHCb!1217 (@rmatev)   
  Ensures strict reproducibility of MDFs. It has no observable effect except for helping the testing in Moore.    


### Enhancements

- Add support for LYAMs with aged fibres and replaced modules., !162, !164 (@mbieker) [LHCBSCIFI-142]  

- New and faster FT decoding versions, lhcb/LHCb!1326, !153 (@jvantilb) [LHCBSCIFI-101]  
  Requires lhcb-conddb/SIMCOND!10.  
  SIMCOND tag in tests updated accordingly, !159 (@cattanem)  

- RichDAQ - Clean up a number of HPD/PMT specific definitions, lhcb/LHCb!1329, !155 (@jonrob)   

- Implementation of Channel to Channel cross talk in FT, !154, !156 (@pluca)   
  
- Update name of Attenuation maps, !147, !149 (@mbieker) [LHCBSCIFI-129]  
  Paths in the MCFTG4AttenuationTool now match the paths in the new branch of the SIMCOND.  

- Add a constant function to access UT Sectors, lhcb/LHCb!1163, !145 (@mhadji)   

### Bug fixes

- Kernel/PartProp: modify LHCb::ParticleID to solve the problems introduced by lhcb/LHCb!1187, lhcb/LHCb!1362 (@ibelyaev) [LHCBPS-1796]  
  All quantities are now calculated on flight.


### Code modernisations and cleanups
- Fully qualify enums, !151 (@graven)   
