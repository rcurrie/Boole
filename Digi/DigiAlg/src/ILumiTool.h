/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef DIGIALG_ILUMITOOL_H
#define DIGIALG_ILUMITOOL_H 1

// Include files
#include "GaudiKernel/IAlgTool.h"

/** @class ILumiTool ILumiTool.h DigiAlg/ILumiTool.h
 *  Interface class for Luminosity tool.
 *
 *  @author Marco Cattaneo
 *  @date   2003-09-29
 */

// Declaration of the interface ID.
static const InterfaceID IID_ILumiTool( "ILumiTool", 1, 0 );

class ILumiTool : virtual public IAlgTool {
public:
  /// Retrieve interface ID
  static const InterfaceID& interfaceID() { return IID_ILumiTool; }

  /// Return number of interactions
  virtual StatusCode numInteractions( int& number ) = 0;
};
#endif // DIGIALG_ILUMITOOL_H
