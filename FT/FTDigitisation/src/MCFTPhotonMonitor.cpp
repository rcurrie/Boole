/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// local
#include "MCFTPhotonMonitor.h"
#include "IFTSiPMTool.h"
#include "IMCFTAttenuationTool.h"

// Gaudi
#include "GaudiKernel/SystemOfUnits.h"

#include "Event/MCFTDeposit.h"

// external libs
#include <boost/range/irange.hpp>

//-----------------------------------------------------------------------------
// Implementation file for class : MCFTPhotonMonitor
//
// 2016-12-18 : Violaine Bellee, Julian Wishahi
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( MCFTPhotonMonitor )

MCFTPhotonMonitor::MCFTPhotonMonitor( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer( name, pSvcLocator, KeyValue{"MCFTPhotonLocation", LHCb::MCFTPhotonLocation::Default} ) {}

//=============================================================================
// Initialization
//=============================================================================
StatusCode MCFTPhotonMonitor::initialize() {
  StatusCode sc = Consumer::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;

  /// Plot the attenuation maps
  IMCFTAttenuationTool* attenuationTool = tool<IMCFTAttenuationTool>( m_attenuationToolName, this );
  if ( attenuationTool == nullptr ) return Error( "Could not find: MCFTAttenuationTool", StatusCode::FAILURE );

  for ( auto x : boost::irange( 12, 3200, 25 ) ) {
    for ( auto y : boost::irange( 1, 2513, 25 ) ) {
      auto [att, attRef] = attenuationTool->attenuation( float( x ), float( y ) );

      // plot attenuation from direct map
      plot2D( float( x ), float( y ), "AttenuationMapDirect",
              "Attenuation coefficient (direct); #it{x} [mm];#it{y} [mm]", 0., 3200., -12., 2513., 128, 101, att );
      // plot attenuation from reflection map
      plot2D( float( x ), float( y ), "AttenuationMapReflected",
              "Attenuation coefficient (reflection); #it{x} [mm];#it{y} [mm]", 0., 3200., -12., 2513.0, 128, 101,
              attRef );
    }
  }

  // Plot the SiPM PDE
  IFTSiPMTool* sipmTool = tool<IFTSiPMTool>( "FTSiPMTool", this );
  for ( auto wl : boost::irange( 350, 700, 10 ) ) {
    plot( float( wl ), "SipmPDE", "SiPM PDE;wavelength [nm]; Efficiency", 350, 700, 35,
          sipmTool->photonDetectionEfficiency( wl * Gaudi::Units::nm ) );
  }

  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
void MCFTPhotonMonitor::operator()( const LHCb::MCFTPhotons& photons ) const {

  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Execute" << endmsg;

  plot( photons.size(), "nPhotons", "nPhotons per event;nPhotons per event;Events", 0., 2.e6, 100 );

  double nPhotonsInMat = 0.0;
  int    prevSensDetID = -1000;
  for ( auto photon : photons ) // Loop over photons
  {
    if ( prevSensDetID == photon->sensDetID() ) {
      nPhotonsInMat += photon->nPhotons();
    } else if ( prevSensDetID != -1000 ) {
      plot( nPhotonsInMat, "nPhotonsPerMat", "nPhotons per mat;nPhotons per mat;Mats #times events", 0., 2500., 100 );
      nPhotonsInMat = photon->nPhotons();
      prevSensDetID = photon->sensDetID();
    } else {
      nPhotonsInMat = photon->nPhotons();
      prevSensDetID = photon->sensDetID();
    }
    std::string dirOrRefl = photon->isReflected() ? "Reflected" : "Direct";

    plot( photon->time(), "ExitTime" + dirOrRefl, "Photon exit time " + dirOrRefl + ";photon exit time [ns];Photons",
          -30., 150., 100 );
    plot( photon->time() - photon->mcHit()->time(), "PropTime" + dirOrRefl,
          "Photon propagation time " + dirOrRefl + ";photon propagation time [ns];Photons", -60., 95., 100 );
    plot( photon->wavelength() / Gaudi::Units::nm, "Wavelength", "wavelength;wavelength [nm];Photons", 0., 900., 100 );
    plot( photon->posX(), "ExitPosX", "x position (local);#it{x} [mm];Photons", -70., +70., 140 );
    plot( photon->posZ(), "ExitPosZ", "z position (local);#it{z} [mm];Photons", -0.7, 0.7, 100 );
    plot2D( photon->posX(), photon->posZ(), "ExitPosMapLeft", "Photon exit positions; #it{x} [mm];#it{z} [mm]", -65.7,
            -64.3, -0.7, 0.7, 100, 100 );
    plot2D( photon->posX(), photon->posZ(), "ExitPosMapMiddle", "Photon exit positions; #it{x} [mm];#it{z} [mm]", -0.7,
            0.7, -0.7, 0.7, 100, 100 );
    plot2D( photon->posX(), photon->posZ(), "ExitPosMapRight", "Photon exit positions; #it{x} [mm];#it{z} [mm]", 64.3,
            65.7, -0.7, 0.7, 100, 100 );
    plot( photon->dXdY(), "dXdY", "dx/dy (local);d#it{x}/d#it{y};Photons", -3., 3., 100 );
    plot( photon->dZdY(), "dZdY", "dz/dy (local);d#it{z}/d#it{y};Photons", -3., 3., 100 );
  }

  plot( nPhotonsInMat, "nPhotonsPerMat", "nPhotons per mat;nPhotons per mat;Mats", 0., 2500., 100 );

  return;
}
