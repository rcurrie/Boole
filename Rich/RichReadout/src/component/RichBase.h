/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// Include files
#include <cmath>
#include <map>
#include <memory>
#include <vector>

#include "RichPixelProperties.h"

// Utils
#include "RichUtils/RichHashMap.h"
#include "RichUtils/RichMap.h"

// Kernel
#include "Kernel/RichSmartID.h"

// gaudi
#include "GaudiKernel/ObjectList.h"
#include "GaudiKernel/ObjectVector.h"

namespace Rich::MC::Digi {

  class RichPixel;
  class RichRegistry;

  class RichBase {

  public:
    RichBase( const LHCb::RichSmartID::Vector& pixelList ) { upDate( pixelList ); }

    enum class BaseState {
      OPEN,      /*!< the base can be initalized */
      CLOSED,    /*!< the base cannot be initalized but channels can be setup */
      UNSETTABLE /*!< the base is CLOSED and channels cannot be setup */
    };

  public:
    bool IsInOpenState() const { return ( m_MyState == BaseState::OPEN ? true : false ); }

    bool IsInSettableState() const { return ( m_MyState != BaseState::UNSETTABLE ? true : false ); }

    void CloseState() {
      if ( m_MyState == BaseState::OPEN ) { m_MyState = BaseState::CLOSED; }
    }

    void OpenState() {
      if ( m_MyState == BaseState::CLOSED ) { m_MyState = BaseState::OPEN; }
    }

    void UnsettableState() { m_MyState = BaseState::UNSETTABLE; }

    void clear() { m_activePixels.clear(); }

    void upDate( const LHCb::RichSmartID::Vector& pixelList );

    unsigned int size() const { return m_activePixels.size(); }

    decltype( auto ) DecodeUniqueID( const LHCb::RichSmartID id ) const {
      const auto it = m_activePixels.find( id );
      return ( it != m_activePixels.end() ? ( *it ).second : nullptr );
    }

    using activemap = Rich::HashMap<LHCb::RichSmartID::KeyType, std::shared_ptr<RichPixelProperties>>;

    activemap::const_iterator begin() const { return m_activePixels.begin(); }

    activemap::const_iterator end() const { return m_activePixels.end(); }

  private: // data
    BaseState m_MyState{BaseState::OPEN};

    activemap m_activePixels;

    // crj : temporary single properties object whilst all pixels are treated the same
    RichPixelProperties m_pixProps;

    friend class RichRegistry;
  };

} // namespace Rich::MC::Digi
