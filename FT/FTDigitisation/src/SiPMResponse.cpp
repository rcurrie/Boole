/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// from Gaudi
#include "GaudiKernel/SystemOfUnits.h"

// local
#include "SiPMResponse.h"

//-----------------------------------------------------------------------------
// Implementation file for class : SiPMResponse
//
//   This class describes the SiPM response to a single pulse
//
// 2013-11-12 : Maurizio Martinelli
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_COMPONENT( SiPMResponse )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
SiPMResponse::SiPMResponse( const std::string& type, const std::string& name, const IInterface* parent )
    : GaudiTool( type, name, parent ), m_responseSpline( 0 ) {
  declareInterface<SiPMResponse>( this );
}

//=============================================================================
// Destructor
//=============================================================================
SiPMResponse::~SiPMResponse() {
  if ( 0 != m_responseSpline ) delete m_responseSpline;
}
//=============================================================================
// Initialize the pulse shape
//=============================================================================
StatusCode SiPMResponse::initialize() {
  StatusCode sc = GaudiTool::initialize();
  if ( sc.isFailure() ) return Error( "Failed to initialize", sc );

  std::vector<double> times;  ///< Times for user-defined function
  std::vector<double> values; ///< Values for user-defined function

  if ( m_electronicsResponse == "pacific2" ) {

    // output of tools/getSiPMResponseShape.py here:
    values = {-0.022, -0.023, -0.017, -0.008, -0.010, -0.024, -0.030, -0.030, -0.031, -0.015, -0.013,
              -0.024, -0.019, -0.032, -0.003, -0.018, -0.016, -0.031, -0.039, -0.053, 0.016,  0.033,
              0.165,  0.330,  0.540,  0.690,  0.810,  0.901,  0.930,  0.962,  0.965,  0.920,  0.883,
              0.737,  0.604,  0.444,  0.335,  0.251,  0.213,  0.165,  0.119,  0.092,  0.069,  0.031,
              0.036,  -0.000, -0.029, -0.016, -0.021, -0.008, -0.006, 0.011,  0.008,  0.025};

    times = {-45.20, -43.06, -40.92, -38.78, -36.64, -34.50, -32.36, -30.22, -28.08, -25.94, -23.80,
             -21.66, -19.52, -17.38, -15.24, -13.10, -10.96, -8.82,  -6.68,  -4.54,  -2.40,  -0.26,
             1.88,   4.02,   6.16,   8.30,   10.44,  12.58,  14.72,  16.86,  19.00,  21.14,  23.28,
             25.42,  27.56,  29.70,  31.84,  33.98,  36.12,  38.26,  40.40,  42.54,  44.68,  46.82,
             48.96,  51.10,  53.24,  55.38,  57.52,  59.66,  61.80,  63.94,  66.08,  68.22};

  } else if ( m_electronicsResponse == "flat" ) {

    // Use f(t) = 1
    info() << "WARNING: Using flat SiPMResponse!" << endmsg;

    values.assign( 54, 1.0 );
    times = {-45.20, -43.06, -40.92, -38.78, -36.64, -34.50, -32.36, -30.22, -28.08, -25.94, -23.80,
             -21.66, -19.52, -17.38, -15.24, -13.10, -10.96, -8.82,  -6.68,  -4.54,  -2.40,  -0.26,
             1.88,   4.02,   6.16,   8.30,   10.44,  12.58,  14.72,  16.86,  19.00,  21.14,  23.28,
             25.42,  27.56,  29.70,  31.84,  33.98,  36.12,  38.26,  40.40,  42.54,  44.68,  46.82,
             48.96,  51.10,  53.24,  55.38,  57.52,  59.66,  61.80,  63.94,  66.08,  68.22};

  } else if ( m_electronicsResponse == "pacific3" ) {

    // Result from tools/getSiPMResponseShapeNew.py with phe=3
    values = {0.010,  0.010,  0.010,  0.010,  0.010,  0.010,  0.010,  0.010,  0.010,  0.010,  0.010,  0.010,  0.010,
              0.010,  0.010,  0.010,  0.010,  0.010,  0.010,  0.010,  0.010,  0.010,  0.011,  0.011,  0.011,  0.005,
              0.005,  0.005,  0.005,  0.005,  0.005,  0.006,  0.007,  0.012,  0.022,  0.040,  0.066,  0.103,  0.150,
              0.206,  0.270,  0.319,  0.394,  0.473,  0.553,  0.632,  0.707,  0.777,  0.841,  0.899,  0.915,  0.963,
              1.000,  0.934,  0.810,  0.675,  0.553,  0.449,  0.362,  0.289,  0.230,  0.180,  0.138,  0.103,  0.074,
              0.049,  0.039,  0.021,  0.006,  -0.007, -0.018, -0.027, -0.035, -0.041, -0.046, -0.050, -0.053, -0.056,
              -0.059, -0.060, -0.061, -0.062, -0.062, -0.062, -0.061, -0.061, -0.060, -0.058, -0.057, -0.056, -0.054};

    times.assign( values.size(), -40.0 );
    int i = 0;
    for ( auto& time : times ) time += i++;

  } else if ( m_electronicsResponse == "pacific4" ) {

    if ( m_useDefaultCalibration ) {
      m_sipmGainShift = 1.0;
      m_tshift        = 5.;
    }

    // Result from tools/getSiPMResponseShapeNew.py with phe=3
    values = {0.000,  0.000,  0.000,  0.000,  0.000,  0.000,  0.000, 0.000,  0.000,  0.000, 0.000, 0.000, 0.000,
              0.000,  0.000,  0.000,  0.000,  0.000,  0.000,  0.000, 0.000,  0.000,  0.000, 0.000, 0.000, -0.000,
              -0.000, -0.000, -0.000, -0.000, -0.000, -0.000, 0.000, -0.000, -0.002, 0.006, 0.058, 0.169, 0.324,
              0.498,  0.662,  0.799,  0.896,  0.953,  0.985,  0.998, 1.000,  0.997,  0.989, 0.983, 0.976, 0.970,
              0.964,  0.959,  0.956,  0.950,  0.947,  0.945,  0.944, 0.937,  0.851,  0.690, 0.508, 0.345, 0.222,
              0.142,  0.089,  0.059,  0.044,  0.037,  0.035,  0.037, 0.042,  0.047,  0.053, 0.057, 0.062, 0.066,
              0.068,  0.071,  0.072,  0.072,  0.072,  0.073,  0.071, 0.062,  0.048,  0.027, 0.012, 0.001, -0.006};

    times.assign( values.size(), -40.0 );
    int i = 0;
    for ( auto& time : times ) time += i++;

  } else if ( m_electronicsResponse == "pacific5q_pz5" ) {
    // Result from tools/getSiPMResponseShapeNew.py with phe=3

    if ( m_useDefaultCalibration ) {
      m_sipmGainShift = 1.5;
      m_tshift        = 7.;
    }

    values = {-0.1492051523, -0.187223457,  -0.2270392182,
              -0.2659463597, -0.295314305,  -0.307569858,
              -0.2936743333, -0.2444440531, -0.1536784783,
              -0.029097907,  0.1116820544,  0.282515235,
              0.4284964748,  0.549722497,   0.6412516785,
              0.6938206774,  0.7042247089,  0.7079459784,
              0.7167430096,  0.7272901904,  0.739825284,
              0.7558809513,  0.7813353277,  0.8234445919,
              0.882836635,   0.9550228332,  1,
              0.9732486779,  0.8884176177,  0.7771665884,
              0.6619448533,  0.5471486753,  0.4340168392,
              0.3205317375,  0.2141107198,  0.1231672425,
              0.0574887298,  0.0209516833,  0.0061333304,
              0.0009797596,  -0.0005831898};

    times = {-15.0, -14.0, -13.0, -12.0, -11.0, -10.0, -9.0, -8.0, -7.0, -6.0, -5.0, -4.0, -3.0, -2.0,
             -1.0,  0.0,   1.0,   2.0,   3.0,   4.0,   5.0,  6.0,  7.0,  8.0,  9.0,  10.0, 11.0, 12.0,
             13.0,  14.0,  15.0,  16.0,  17.0,  18.0,  19.0, 20.0, 21.0, 22.0, 23.0, 24.0, 25.0};

  } else if ( m_electronicsResponse == "pacific5q_pz6" ) {
    // Result from tools/getSiPMResponseShapeNew.py with phe=3

    if ( m_useDefaultCalibration ) {
      m_sipmGainShift = 1.3;
      m_tshift        = 8.;
    }

    values = {-0.04502588,   -0.0663589799, -0.0905883317, -0.1132151155, -0.1252598574,
              -0.1170492639, -0.0808864133, -0.012558064,  0.0888085528,  0.2209638607,
              0.3666501654,  0.53030451,    0.6592162163,  0.758905349,   0.8212842322,
              0.8494948406,  0.8487543419,  0.8489081291,  0.8529853625,  0.8541187485,
              0.8597486345,  0.8738996821,  0.904812793,   0.9584231829,  1,
              0.9813827608,  0.9118624985,  0.8205336306,  0.7246824443,  0.6304849388,
              0.5377341935,  0.4461274982,  0.3554014315,  0.2666775927,  0.1779749323,
              0.1010260529,  0.0465605909,  0.0162691434,  0.0044796378,  0.000992842,
              -5.6451488E-05};

    times = {-15.0, -14.0, -13.0, -12.0, -11.0, -10.0, -9.0, -8.0, -7.0, -6.0, -5.0, -4.0, -3.0, -2.0,
             -1.0,  0.0,   1.0,   2.0,   3.0,   4.0,   5.0,  6.0,  7.0,  8.0,  9.0,  10.0, 11.0, 12.0,
             13.0,  14.0,  15.0,  16.0,  17.0,  18.0,  19.0, 20.0, 21.0, 22.0, 23.0, 24.0, 25.0};

  } else {

    // use old SiPMResponse
    info() << "WARNING: Using old SiPMResponse!" << endmsg;

    std::vector<double> Tprev = {
        3.22188688e-05, 3.22145863e-05, 3.22145863e-05, 3.22145863e-05, 3.22145863e-05, 3.22145863e-05, 3.22145863e-05,
        3.22145863e-05, 3.22145863e-05, 3.22145863e-05, 3.22145863e-05, 3.22145863e-05, 3.22145863e-05, 3.22145863e-05,
        3.22145863e-05, 3.22145863e-05, 3.22145863e-05, 3.22145863e-05, 3.22145863e-05, 3.22145863e-05, 3.22145863e-05,
        3.22145863e-05, 3.22145863e-05, 3.22145863e-05, 3.22145863e-05, 3.22145863e-05};
    // current spill
    std::vector<double> Tcurr = {2.88048286e-02, 2.76166647e-02, 2.84853231e-02, 2.86509678e-02,  2.87964850e-02,
                                 2.90245415e-02, 2.92078012e-02, 2.93951976e-02, 2.95711670e-02,  2.96709382e-02,
                                 2.97279647e-02, 2.96909134e-02, 2.94095934e-02, 2.89146629e-02,  2.80610778e-02,
                                 2.67112946e-02, 2.47312816e-02, 2.20768793e-02, 1.86479251e-02,  1.44897527e-02,
                                 9.82891757e-03, 5.23039367e-03, 1.56929340e-03, -2.17283810e-04, -1.82921036e-05,
                                 6.66654783e-05};
    // next spill
    std::vector<double> Tnext = {-0.00136548, -0.00089388, -0.0012617,  -0.00137867, -0.00145512, -0.00160822,
                                 -0.00173438, -0.00187304, -0.00201678, -0.00213902, -0.0022567,  -0.00234672,
                                 -0.0023399,  -0.00226133, -0.00202669, -0.00154593, -0.00067318, 0.00060853,
                                 0.00250173,  0.00515046,  0.00876429,  0.01327495,  0.01883291,  0.02372474,
                                 0.02788753,  0.0281931};
    // nextnext spill
    std::vector<double> Tnext2 = {-0.00034101, -0.00025578, -0.00030324, -0.00031672, -0.00032477, -0.00034131,
                                  -0.00035811, -0.00037248, -0.00038796, -0.00040745, -0.00042273, -0.00044454,
                                  -0.00046555, -0.00048982, -0.00051403, -0.00054482, -0.00056937, -0.00060258,
                                  -0.00064924, -0.00068913, -0.00070694, -0.00073142, -0.00099868, -0.00083826,
                                  -0.00119224, -0.00104176};
    // reverse the vectors
    std::reverse( Tprev.begin(), Tprev.end() );
    std::reverse( Tcurr.begin(), Tcurr.end() );
    std::reverse( Tnext.begin(), Tnext.end() );
    std::reverse( Tnext2.begin(), Tnext2.end() );
    // set times
    for ( int i = -25; i < 76; ++i ) times.push_back( i );
    // set values
    for ( int i = 0; i < 26; ++i ) values.push_back( Tprev[i] - Tprev[0] );
    for ( int i = 1; i < 26; ++i ) values.push_back( Tcurr[i] - Tcurr[0] + values[25] );
    for ( int i = 1; i < 26; ++i ) values.push_back( Tnext[i] - Tnext[0] + values[50] );
    for ( int i = 1; i < 26; ++i ) values.push_back( Tnext2[i] - Tnext2[0] + values[75] );
  }

  // shift times by tshift
  std::for_each( times.begin(), times.end(), [this]( double& n ) { n += m_tshift; } );

  // Check if the data is provided and the times and values are of equal length
  if ( times.size() == 0 ) return Error( "No data !", StatusCode::FAILURE );
  if ( times.size() != values.size() ) { return Error( "inconsistent data !", StatusCode::FAILURE ); }

  // Store the first and last entry of the vector of times
  m_tMin = times.front();
  m_tMax = times.back();

  // Normalize function. Set maximum to 1.
  auto maxVal = *max_element( std::begin( values ), std::end( values ) );
  std::transform( values.begin(), values.end(), values.begin(),
                  std::bind1st( std::multiplies<double>(), 1. / maxVal ) );

  if ( msgLevel( MSG::DEBUG ) ) {
    debug() << "SiPMResponse max = " << maxVal << endmsg;
    debug() << "SiPMResponse after reweighting:" << endmsg;
    debug() << "----------------------------" << endmsg;
    for ( unsigned int i = 0; i < values.size(); ++i )
      debug() << "\t t = " << times[i] << "\t val = " << values[i] << endmsg;
  }

  // Fit the spline to the data
  m_responseSpline = new GaudiMath::SimpleSpline( times, values, typeFromString() );

  return sc;
}

double SiPMResponse::response( const double time ) const {
  return ( ( time > m_tMin ) && ( time < m_tMax ) ? m_responseSpline->eval( time ) * m_sipmGainShift : 0.0 );
}

GaudiMath::Interpolation::Type SiPMResponse::typeFromString() const {
  GaudiMath::Interpolation::Type aType;
  using namespace GaudiMath::Interpolation;
  if ( m_splineType == "Cspline" )
    aType = Cspline;
  else if ( m_splineType == "Linear" )
    aType = Linear;
  else if ( m_splineType == "Polynomial" )
    aType = Polynomial;
  else if ( m_splineType == "Akima" )
    aType = Akima;
  else if ( m_splineType == "Akima_Periodic" )
    aType = Akima_Periodic;
  else if ( m_splineType == "Cspline_Periodic" )
    aType = Cspline_Periodic;
  else
    aType = Cspline; // default to cubic

  return aType;
}

void SiPMResponse::sample( std::vector<double>& times, std::vector<double>& values ) const {
  double t = m_tMin;
  while ( t < m_tMax ) {
    times.push_back( t );
    values.push_back( response( t ) );
    t += m_samplingDt;
  } // loop times
}

//=============================================================================
