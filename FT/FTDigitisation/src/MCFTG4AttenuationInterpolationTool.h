/*****************************************************************************\
* (c) Copyright 2018 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MCFTG4ATTENUATIONINTERPOLATIONTOOL_H
#define MCFTG4ATTENUATIONINTERPOLATIONTOOL_H 1

// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "GaudiKernel/TaggedBool.h"

#include "IMCFTAttenuationTool.h" // Base Class

#include "Kernel/STLExtensions.h"

/** @class MCFTG4AttenuationInterpolationTool MCFTG4AttenuationInterpolationTool.h
 *
 *  Tool that reads LYAM parameters from the CondDB and creates a interpolated attenuation map
 *  for a given irradiation dose and (time) age of the fibres.
 *
 *  @author Martin Bieker based on implementation of M. Demmer and J. Wishahi
 *  @date   2018-28-08
 */

class MCFTG4AttenuationInterpolationTool : public extends<GaudiTool, IMCFTAttenuationTool> {

public:
  using extends::extends;

  /// Initialize the LYAM
  StatusCode initialize() override;

  /// Calculate the direct attenuation and the attenuation with reflection
  Attenuation attenuation( double x, double y ) const override;

protected:
  using ReflectedPhotons = Gaudi::tagged_bool<struct IsReflection_tag>;
  double              calculateAttenuation( int binID, MCFTG4AttenuationInterpolationTool::ReflectedPhotons isRefl );
  unsigned int        m_nBinsX, m_nBinsY;
  std::vector<double> m_xEdges, m_yEdges, m_effDir, m_effRef;
  enum class ParameterName { mask, a_refl, a_dir, b_refl, b_dir, c_refl, c_dir, d_refl, d_dir, y_pos };
  ParameterName                                convert( std::string const& key ) const;
  std::map<ParameterName, std::vector<double>> m_parameters;

private:
  void                         processParameterMap( const std::string& conditionsLocation );
  bool                         validateMap();
  void                         fillMaps();
  int                          findBin( LHCb::span<const double> axis, double position ) const;
  Gaudi::Property<std::string> m_parameterLocation{this, "ParameterLocation",
                                                   "/dd/Conditions/Calibration/FT/AttenuationInterpolation",
                                                   "Location of ParameterFile in CondDB"};
  Gaudi::Property<float>       m_fibreAge{this, "FibreAge", 0, "Age of fibres in months"};
  Gaudi::Property<float>       m_irrad{this, "IrradiationLevel", 0, "Irradiation level in 1/fb"};
  Gaudi::Property<float>       m_mirrorReflectivity{this, "MirrorReflectivity", 0.75, "Reflectivity of the Mirror"};
};
#endif // MCFG4TATTENUATIONTOOL_H
