/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/** @file MCFTMCHitInjector.cpp
 *
 *  Implementation of class : MCFTMCHitInjector
 *
 *  @author Belle, Violaine and Wishahi, Julian
 *  @date   2016-11-26
 */

// local
#include "MCFTMCHitInjector.h"

#include <range/v3/all.hpp>

#if RANGE_V3_VERSION < 900
namespace ranges::views {
  using namespace ranges::view;
}
#endif

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( MCFTMCHitInjector )

MCFTMCHitInjector::MCFTMCHitInjector( const std::string& name, ISvcLocator* pSvcLocator )
    : Producer( name, pSvcLocator,
                // Output
                KeyValue{"OutputLocation", "/Event/MC/FT/InjectorHits"} ) {}
//=============================================================================
// Initialization
//=============================================================================
StatusCode MCFTMCHitInjector::initialize() {

  StatusCode sc = Producer::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;        // error printed already by GaudiAlgorithm

  // Retrieve and initialize DeFT (no test: exception in case of failure)
  m_deFT = getDet<DeFTDetector>( DeFTDetectorLocation::Default );
  if ( m_deFT->version() < 61 ) return Error( "This version requires FTDet v6.1 or higher", StatusCode::FAILURE );

  auto targetProps =
      ranges::views::zip( m_targetFTStations, m_targetFTLayers, m_targetFTQuarters, m_targetFTModules, m_targetFTMats );

  for ( const auto&& targetProp : targetProps ) {
    m_targetFTChannelIDs.emplace_back( std::get<0>( targetProp ), std::get<1>( targetProp ), std::get<2>( targetProp ),
                                       std::get<3>( targetProp ), std::get<4>( targetProp ), 0u, 0u );
  }

  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
std::vector<LHCb::MCHit> MCFTMCHitInjector::operator()() const {

  // define photons container
  std::vector<LHCb::MCHit> mchitsCont;

  auto mchitProps = ranges::views::zip( m_propMCHitXs, m_propMCHitYs, m_propMCHitDeltaXs, m_propMCHitDeltaYs,
                                        m_propMCHitEnergies, m_propMCHitMomenta, m_propMCHitTimes );
  mchitsCont.reserve( mchitProps.size() );
  for ( auto targetFTChannelID : m_targetFTChannelIDs ) {
    // info() << "MCHits in: " << targetFTChannelID << endmsg;
    const DeFTMat* mat = m_deFT->findMat( targetFTChannelID );

    auto mat_geom      = mat->geometry();
    auto mat_thickness = mat->fibreMatThickness();

    for ( const auto&& mchitProp : mchitProps ) {
      Gaudi::XYZPoint enPointLoc( std::get<0>( mchitProp ), std::get<1>( mchitProp ), -mat_thickness / 2. );

      Gaudi::XYZVector displVecLoc( std::get<2>( mchitProp ), std::get<3>( mchitProp ), mat_thickness );

      auto enPointGlob  = mat_geom->toGlobal( enPointLoc );
      auto displVecGlob = mat_geom->toGlobal( displVecLoc );

      LHCb::MCHit mc_hit{};
      mc_hit.setEntry( enPointGlob );
      mc_hit.setDisplacement( displVecGlob );
      mc_hit.setEnergy( std::get<4>( mchitProp ) );
      mc_hit.setP( std::get<5>( mchitProp ) );
      mc_hit.setTime( std::get<6>( mchitProp ) );
      mc_hit.setSensDetID( m_deFT->sensitiveVolumeID( mc_hit.midPoint() ) );

      if ( msgLevel( MSG::DEBUG ) ) {
        debug() << " MCHit : "
                << "entry  " << mc_hit.entry() << ", exit   " << mc_hit.exit() << ", E " << mc_hit.energy() << ", p "
                << mc_hit.p() << ", t " << mc_hit.time() << ", sensDetID " << mc_hit.sensDetID() << endmsg;
      }
      mchitsCont.push_back( mc_hit );
    }
  }

  return mchitsCont;
}
