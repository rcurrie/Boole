/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "GaudiKernel/MsgStream.h"
#include "Kernel/MuonTileID.h"
#include "MuonPhChIDPack.h"

class MuonPhChID final {
public:
  MuonPhChID() = default;
  explicit MuonPhChID( unsigned int ID ) : m_channelID( ID ){};
  MuonPhChID& setStation( unsigned int value ) {
    return set<MuonPhChIDPack::shiftStation, MuonPhChIDPack::maskStation>( value );
  }
  MuonPhChID& setRegion( unsigned int value ) {
    return set<MuonPhChIDPack::shiftRegion, MuonPhChIDPack::maskRegion>( value );
  }
  MuonPhChID& setQuadrant( unsigned int value ) {
    return set<MuonPhChIDPack::shiftQuadrant, MuonPhChIDPack::maskQuadrant>( value );
  }
  MuonPhChID& setChamber( unsigned int value ) {
    return set<MuonPhChIDPack::shiftChamber, MuonPhChIDPack::maskChamber>( value );
  }
  MuonPhChID& setPhChIDX( unsigned int value ) {
    return set<MuonPhChIDPack::shiftPhChIDX, MuonPhChIDPack::maskPhChIDX>( value );
  }
  MuonPhChID& setPhChIDY( unsigned int value ) {
    return set<MuonPhChIDPack::shiftPhChIDY, MuonPhChIDPack::maskPhChIDY>( value );
  }
  MuonPhChID& setFrontEnd( unsigned int value ) {
    return set<MuonPhChIDPack::shiftFrontEnd, MuonPhChIDPack::maskFrontEnd>( value );
  }
  MuonPhChID& setReadout( unsigned int value ) {
    return set<MuonPhChIDPack::shiftReadout, MuonPhChIDPack::maskReadout>( value );
  }
  MuonPhChID& setGap( unsigned int value ) { return set<MuonPhChIDPack::shiftGap, MuonPhChIDPack::maskGap>( value ); }

  [[nodiscard]] unsigned int getID() const { return m_channelID; }
  [[nodiscard]] unsigned int getStation() const {
    return get<MuonPhChIDPack::shiftStation, MuonPhChIDPack::maskStation>();
  }
  [[nodiscard]] unsigned int getRegion() const {
    return get<MuonPhChIDPack::shiftRegion, MuonPhChIDPack::maskRegion>();
  }
  [[nodiscard]] unsigned int getQuadrant() const {
    return get<MuonPhChIDPack::shiftQuadrant, MuonPhChIDPack::maskQuadrant>();
  }
  [[nodiscard]] unsigned int getChamber() const {
    return get<MuonPhChIDPack::shiftChamber, MuonPhChIDPack::maskChamber>();
  }
  [[nodiscard]] unsigned int getPhChIDX() const {
    return get<MuonPhChIDPack::shiftPhChIDX, MuonPhChIDPack::maskPhChIDX>();
  }
  [[nodiscard]] unsigned int getPhChIDY() const {
    return get<MuonPhChIDPack::shiftPhChIDY, MuonPhChIDPack::maskPhChIDY>();
  }
  [[nodiscard]] unsigned int getFrontEnd() const {
    return get<MuonPhChIDPack::shiftFrontEnd, MuonPhChIDPack::maskFrontEnd>();
  }
  [[nodiscard]] unsigned int getReadout() const {
    return get<MuonPhChIDPack::shiftReadout, MuonPhChIDPack::maskReadout>();
  }
  [[nodiscard]] unsigned int getGap() const { return get<MuonPhChIDPack::shiftGap, MuonPhChIDPack::maskGap>(); }
  [[nodiscard]] MuonPhChID   getFETile() const;
  /// printout to std::ostream
  std::ostream& printOut( std::ostream& ) const;
  /// printout to MsgStream
  MsgStream& printOut( MsgStream& ) const;
  //
  // output to std::ostream
  friend std::ostream& operator<<( std::ostream& os, const MuonPhChID& id ) { return id.printOut( os ); }

  friend MsgStream& operator<<( MsgStream& os, const MuonPhChID& id ) { return id.printOut( os ); }

  friend bool operator==( MuonPhChID const& lhs, MuonPhChID const& rhs ) { return lhs.m_channelID == rhs.m_channelID; }

private:
  template <unsigned int shift, unsigned mask>
  MuonPhChID& set( unsigned int value ) {
    MuonPhChIDPack::setBit( m_channelID, shift, mask, value );
    return *this;
  }
  template <unsigned int shift, unsigned mask>
  unsigned int get() const {
    return MuonPhChIDPack::getBit( m_channelID, shift, mask );
  }
  unsigned int m_channelID{0};
};

inline MuonPhChID MuonPhChID::getFETile() const {
  return MuonPhChID{m_channelID -
                    MuonPhChIDPack::getBit( m_channelID, MuonPhChIDPack::shiftGap, MuonPhChIDPack::maskGap )};
}

inline MsgStream& MuonPhChID::printOut( MsgStream& os ) const {
  return os << "[" << getStation() << "," << getRegion() << "," << getQuadrant() << "," << getChamber() << ","
            << getPhChIDX() << "," << getPhChIDY() << "," << getFrontEnd() << "," << getReadout() << "," << getGap()
            << "]";
}

inline std::ostream& MuonPhChID::printOut( std::ostream& os ) const {
  return os << "[" << getStation() << "," << getRegion() << "," << getQuadrant() << "," << getChamber() << ","
            << getPhChIDX() << "," << getPhChIDY() << "," << getFrontEnd() << "," << getReadout() << "," << getGap()
            << "]";
}
