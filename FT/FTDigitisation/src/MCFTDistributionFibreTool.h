/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MCFTDISTRIBUTIONFIBRETOOL_H
#define MCFTDISTRIBUTIONFIBRETOOL_H 1

// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/Point3DTypes.h"
#include "GaudiKernel/SystemOfUnits.h"

// from ROOT
#include "Math/SVector.h"

#include "IMCFTDistributionFibreTool.h" // Interface

/** @class MCFTDistributionFibreTool MCFTDistributionFibreTool.h
 *
 *  This tool calculates the fraction of the total path length
 *  of a track in the core of each fibre that was hit along with
 *  the position of the fibre.
 *
 *  @author Tobias Tekampe, Luca Pescatore
 *  @author based on Julian Wishahi's MCSciFiEnergyInFibreCoreCalc
 *  @date   2015-10-27
 */

typedef ROOT::Math::SVector<double, 2> Vec2;
typedef ROOT::Math::SVector<double, 3> Vec3;

namespace MCFT {

  double distanceToSquared( const Vec2& v, const Vec2& p );
  double distanceTo( const Vec2& v, const Vec2& p );
  double minDistanceFromLineSegmentToPoint( const Vec2& v, const Vec2& w, const Vec2& p );
  double minDistanceFromLineToPoint( const Vec2& v, const Vec2& w, const Vec2& p );

  inline double getXOnPath( const Vec2& entry, const Vec2& exit, double z ) {
    return entry( 0 ) + ( exit( 0 ) - entry( 0 ) ) / ( exit( 1 ) - entry( 1 ) ) * ( z - entry( 1 ) );
  }

  inline double getZOnPath( const Vec2& entry, const Vec2& exit, double x ) {
    return entry( 1 ) + ( exit( 1 ) - entry( 1 ) ) / ( exit( 0 ) - entry( 0 ) ) * ( x - entry( 0 ) );
  }

} // namespace MCFT

class MCFTDistributionFibreTool : public extends<GaudiTool, IMCFTDistributionFibreTool> {
public:
  using base_class::base_class;

  StatusCode initialize() override;

  std::vector<std::pair<Gaudi::XYZPoint, double>>
  effectivePathFracInCores( const Gaudi::XYZPoint& enPoint, const Gaudi::XYZPoint& exPoint ) const override;

private:
  /*!
   * The fibre positions in the local coordinate system can be expressed using
   * two indices: the layer index i and the fibre index j. The indices of the
   * fibres j are defined such that in even-numbered fibre layers (i = 0, 2, 4,
   * ...) the fibre with index j= 0 is at x = 0, while in odd-numbered layers
   * (i= 1, 3, ...) the fibre with index j = 0 is at x = 0.5*fibre_pitch.
   * Then, the fibre position can be expressed as:
   *   x_fib = j * fibre_pitch + (i%2)*fibre_pitch/2. = (j + (i%2)*0.5)*fibre_pitch
   *   z_fib = (i-nLayers/2)*m_deltaZ
   */
  inline double getFibreX( unsigned int iLayer, int iFibre ) const {
    return m_fibrePitch * ( iFibre + 0.5 * ( iLayer % 2 ) );
  }
  inline double getFibreZ( unsigned int iLayer ) const { return ( iLayer - 0.5 * ( m_nlayers - 1 ) ) * m_deltaZ; }

  inline double firstFibre( unsigned int iLayer ) const {
    return ( ( iLayer % 2 ) == 0 ) ? m_firstFibre0 : m_firstFibre1;
  }

  inline double lastFibre( unsigned int iLayer ) const {
    return ( ( iLayer % 2 ) == 0 ) ? -m_firstFibre0 : m_lastFibre1;
  }

  /*!
   * Get the fibre index of the first and last fibre to consider, given the x
   * position at the lower edge of the fibre layer's sensitive region
   */
  inline std::pair<int, int> getAdjacentFibreIds( double x, double dx_over_dz, unsigned int iLayer ) const {
    // transform x into local coordinate system x' of layer, in which the fibre
    // with index 0 is at x' = 0
    std::pair<int, int> fibreRange;
    double              xprime = x - ( iLayer % 2 ) * 0.5 * m_fibrePitch;
    fibreRange.first           = std::lround( xprime / m_fibrePitch );

    // get x at the upper edge of the sensitive material
    xprime += dx_over_dz * 2 * m_fibreCoreRadius;
    fibreRange.second = std::lround( xprime / m_fibrePitch );

    // swap indices to allow for ++i iteration
    if ( fibreRange.first > fibreRange.second ) std::swap<int>( fibreRange.first, fibreRange.second );

    // Restrict the fibres to the boundaries of the mat
    if ( iLayer % 2 == 0 ) {
      fibreRange.first  = std::max( m_firstFibre0, fibreRange.first );
      fibreRange.second = std::min( -m_firstFibre0, fibreRange.second );
    } else {
      fibreRange.first  = std::max( m_firstFibre1, fibreRange.first );
      fibreRange.second = std::min( m_lastFibre1, fibreRange.second );
    }

    return fibreRange;
  }

  Gaudi::Property<double> m_fibreDiameter{this, "FibreDiameter", 0.250 * Gaudi::Units::mm,
                                          "Diameter of the fibre in the fibre mats"};
  Gaudi::Property<double> m_fibreCoreRadius{this, "FibreCoreRadius", 0.110 * Gaudi::Units::mm,
                                            "Radius of the fibre core"};
  Gaudi::Property<double> m_fibrePitch{this, "FibrePitch", 0.275 * Gaudi::Units::mm,
                                       "Distance between two neighboring fibre centers in x direction"};
  Gaudi::Property<double> m_deltaZ{this, "DeltaZ", 0.210 * Gaudi::Units::mm, "Pitch in z-direction"};
  Gaudi::Property<int>    m_nlayers{this, "Nlayers", 6, "Number of layers"};

  Gaudi::Property<double> m_crossTalkProb{this, "CrossTalkProb", 0.135, "Probability of cross talk between fibers"};
  Gaudi::Property<double> m_CTmodelTouching{
      this, "CTModelTouching", 0.163, "Fraction of cross talk going to the each of the 4 neighbors touching fibers"};
  Gaudi::Property<double> m_CTmodelSide{this, "CTModelSide", 0.147,
                                        "Fraction of cross talk going to each  of the 2 neighbors on the sides"};
  Gaudi::Property<double> m_CTmodel2layers{this, "CTModel2layers", 0.028,
                                           "Fraction of cross talk going to each of the 2 neighbors on the same column "
                                           "2 layers above and below."};

  double m_minZ;
  int    m_firstFibre0;
  int    m_firstFibre1;
  int    m_lastFibre1;
};

#endif // MCFTDISTRIBUTIONFIBRETOOL_H
