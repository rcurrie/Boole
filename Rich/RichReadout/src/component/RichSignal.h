/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//===============================================================================
/** @file RichSignal.h
 *
 *  Header file for RICH digitisation algorithm : Rich::MC::Digi::Signal
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @author Alex Howard   a.s.howard@ic.ac.uk
 *  @date   2003-11-06
 */
//===============================================================================

#ifndef RICHREADOUT_RICHSIGNAL_H
#define RICHREADOUT_RICHSIGNAL_H 1

// base class
#include "RichKernel/RichAlgBase.h"

// from Gaudi
#include "GaudiKernel/RndmGenerators.h"

// Event model
#include "Event/MCParticle.h"
#include "Event/MCRichDeposit.h"
#include "Event/MCRichHit.h"

// Math
#include "GaudiKernel/Point3DTypes.h"

// interfaces
#include "MCInterfaces/IRichMCTruthTool.h"
#include "RichInterfaces/IRichSmartIDTool.h"

// kernel
#include "Kernel/ParticleID.h"

namespace Rich {
  namespace MC {
    namespace Digi {

      /** @class Signal RichSignal.h
       *
       *  Performs a simulation of the photon energy desposition
       *
       *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
       *  @author Alex Howard   a.s.howard@ic.ac.uk
       *  @date   2003-11-06
       */

      class Signal final : public Rich::AlgBase {

      public:
        /// Constructor
        Signal( const std::string& name, ISvcLocator* pSvcLocator );

        StatusCode initialize() override final;
        StatusCode execute() override final;
        StatusCode finalize() override final;

      private: // methods
        /// Process the event at the given location, with the corresponding TOF offset
        StatusCode ProcessEvent( const std::string& hitLoc, const double tofOffset, const int eventType ) const;

      private: // data
        LHCb::MCRichDeposits* m_mcDeposits = nullptr;

        // locations of MCRichHits in TES
        std::string m_RichHitLocation;
        std::string m_RichPrevLocation;
        std::string m_RichPrevPrevLocation;
        std::string m_RichNextLocation;
        std::string m_RichNextNextLocation;

        std::string m_RichDepositLocation;
        std::string m_lhcBkgLocation;

        /// Flag to turn on the use of the spillover events
        bool m_doSpillover;

        /// Flag to turn on the use of the LHC backgrounde events
        bool m_doLHCBkg;

        /// Flag to turn on testing on smartIDs from Gauss
        bool m_testSmartIDs;

        /// Pointer to RichSmartID tool
        const Rich::ISmartIDTool* m_smartIDTool = nullptr;

        /// Pointer to RichMCTruth tool
        const Rich::MC::IMCTruthTool* m_truth = nullptr;

        /// random number generator
        mutable Rndm::Numbers m_rndm;

        // Global time shift for each RICH, to get both to same calibrated point
        std::vector<double> m_timeShift;
      };

    } // namespace Digi
  }   // namespace MC
} // namespace Rich

#endif // RICHREADOUT_RICHSIGNAL_H
