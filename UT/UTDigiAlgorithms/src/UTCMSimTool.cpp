/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "GaudiKernel/IIncidentListener.h"
#include "GaudiKernel/IIncidentSvc.h"
#include "GaudiKernel/IRndmGen.h"
#include "GaudiKernel/SmartIF.h"
#include "IUTCMSimTool.h"
#include "Kernel/UTToolBase.h"
#include "UTDet/DeUTDetector.h"

/** @class UTCMSimTool UTCMSimTool.h
 *
 *  Tool for for simulating pedestal
 *
 *  @author M.Needham
 *  @date   14/3/2010
 */

class UTCMSimTool : public extends<UT::ToolBase, IUTCMSimTool, IIncidentListener> {

public:
  UTCMSimTool( const std::string& type, const std::string& name, const IInterface* parent );

  /// initialize
  StatusCode initialize() override;

  /// return the simulated cm noise in this sector for this event
  double noise( const LHCb::UTChannelID& chan ) const override;

  /** Implement the handle method for the Incident service.
   *  This is used to inform the tool of software incidents.
   *
   *  @param incident The incident identifier
   */
  void handle( const Incident& incident ) override;

private:
  void initEvent() const;

  SmartIF<IRndmGen>       m_gaussDist;
  Gaudi::Property<bool>   m_forceOptions{this, "forceOptions", true};
  Gaudi::Property<double> m_cmNoise{this, "cmNoise", 2.0}; // value to use if forceOptions true

  mutable bool                                m_isInitialized = false;
  mutable std::map<LHCb::UTChannelID, double> m_cmValues;
};

DECLARE_COMPONENT( UTCMSimTool )

UTCMSimTool::UTCMSimTool( const std::string& type, const std::string& name, const IInterface* parent )
    : extends{type, name, parent} {
  setForcedInit();
}

StatusCode UTCMSimTool::initialize() {

  return UT::ToolBase::initialize().andThen( [&] {
    // Add incident at begin of each event
    incSvc()->addListener( this, IncidentType::BeginEvent );
  } );
}

void UTCMSimTool::handle( const Incident& incident ) {
  if ( IncidentType::BeginEvent == incident.type() ) m_isInitialized = false;
}

void UTCMSimTool::initEvent() const {
  // make the map
  m_cmValues.clear();
  for ( const auto& iterS : tracker()->sectors() ) {
    if ( m_forceOptions.value() ) {
      m_cmValues[iterS->elementID()] = m_cmNoise * m_gaussDist->shoot();
    } else {
      m_cmValues[iterS->elementID()] = iterS->cmSectorNoise() * m_gaussDist->shoot();
    }
  }
}

double UTCMSimTool::noise( const LHCb::UTChannelID& chan ) const {
  if ( !m_isInitialized ) initEvent();
  return m_cmValues[chan];
}
