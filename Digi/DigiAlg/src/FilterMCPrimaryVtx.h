/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef FILTERMCPRIMARYVTX_H
#define FILTERMCPRIMARYVTX_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"

/** @class FilterMCPrimaryVtx FilterMCPrimaryVtx.h
 *  Filters MCParticles container to keep only primary vertices
 *  Removes daughters, updates SmartRefs in MCHeader
 *
 *  @author Marco Cattaneo
 *  @date   2009-04-06
 */
class FilterMCPrimaryVtx : public GaudiAlgorithm {
public:
  /// Standard constructor
  FilterMCPrimaryVtx( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~FilterMCPrimaryVtx(); ///< Destructor

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution
  StatusCode finalize() override;   ///< Algorithm finalization

protected:
private:
};
#endif // FILTERMCPRIMARYVTX_H
