/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//===============================================================================
/** @file RichDetailedFrontEndResponse.h
 *
 *  Header file for RICH digitisation algorithm : Rich::MC::Digi::DetailedFrontEndResponse
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @author Alex Howard   a.s.howard@ic.ac.uk
 *  @date   2003-11-06
 */
//===============================================================================

#ifndef RICHREADOUT_RICHDETAILEDFRONTENDRESPONSE_H
#define RICHREADOUT_RICHDETAILEDFRONTENDRESPONSE_H 1

// STD
#include <memory>
#include <sstream>

// base class
#include "RichKernel/RichAlgBase.h"

// from Gaudi
#include "GaudiKernel/RndmGenerators.h"

// Interfaces
#include "RichInterfaces/IRichSmartIDTool.h"

// Kernel
#include "Kernel/RichDetectorType.h"
#include "Kernel/RichSmartID.h"

// RichUtils
#include "RichUtils/RichHashMap.h"
#include "RichUtils/RichMap.h"

// local
#include "RichBase.h"
#include "RichFrontEndDigitiser.h"
#include "RichPixel.h"
#include "RichPixelReadout.h"
#include "RichRegistry.h"
#include "RichShape.h"
#include "RichTimeSample.h"

// event model
#include "Event/MCRichDeposit.h"
#include "Event/MCRichDigit.h"
#include "Event/MCRichHit.h"
#include "Event/MCRichSummedDeposit.h"

namespace Rich {
  namespace MC {
    namespace Digi {

      /** @class DetailedFrontEndResponse RichDetailedFrontEndResponse.h
       *
       *  Performs a detailed simulation of the RICH HPD pixel response
       *
       *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
       *  @author Alex Howard   a.s.howard@ic.ac.uk
       *  @date   2003-11-06
       */

      class DetailedFrontEndResponse final : public Rich::AlgBase {

      public:
        /// Constructor
        DetailedFrontEndResponse( const std::string& name, ISvcLocator* pSvcLocator );

        StatusCode initialize() override final;
        StatusCode execute() override final;
        StatusCode finalize() override final;

        typedef std::pair<LHCb::MCRichSummedDeposit*, RichTimeSample> TimeData;
        typedef std::vector<TimeData>                                 samplecache_t;

      private:                // methods
        StatusCode Analog();  ///< Run the analogue simulation
        StatusCode Digital(); ///< Run the digital simulation

      private: // data
        RichRegistry::BasePtr actual_base;

        RichRegistry theRegistry;

        samplecache_t tscache;

        LHCb::MCRichSummedDeposits* m_summedDeposits = nullptr;

        std::string m_mcRichSummedDepositsLocation;
        std::string m_mcRichDigitsLocation;

        int    m_Pedestal;
        double m_Calibration;

        double m_Noise;
        double m_Threshold;
        double m_ThresholdSigma;

        double el_per_adc;

        Rndm::Numbers m_gaussThreshold;
        Rndm::Numbers m_gaussNoise;

        std::vector<double> m_timeShift;
      };

    } // namespace Digi
  }   // namespace MC
} // namespace Rich

#endif // RICHREADOUT_RICHDETAILEDFRONTENDRESPONSE_H
