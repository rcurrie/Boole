/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef IFTSIPMTOOL_H
#define IFTSIPMTOOL_H 1

// from Gaudi
#include "GaudiKernel/IAlgTool.h"

// from LHCb
#include "Event/MCFTDeposit.h"

/** @class IFTSiPMTool IFTSiPMTool.h
 *
 *  Interface for the tool that adds noise from the SiPM and
 *  the efficiency function for detecting photons.
 *
 *  @author Violaine Bellee, Julian Wishahi
 *  @date   2017-02-14
 */

struct IFTSiPMTool : extend_interfaces<IAlgTool> {

  // Return the interface ID
  DeclareInterfaceID( IFTSiPMTool, 1, 0 );
  virtual void  addNoise( LHCb::MCFTDeposits* depositConts )   = 0;
  virtual int   generateDirectXTalk( int nPhotons )            = 0;
  virtual int   generateDelayedXTalk( int nPhotons )           = 0;
  virtual float generateDelayedXTalkTime()                     = 0;
  virtual int   generateChannelXTalk()                         = 0;
  virtual float photonDetectionEfficiency( double wavelength ) = 0;
  virtual bool  sipmDetectsPhoton( double wavelength )         = 0;
};

#endif // IFTSIPMTOOL_H
