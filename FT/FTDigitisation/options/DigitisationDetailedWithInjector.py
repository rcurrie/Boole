###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import *
from Configurables import Boole, LHCbApp
###
#job options
importOptions("$APPCONFIGOPTS/Boole/Boole-Upgrade-Baseline-20150522.py")
Boole().DatasetName = "DetailedWithInjector"
Boole().DataType = "Upgrade"
Boole().DetectorDigi = ['FT']
Boole().DetectorLink = []
Boole().DetectorMoni = []
Boole().DigiSequence = ['FT']
Boole().Outputs = []
Boole().EvtMax = 10
#Boole().InputDataType = "None"
#Boole().InitSequence = []
Boole().DetectorInit = {'MUON': [''], 'DATA': ['']}

##################################################################
LHCbApp().DDDBtag = "dddb-20170211"
LHCbApp().CondDBtag = "sim-20170210-vc-md100"  #(for magnet down)
##################################################################

from Configurables import ApplicationMgr
ApplicationMgr().EvtSel = "NONE"

from Configurables import MCFTMCHitInjector
myAlgInjector = MCFTMCHitInjector()
myAlgInjector.OutputLocation = "MC/FT/InjectorHits"

myAlgInjector.TargetStations = [1, 1]
myAlgInjector.TargetLayers = [1, 2]
myAlgInjector.TargetQuarters = [1, 1]
myAlgInjector.TargetModules = [1, 1]
myAlgInjector.TargetMats = [1, 1]

#myAlgInjector.MCHitLocalXs  = [8.35] # shoot at channel 32 in SiPM2
myAlgInjector.MCHitLocalXs = [0.4]  # shoot at fibre 2 in mat
myAlgInjector.MCHitLocalYs = [0.0]
#myAlgInjector.MCHitDeltaXs  = [0.0]
myAlgInjector.MCHitDeltaXs = [0.2]
myAlgInjector.MCHitDeltaYs = [0.0]
myAlgInjector.MCHitEnergies = [0.3]
myAlgInjector.MCHitMomenta = [15000.]
myAlgInjector.MCHitTimes = [27.]

from Configurables import MCFTDepositCreator
myAlgDeposit = MCFTDepositCreator()
myAlgDeposit.SimulationType = "detailed"  # detailed, improved, effective
myAlgDeposit.InputLocation = "MC/FT/InjectorHits"
myAlgDeposit.SimulateNoise = False
myAlgDeposit.SimulateIntraChannelXTalk = False

from Configurables import MCFTDistributionChannelTool
distrChanTool = myAlgDeposit.addTool(MCFTDistributionChannelTool())
distrChanTool.LightSharing = "gauss"

from Configurables import FTClusterCreator
myAlgCluster = FTClusterCreator()
myAlgCluster.WriteFullClusters = True

from Configurables import MCFTDepositMonitor
myDepositMonitor = MCFTDepositMonitor()
myDepositMonitor.HitLocation = "MC/FT/InjectorHits"

from Configurables import FTClusterMonitor
FTClusterMonitor().HitLocation = "MC/FT/InjectorHits"

from Configurables import MCFTDigitCreator
from Configurables import MCFTPhotonMonitor, MCFTDigitMonitor


def doIt():
    GaudiSequencer("DigiFTSeq").Members = [
        myAlgInjector, myAlgDeposit,
        MCFTDigitCreator(), myAlgCluster,
        MCFTPhotonMonitor(), myDepositMonitor,
        MCFTDigitMonitor(),
        FTClusterMonitor()
    ]
    GaudiSequencer("InitBooleSeq").Members = []


appendPostConfigAction(doIt)
