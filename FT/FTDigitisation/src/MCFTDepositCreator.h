/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/** @class MCFTDepositCreator MCFTDepositCreator.h
 *
 *  From the list of MCHits, this algorithm fill a container of FTChannelID
 *  with the photons in each of them, and store it in the
 *  transient data store.
 *
 *
 *  @author DE VRIES Jacco, WISHAHI Julian, BELLEE Violaine, COGNERAS Eric
 *  @date   2016-12-19
 */

#ifndef MCFTDEPOSITCREATOR_H
#define MCFTDEPOSITCREATOR_H 1

// Include files
/// from Gaudi
#include "GaudiAlg/Transformer.h"
#include "GaudiKernel/SystemOfUnits.h"

/// from Event
#include "Event/MCFTDeposit.h"
#include "Event/MCFTPhoton.h"
#include "Event/MCHit.h"

// FTDet
#include "FTDet/DeFTDetector.h"

// from Boole/FT/FTDigitisation
#include "IFTSiPMTool.h"
#include "IMCFTAttenuationTool.h"
#include "IMCFTDistributionChannelTool.h"
#include "IMCFTDistributionFibreTool.h"
#include "IMCFTPhotonTool.h"

using namespace Gaudi::Functional;

typedef std::pair<double, const LHCb::MCHits*> SpillPair;

class MCFTDepositCreator final
    : public MultiTransformer<std::tuple<LHCb::MCFTDeposits, LHCb::MCFTPhotons>( const std::array<SpillPair, 4>& )> {

public:
  MCFTDepositCreator( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode                                        initialize() override;
  std::tuple<LHCb::MCFTDeposits, LHCb::MCFTPhotons> operator()( const std::array<SpillPair, 4>& ) const override;

private:
  void convertHitToPhotons( const LHCb::MCHit* mchit, const DeFTMat& mat, double timeOffset,
                            LHCb::MCFTPhotons& photons ) const;

  void convertPhotonsToDepositsDetailed( const LHCb::MCFTPhotons& photons, LHCb::MCFTDeposits& deposits ) const;

  void convertHitToDepositsEffective( const LHCb::MCHit* mchit, const DeFTMat& mat, double timeOffset,
                                      LHCb::MCFTDeposits& deposits ) const;

  // Simulation types
  enum class SimulationType { effective, detailed };
  const std::map<std::string, SimulationType> m_simulationTypes{{"effective", SimulationType::effective},
                                                                {"detailed", SimulationType::detailed}};
  SimulationType                              m_simulationType = SimulationType::detailed;
  Gaudi::Property<std::string>                m_simulationTypeChoice{this, "SimulationType", "detailed",
                                                      "Simulation type (effective, detailed)"};

  // FTDet pointer
  DeFTDetector* m_deFT = nullptr;

  // Required tools
  // AttenuationTool describes the attenuation in the fibres
  IMCFTAttenuationTool*        m_attenuationTool = nullptr;
  Gaudi::Property<std::string> m_attenuationToolName{this, "AttenuationToolName", "MCFTG4AttenuationTool",
                                                     "Name of the MCFTAttenuationTool"};

  // DistributionFibreTool describes the energy distribution in the mat's fibres
  IMCFTDistributionFibreTool*  m_distrFibreTool = nullptr;
  Gaudi::Property<std::string> m_distrFibreToolName{this, "DistributionFibreToolName", "MCFTDistributionFibreTool",
                                                    "Name of the MCFTDistributionFibreTool"};

  // PhotonTool describes the distribution of photon observables
  IMCFTPhotonTool*             m_photonTool = nullptr;
  Gaudi::Property<std::string> m_photonToolName{this, "PhotonToolName", "MCFTPhotonTool", "Name of the MCFTPhotonTool"};

  // DistributionChannelTool distributes energy/photons to the channels
  IMCFTDistributionChannelTool* m_distrChannelTool = nullptr;
  Gaudi::Property<std::string>  m_distrChannelToolName{
      this, "DistributionChannelToolName", "MCFTDistributionChannelTool", "Name of the DistributionChannelTool"};

  // SiPMTool adds the noise deposits and the wavelenght-dependent efficiency
  IFTSiPMTool*                 m_sipmTool = nullptr;
  Gaudi::Property<std::string> m_sipmToolName{this, "SiPMToolName", "FTSiPMTool", "Name of the SiPMTool"};
  Gaudi::Property<bool>        m_simulateNoise{this, "SimulateNoise", true, "Simulate noise"};
  Gaudi::Property<bool>        m_simulateIntraChannelXTalk{this, "SimulateIntraChannelXTalk", true,
                                                    "Simulate intra-channel cross-talk"};
  Gaudi::Property<bool>        m_simulatePDE{this, "SimulatePDE", false, "Simulate Effect of SiPM PDE"};
  Gaudi::Property<bool>        m_simulateChannelXT{this, "SimulateChannelXT", true,
                                            "Simulate Effect of channel to channel cross talk"};
};

#endif // MCFTDEPOSITCREATOR_H
