/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/MCUTDeposit.h"
#include "Event/MCUTDigit.h"
#include "GaudiAlg/Transformer.h"
#include "Kernel/UTAlgBase.h"

using namespace LHCb;

/** @class MCUTDigitCreator MCUTDigitCreator.h
 *
 *  Algorithm for creating MCUTDigits from MCUTDeposits.
 *
 *  @author M.Needham
 *  @date   10/03/2002
 */

struct MCUTDigitCreator : Gaudi::Functional::Transformer<MCUTDigits( const MCUTDeposits& ),
                                                         Gaudi::Functional::Traits::BaseClass_t<UT::AlgBase>> {

  MCUTDigitCreator( const std::string& name, ISvcLocator* pSvcLocator );

  MCUTDigits operator()( const MCUTDeposits& ) const override;
};

DECLARE_COMPONENT( MCUTDigitCreator )
namespace {
  bool keepAdding( const MCUTDeposit* firstDep, const MCUTDeposit* secondDep ) {
    // check whether to continueing adding deposits to vector
    return ( firstDep->channelID() == secondDep->channelID() );
  }
} // namespace

MCUTDigitCreator::MCUTDigitCreator( const std::string& name, ISvcLocator* pSvcLocator )
    : Transformer{name,
                  pSvcLocator,
                  {"InputLocation", MCUTDepositLocation::UTDeposits},
                  {"OutputLocation", MCUTDigitLocation::UTDigits}} {}

MCUTDigits MCUTDigitCreator::operator()( const MCUTDeposits& depositCont ) const {

  // digits container
  MCUTDigits digitCont;
  digitCont.reserve( depositCont.size() );

  // Collect all deposits on 1 strip (= MCUTDigit)
  // this requires the list of deposits to be sorted by UTChannelID
  auto iterDep = depositCont.begin();
  auto jterDep = iterDep;
  while ( iterDep != depositCont.end() ) {

    SmartRefVector<MCUTDeposit> depositVector;
    do {
      depositVector.push_back( *jterDep );
      ++jterDep;
    } while ( ( jterDep != depositCont.end() ) && ( keepAdding( *iterDep, *jterDep ) == true ) );

    // make a new MCUTDigit and add it to the vector
    auto* newDigit = new MCUTDigit();
    newDigit->setMcDeposit( depositVector );
    const UTChannelID aChan = ( *iterDep )->channelID();
    digitCont.insert( newDigit, aChan );

    iterDep = jterDep;

  } // iterDep

  return digitCont;
}
