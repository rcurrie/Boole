/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef IUTPEDESTALSIMTOOL_H
#define IUTPEDESTALSIMTOOL_H 1

#include "GaudiKernel/IAlgTool.h"

/** @class IUTChargeSharingTool IUTChargeSharingTool.h
 *
 *  Interface Class for charge sharing UT strip
 *
 *  @author M.Needham
 *  @date   14/3/2002
 */

namespace LHCb {
  class UTChannelID;
}

struct IUTPedestalSimTool : extend_interfaces<IAlgTool> {

  DeclareInterfaceID( IUTPedestalSimTool, 1, 0 );

  /// calc sharinh
  virtual double pedestal( const LHCb::UTChannelID& chan ) const = 0;
};

#endif // IUTPedestalSimTool_H
