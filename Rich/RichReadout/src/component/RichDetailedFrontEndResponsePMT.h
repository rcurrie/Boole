/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// STL
#include <cmath>
#include <sstream>

// from Kernel
#include "Kernel/RichDetectorType.h"
#include "Kernel/RichSmartID.h"
#include "RichKernel/RichHistoAlgBase.h"

// Interfaces
#include "RichInterfaces/IRichSmartIDTool.h"

// Utils
#include "RichUtils/RichHashMap.h"
#include "RichUtils/RichMap.h"

// from Gaudi
#include "GaudiKernel/RndmGenerators.h"

// from Event
#include "Event/MCRichDeposit.h"
#include "Event/MCRichDigit.h"
#include "Event/MCRichHit.h"
#include "Event/MCRichSummedDeposit.h"

// from RICHDet
#include "RichDet/DeRichPMT.h"
#include "RichDet/DeRichSystem.h"

// Local
#include "RichChannelPropertiesPMT.h"

/** @class DetailedFrontEndResponsePMT DetailedFrontEndResponsePMT.h
 *
 *  @author Marcin Kucharczyk, Mariusz Witek
 *  @date   2015-10-08
 */

using namespace LHCb;

namespace Rich {
  namespace MC {
    namespace Digi {

      class DetailedFrontEndResponsePMT final : public Rich::HistoAlgBase {

      public:
        DetailedFrontEndResponsePMT( const std::string& name, ISvcLocator* pSvcLocator );

        StatusCode initialize() override final;
        StatusCode execute() override final;
        StatusCode finalize() override final;

      private:
        class PMTChannel;
        typedef Rich::Map<LHCb::RichSmartID, PMTChannel> PMTChannelContainer;
        StatusCode                                       Analog( PMTChannelContainer& channels );
        StatusCode                                       Digital( PMTChannelContainer& channels );

        // get gain based on channel properties (gain mean/rms); require that gain is higher than certain value
        // (optionally)
        float getChannelGain( const LHCb::RichSmartID& RichSmartID, const float& minValue = 0. );
        // read data needed for time info simulation from the DB
        StatusCode readTimeInfoPropertiesFromDB();
        // get time from signal arrival at the anode to it's acquisition at the readout output [ns]
        float getReadoutDelay( const float inputSignal, const float threshold ) const;
        // get time-over-threshold of the readout output signal after acquisition [ns]
        float getReadoutTimeOverThreshold( const float inputSignal, const float threshold ) const;
        // read the single photoelectron energy deposit from the DB ( must be the same number as used in Gauss to create
        // the deposit )
        StatusCode readPhotoelectronEnergyFromDB();

      private:
        Gaudi::Property<bool>        m_readParametersFromDB{this, "ReadParametersFromDB", true};
        Gaudi::Property<bool>        m_sin{this, "Sin", false};
        Gaudi::Property<std::string> m_spillover{this, "SpilloverModel", "None"};
        Gaudi::Property<float>       m_gainMean{this, "GainMean", 0.9};
        Gaudi::Property<float>       m_gainRms{this, "GainRms", 0.2};
        Gaudi::Property<float>       m_threshold{this, "Threshold", 0.125};

        // properties for the time gate (all times in ns)
        const float                        m_timeGateWindowRange = 25.0f;
        const float                        m_timeGateSlotWidth   = 3.125f;
        const unsigned int                 m_timeGateNrOfSlots   = 8;
        typedef std::bitset<8>             TimeGateSlots;
        typedef std::vector<TimeGateSlots> TimeGateLookupTable;

        // time gate is applied to 'raw' deposit times with t0 at the collision time (no offsets)
        // the beginning of the time-gate window and the lookup table are given separately for R1 / R2
        // if the lookup table is not provided, a 25 ns time window is used by default
        Gaudi::Property<bool>                     m_timeGate{this, "TimeGate", true};
        Gaudi::Property<std::vector<float>>       m_timeGateWindowBegin{this, "TimeGateWindowBegin", {18.00, 48.00}};
        Gaudi::Property<std::vector<std::string>> m_timeGateLookupTableR1{this, "TimeGateLookupTableR1", {}};
        Gaudi::Property<std::vector<std::string>> m_timeGateLookupTableR2{this, "TimeGateLookupTableR2", {}};

        // end of the time-gate window [ns] (based on the begin and range)
        std::vector<float> m_timeGateWindowEnd{0., 0.};

        // time-gate lookup tables (parsed to a proper format from a string from the Gaudi property)
        TimeGateLookupTable m_lookupTableR1{};
        TimeGateLookupTable m_lookupTableR2{};

        // probability to get a SIN hit is given for a certain time period in the DB (SinProbTimeRange); it is scaled
        // according to the actual time window in which the SIN is simulated (SinTimeWindowRange); to see the final
        // effect of SIN in the studied time-gate window, make sure that it is simulated in the full relevant time range
        // (SinTimeWindowBegin must be earlier than the time-gate beginning by at least the maximum SIN
        // time-over-threshold; SinTimeWindowRange should then cover the whole range up to the end of the time-gate
        // window)
        Gaudi::Property<float>              m_sinProbTimeRange{this, "SinProbTimeRange", 25.0};
        Gaudi::Property<float>              m_sinTimeWindowRange{this, "SinTimeWindowRange", 100.0};
        Gaudi::Property<std::vector<float>> m_sinTimeWindowBegin{this, "SinTimeWindowBegin", {-35.00, -5.00}};

        Gaudi::Property<std::string> m_mcRichSumDepsLocation{this, "MCRichSummedDepositsLocation",
                                                             LHCb::MCRichSummedDepositLocation::Default};
        Gaudi::Property<std::string> m_mcRichDigitsLocation{this, "MCRichDigitsLocation",
                                                            LHCb::MCRichDigitLocation::Default};

        // parameters for the monitoring histograms
        const float m_histInputSignalMax       = 5.0;
        const float m_histReadoutDelayMax      = 20.0;
        const float m_histTimeOverThresholdMax = 35.0;
        const float m_histTimeOfAcquisitionMax = 100.0;

        const unsigned int m_nrOfBinsInHistogram1D = 100;
        const unsigned int m_nrOfBinsInHistogram2D = 50;

        RichChannelPropertiesPMT*   m_propertyTool = nullptr;
        DeRichSystem*               m_deRichSystem = nullptr;
        LHCb::RichSmartID::Vector   m_readoutChannelsList{};
        LHCb::MCRichSummedDeposits* m_sDeposits = nullptr;
        mutable Rndm::Numbers       m_gaussForGain{};
        mutable Rndm::Numbers       m_flatForSinProb{};
        mutable Rndm::Numbers       m_flatForSinTime{};
        mutable Rndm::Numbers       m_pdTransitTimeTypeR{};
        mutable Rndm::Numbers       m_pdTransitTimeTypeH{};

        // energy of a single photoelectron deposit from Gauss [MeV]
        float m_photoelectronEnergy{0.};

        float        m_timeInfoIndexIntervalInElectrons{0.};
        unsigned int m_readoutTimeInfoIndexMin{0};
        unsigned int m_readoutTimeInfoIndexMax{0};

        std::vector<std::shared_ptr<Rich::TabulatedProperty1D>> m_readoutDelay{};
        std::vector<std::shared_ptr<Rich::TabulatedProperty1D>> m_readoutTimeOverThreshold{};

        // time of flight of a photoelectron in a PD (from photocathode to anode) in Gauss [ns]
        float m_pdPhotoelectronTimeOfFlight{0.};
        // info on PD transit time (mean and spread) for R/H PD types [ns]
        float m_pdTransitTimeMeanTypeR{0.};
        float m_pdTransitTimeSpreadTypeR{0.};
        float m_pdTransitTimeMeanTypeH{0.};
        float m_pdTransitTimeSpreadTypeH{0.};

        // structure for deposits (not the same as RichDeposit, it is what is effectively seen by the Claro - can be a
        // SIN hit, or two deposits close in time which are seen as a single one)
        struct PMTDeposit {
          float inputSignal{0.};
          float timeOfAcquisition{0.};
          float timeOverThreshold{0.};
        };

        // helper class to process all deposits within a single PMT channel (including SIN)
        class PMTChannel {

        private:
          typedef std::vector<PMTDeposit> PMTDepositContainer;
          PMTDepositContainer             m_deposits{};
          TimeGateSlots                   m_outputTimeSampling{};
          bool                            m_hasSin{false};
          LHCb::MCRichSummedDeposit*      m_summedDeposit{nullptr};
          typedef std::vector<bool>       TimeGatePatternMatches;
          TimeGatePatternMatches          m_timeGatePatternMatches{};

        public:
          inline void addDeposit( const float inputSignal,       //
                                  const float timeOfAcquisition, //
                                  const float timeOverThreshold ) {
            PMTDeposit deposit;
            deposit.inputSignal       = inputSignal;
            deposit.timeOfAcquisition = timeOfAcquisition;
            deposit.timeOverThreshold = timeOverThreshold;
            m_deposits.push_back( deposit );
          }

          // add the given deposit (with a certain time-of-acquisition and time-over-threshold) to the Claro output
          // pattern
          inline void processDeposit( const float timeOfAcquisition, //
                                      const float timeOverThreshold, //
                                      const float timeGateMin,       //
                                      const float timeGateMax,       //
                                      const float timeGateBin ) {

            // check if the deposit has any overlap with the time-sampling window
            if ( isOverlapBetweenSignalAndTimeSampling( timeOfAcquisition, timeOverThreshold, timeGateMin,
                                                        timeGateMax ) ) {

              const auto firstTimeSlotIndex = getTimeSlotIndex( timeOfAcquisition, timeGateMin, timeGateBin );

              const auto lastTimeSlotIndex =
                  getTimeSlotIndex( timeOfAcquisition + timeOverThreshold, timeGateMin, timeGateBin );

              // set time slots status within the time-sampling window; time slots ordering is reversed wrt to the
              // 'bitset' format
              for ( auto i = firstTimeSlotIndex; i < lastTimeSlotIndex + 1; ++i ) {
                if ( i >= 0 && (std::size_t)i < m_outputTimeSampling.size() ) {
                  m_outputTimeSampling.set( m_outputTimeSampling.size() - 1 - i );
                }
              }
            }
          }

          // apply a time-gate on the Claro output
          inline bool applyTimeGate( const TimeGateLookupTable& timeGateLookupTable );

          inline const TimeGateSlots&       getOutputTimeSampling() const { return m_outputTimeSampling; }
          inline const PMTDepositContainer& getDeposits() const { return m_deposits; }

          inline LHCb::MCRichSummedDeposit* getSummedDeposit() { return m_summedDeposit; }
          inline void setSummedDeposit( LHCb::MCRichSummedDeposit* summedDeposit ) { m_summedDeposit = summedDeposit; }

          inline bool hasSin() const noexcept { return m_hasSin; }
          inline void setHasSin() { m_hasSin = true; }

          inline const TimeGatePatternMatches& getTimeGatePatternMatches() const { return m_timeGatePatternMatches; }

        private:
          // check if the given signal has any overlap (in time) with the time-sampling window
          inline bool isOverlapBetweenSignalAndTimeSampling( const float timeOfAcquisition, //
                                                             const float timeOverThreshold, //
                                                             const float timeGateMin,       //
                                                             const float timeGateMax ) const {
            return ( ( timeOfAcquisition <= timeGateMax ) && ( timeOfAcquisition + timeOverThreshold >= timeGateMin ) );
          }

          // get index of the time slot to which the given time corresponds
          inline int getTimeSlotIndex( const float time, const float timeGateMin, const float timeGateBin ) const {
            const auto var = ( 0 != timeGateBin ? ( time - timeGateMin ) / timeGateBin : 0 );
            // mimics std::floor, but avoid uses it as throws strange FPE with clang..
            auto j = static_cast<int>( var );
            if ( var < 0 ) { --j; }
            return j;
          }

          // process a single line of the lookup table
          inline bool processLookupTableLine( const TimeGateSlots& line,
                                              const TimeGateSlots& outputTimeSampling ) const {
            return ( outputTimeSampling == line );
          }
        };

        // helper functions
        inline const DeRichPMT* getDePmtFromSmartId( const LHCb::RichSmartID smartID ) const {
          return (DeRichPMT*)m_deRichSystem->dePD( smartID );
        }
        inline float getChannelGainMean( const LHCb::RichSmartID smartID ) const {
          return m_readParametersFromDB.value() ? getDePmtFromSmartId( smartID )->PmtChannelGainMean( smartID )
                                                : m_gainMean.value();
        }
        inline float getChannelGainRms( const LHCb::RichSmartID smartID ) const {
          return m_readParametersFromDB.value() ? getDePmtFromSmartId( smartID )->PmtChannelGainRms( smartID )
                                                : m_gainRms.value();
        }
        inline float getChannelThreshold( const LHCb::RichSmartID smartID ) const {
          return m_readParametersFromDB.value() ? getDePmtFromSmartId( smartID )->PmtChannelThreshold( smartID )
                                                : m_threshold.value();
        }
        inline float getChannelSinProbability( const LHCb::RichSmartID smartID ) const {
          return m_readParametersFromDB.value() ? getDePmtFromSmartId( smartID )->PmtChannelSinProbability( smartID )
                                                : 0.;
        }
        inline float getPmtAverageOccupancy( const LHCb::RichSmartID smartID ) const {
          return m_readParametersFromDB.value() ? getDePmtFromSmartId( smartID )->PmtAverageOccupancy() : 0.;
        }

        inline void updateHistoryWithSin( LHCb::MCRichDigit*& digit ) const {
          auto digitHistory = digit->history();
          digitHistory.setSignalInducedNoise( true );
          digit->setHistory( digitHistory );
        }

        // provide a valid index for time info input set (take the closest one from the DB)
        inline unsigned int validTimeInfoInputIndex( const float thresholdInElectrons ) const {

          auto inputIndex = round( thresholdInElectrons / m_timeInfoIndexIntervalInElectrons );

          if ( inputIndex < m_readoutTimeInfoIndexMin ) {
            warning() << "No reliable time info for this threshold setting. The closest time info input will be used."
                      << endmsg;
            inputIndex = m_readoutTimeInfoIndexMin;
          } else if ( inputIndex > m_readoutTimeInfoIndexMax ) {
            warning() << "No reliable time info for this threshold setting. The closest time info input will be used."
                      << endmsg;
            inputIndex = m_readoutTimeInfoIndexMax;
          }

          // offset the index ( should start from 0 )
          inputIndex -= m_readoutTimeInfoIndexMin;

          return inputIndex;
        }

        // provide time of acquisition at the Claro output (taking all offsets into account)
        inline float timeOfAcquisition( const float timeOfDeposit, //
                                        const float inputSignal,   //
                                        const float threshold,     //
                                        const bool  isLargePMT ) const {
          const auto pdTransitTime = isLargePMT ? m_pdTransitTimeTypeH() : m_pdTransitTimeTypeR();
          return ( timeOfDeposit - m_pdPhotoelectronTimeOfFlight + pdTransitTime +
                   getReadoutDelay( inputSignal, threshold ) );
        }

        // provide time of acquisition at the readout output for SIN hits ( random time within the time window of
        // interest )
        inline float timeOfAcquisitionForSin( const float timeWindowMin ) const {
          return timeWindowMin + m_flatForSinTime();
        }

        // SIN probability needs to be multiplied by a factor corresponding to the studied time window (by default SIN
        // probability is given for 25 ns time slot)
        inline float sinProbFactorForTimeSampling( const float timeSamplingRange ) const {
          return timeSamplingRange / m_sinProbTimeRange;
        }

        // return a proper number if given timeGate property is different for R1/R2
        inline float timeGateParam( const LHCb::RichSmartID smartID, std::vector<float>& timeGateParamProperty ) const {
          const auto rich = smartID.rich();
          if ( rich == Rich::Rich1 ) {
            return timeGateParamProperty[0];
          } else if ( rich == Rich::Rich2 ) {
            return timeGateParamProperty[1];
          } else {
            warning() << "Given smartID not in R1/R2." << endmsg;
            return 0.;
          }
        }

        // parse & validate the patterns in a lookup table
        inline bool parseLookupTable( const std::vector<std::string>& lookupTableFrom,
                                      TimeGateLookupTable&            lookupTableTo ) const {
          bool isValid = true;
          for ( auto const& line : lookupTableFrom ) {
            if ( line.length() != m_timeGateNrOfSlots ) {
              isValid = false;
              break;
            } else {
              lookupTableTo.emplace_back( line );
            }
          }
          return isValid;
        }
      };

    } // namespace Digi
  }   // namespace MC
} // namespace Rich
