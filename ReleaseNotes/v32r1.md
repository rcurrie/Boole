2017-12-06 Boole v32r1
===

This version uses projects LHCb v43r1, Lbcom v21r1, Gaudi v29r0, LCG_91
 (Root 6.10.06) and SQLDDDB v7r*, ParamFiles v8r*, FieldMap v5r*, AppConfig v3r*  

This version is a production release for upgrade simulations  

This version is released on `v32r0-patches` branch. The previous release on `v32r0-patches` branch  was Boole `v32r0`.  

### New features
- Add template for generating these release notes with `gen_release_notes`, !118

### Enhancements
- Implementation of FT attenuation maps, !114 [LHCBSCIFI-33,LHCBSCIFI-89,LHCBSCIFI-90]  

### Monitoring changes
- Added monitoring of FTClusters, !116   

### Changes to tests
- Update tags for upgrade tests to pick up FT attenuation map, !115     
- Update exclusion in boole-mc11-spillover.qmt, fixes test with gcc7, !100  

